﻿Imports System.Xml
Imports System.Xml.XPath
Imports System.IO
Imports System.Web.Configuration
Imports System.Web.UI.WebControls


''' <summary>
''' This class shall deal with showing pictures of employers in the current department
''' We are using the data from the XML-file Telefonliste as source for data
''' We map up the signature to the picture of the employer.
''' 
''' To find out for which department we are in, we use the URL. This is stated like this:
''' http://intern.ntb.no/innenriks/
''' </summary>
''' <remarks></remarks>
Partial Class SectionPictures


    Inherits System.Web.UI.Page



    ''' <summary>
    ''' This code is run when the website is loaded. 
    ''' Here we must get the subdirectory of this department
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim pageUrl As String = Request.QueryString("avdeling")
        Dim sign As String = Request.QueryString("sign")


        Response.Cache.SetNoServerCaching()
        Dim ts As New TimeSpan(0, 0, 0)
        Response.Cache.SetMaxAge(ts)
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If (sign Is Nothing) Then
            getPictures(pageUrl)
        Else
            getPictures(pageUrl, sign)
        End If

    End Sub

    ''' <summary>
    ''' This code shall show the selected person with more information
    ''' </summary>
    ''' <param name="signature"></param>
    ''' <remarks></remarks>
    Public Sub getInfo(ByVal signature As String)

    End Sub
    ''' <summary>
    ''' This code shall get the pictures and other information from the XML-file
    ''' </summary>
    ''' <param name="department"></param>
    ''' <remarks></remarks>
    Public Sub getPictures(ByVal department As String, Optional ByVal signature As String = "")

        Try

            ' Dim xmlText As New XmlTextReader(WebConfigurationManager.AppSettings("PhoneListXML"))
            Dim Doc As XPathDocument = New XPathDocument(WebConfigurationManager.AppSettings("PhoneListXML"))

            Dim Navigator As XPathNavigator
            Navigator = Doc.CreateNavigator()

            Dim XPathQuer As String = "/telefonliste/person[avdeling='" + department + "']"
            If signature <> "" Then

                XPathQuer = "/telefonliste/person[avdeling='" + department + "'][signatur='" + signature + "']"
            End If

            Dim XPathExpr As XPathExpression = Navigator.Compile(XPathQuer)
            XPathExpr.AddSort("navn", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Text)

            Dim iterator As XPathNodeIterator = Navigator.Select(XPathExpr)


            Dim rowNew As TableRow = New TableRow()
            table1.Controls.Add(rowNew)

            Dim strText As String = ""
            Dim strSignatur As String = ""
            Dim counter As Integer = 0
            Dim totalcount As Integer = 0
            Dim ic As Integer = iterator.Count
            Dim strBirthdate As String = ""

            Dim oldname As String = ""
            For Each item As XPathNavigator In iterator

                ' Response.Write(item.SelectSingleNode("navn").Value & "<br>")

                Dim htmlImg As New ImageButton()
                Dim strNavn As String = ""

                ' Response.Write(item.SelectSingleNode("navn").Value.ToString() & "<br>")
                If Not (item.SelectSingleNode("navn") Is Nothing) Then
                    strNavn = item.SelectSingleNode("navn").Value.ToString()
                    
                    If strNavn.IndexOf(",") > 0 Then
                        Dim tmpNavn As Array = strNavn.Split(",")
                        strNavn = tmpNavn(1).ToString().Trim & " " & tmpNavn(0).ToString().Trim

                        ' strText = item.SelectSingleNode("navn").Value.ToString() & "<br>"
                        strText = "<strong>" & strNavn & "</strong><br>"
                    Else
                        strText = "<strong>" & strNavn & "</strong><br>"
                    End If
                End If

                If Not (item.SelectSingleNode("signatur") Is Nothing) Then

                    Dim imgFile As String = Trim(item.SelectSingleNode("signatur").Value.ToString()) + ".jpg"
                    htmlImg.ImageUrl = "dokumenter/Ansattbilder-lowres/" & imgFile ' + Trim(item.SelectSingleNode("signatur").Value.ToString()) + ".jpg"
                    htmlImg.Visible = True
                    htmlImg.AlternateText = strNavn
                    htmlImg.PostBackUrl = "ShowPicture.aspx?file=" & imgFile

                    ' htmlImg.Width = 75%


                End If

                If Not (item.SelectSingleNode("stilling") Is Nothing) Then
                    Dim strTemp As String = item.SelectSingleNode("stilling").Value.ToString()
                    If strTemp.Length > 20 Then
                        Dim s As Array = strTemp.Split(" ")

                        Dim strTemp2 As String = ""
                        Dim intArrayLength As Integer = 0
                        For Each strArray As String In s
                            strTemp2 &= strArray & " "
                            intArrayLength += strArray.Length

                            If intArrayLength > 10 Then
                                strTemp2 &= "<br>"
                                intArrayLength = 0
                            End If

                        Next

                        strTemp = strTemp2

                        ' Empty string just in case it causes any troubles
                        strTemp2 = ""


                    End If

                    ' strText &= "Stilling: " & item.SelectSingleNode("stilling").Value.ToString() & "<br>"
                    strText &= "Stilling: " & strTemp & "<br>"
                    strTemp = ""

                End If

                ' Getting the signature
                If Not (item.SelectSingleNode("signatur") Is Nothing) Then
                    strSignatur = item.SelectSingleNode("signatur").Value.ToString()
                    strText &= "Signatur: " & strSignatur & "<br>"
                End If

                ' Getting the e-mail address
                If Not (item.SelectSingleNode("epost") Is Nothing) Then
                    Dim strEpost As String = item.SelectSingleNode("epost").Value.ToString()

                    ' Cutting the address to 10 characters
                    Dim strSubEpost As String = strEpost.Substring(0, 10) & "..."
                    strText &= "E-post: <a href='mailto:" & strEpost & "'>" & strSubEpost & "</a><br>"
                End If

                ' Getting the phone number
                If Not (item.SelectSingleNode("telefon") Is Nothing) Then
                    strText &= "T: " & item.SelectSingleNode("telefon").Value.ToString() & "<br>"
                End If

                ' Getting the moble number
                If Not (item.SelectSingleNode("mobil") Is Nothing) Then
                    strText &= "M: " & item.SelectSingleNode("mobil").Value.ToString() & "<br>"
                End If

                If Not (item.SelectSingleNode("birthdate") Is Nothing) Then
                    Dim strTemp As String = item.SelectSingleNode("birthdate").Value.ToString()
                    Dim arrTemp As Array = strTemp.Split("-")

                    ' 1956-12-11
                    strBirthdate = arrTemp(2).ToString() & "." & arrTemp(1).ToString() & "." & arrTemp(0).ToString()
                    strText &= "<b>F&oslash;dselsdato:</b> " & strBirthdate & "<br>"
                End If

                ' Check if signature is nothing. If it isn't we shall show more information regarding the person
                If signature <> "" Then
                    ' We will show languages this person know
                    If Not (item.SelectSingleNode("spraak") Is Nothing) Then
                        Dim strTemp As String = item.SelectSingleNode("spraak").Value.ToString()
                        Dim strTemp2 As String = ""
                        Dim arrTemp As Array = strTemp.Split(vbCrLf)
                        Dim strBuilder As String = ""
                        For Each strTemp2 In arrTemp
                            strBuilder &= strTemp2 & "<br>"

                        Next
                        strText &= "<b>Spr&aring;k:</b> " & strBuilder & "<br>"


                        ' strText &= "<b>Språk:</b> " & item.SelectSingleNode("spraak").Value.ToString() & "<p>"
                    End If

                    ' We will show the education this person have
                    If Not (item.SelectSingleNode("utdanning") Is Nothing) Then
                        Dim strTemp As String = item.SelectSingleNode("utdanning").Value.ToString()
                        Dim arrTemp As Array = strTemp.Split(vbCrLf)
                        Dim strTemp2 As String = ""
                        Dim strBuilder As String = ""
                        For Each strTemp2 In arrTemp
                            strBuilder &= strTemp2 & "<br>"

                        Next
                        strText &= "<b>Utdanning:</b> " & strBuilder & "<br>"
                    End If



                    ' We will show the experience this person have
                    If Not (item.SelectSingleNode("erfaring") Is Nothing) Then
                        Dim strTemp As String = item.SelectSingleNode("erfaring").Value.ToString()
                        Dim arrTemp As Array = strTemp.Split(vbCrLf)
                        Dim strTemp2 As String = ""
                        Dim strBuilder As String = ""
                        For Each strTemp2 In arrTemp
                            strBuilder &= strTemp2 & "<br>"
                        Next
                        ' strText &= "<b>Erfaring:</b> " & item.SelectSingleNode("erfaring").Value.ToString() & "<br>"
                        strText &= "<b>Erfaring:</b> " & strBuilder & "<br>"
                    End If

                    ' We will show which interests this person have
                    If Not (item.SelectSingleNode("interesser") Is Nothing) Then
                        ' strText &= "<b>Interesser:</b> " & item.SelectSingleNode("interesser").Value.ToString() & "<br>"
                        Dim strTemp As String = item.SelectSingleNode("interesser").Value.ToString()
                        Dim strTemp2 As String = ""
                        Dim arrTemp As Array = strTemp.Split(vbCrLf)
                        Dim strBuilder As String = ""
                        For Each strTemp2 In arrTemp
                            strBuilder &= strTemp2 & "<br>"
                        Next
                        ' strText &= "<b>Erfaring:</b> " & item.SelectSingleNode("erfaring").Value.ToString() & "<br>"
                        strText &= "<b>Interesser:</b> " & strBuilder & "<br>"
                    End If

                    ' We will show spesial fields/knowledge this person have
                    If Not (item.SelectSingleNode("spesialfelt") Is Nothing) Then
                        strText &= "<b>Spesialfelt:</b> " & item.SelectSingleNode("spesialfelt").Value.ToString() & "<br>"
                    End If

                    ' This is a link to the bigger picture
                    If signature <> "" Then
                        strText &= "<a href='showpicture.aspx?file=" & Trim(item.SelectSingleNode("signatur").Value.ToString()) + ".jpg" & "'>Link til stort bilde</a><br>"
                    End If




                    ' htmlImg.ImageUrl = "dokumenter/Ansattbilder/" + Trim(item.SelectSingleNode("signatur").Value.ToString()) + ".jpg"
                    ' End If

                End If



                If signature = "" Then
                    If strSignatur <> "" Then
                        strText &= "<a href = '?avdeling=" & department & "&sign=" & strSignatur & "'>Mer informasjon</a>"
                    End If

                Else
                    If Request.QueryString("s") = "t" Then
                        strText &= "<a href = 'javascript:history.back(-1);'>Tilbake</a>"
                    Else
                        strText &= "<a href = '?avdeling=" & department & "'>Tilbake</a>"
                    End If

                End If




                If counter = 3 Then
                    rowNew = New TableRow()

                    table1.Controls.Add(rowNew)
                    counter = 0
                End If
                counter = counter + 1

                totalcount += 1

                If signature <> "" Then
                    totalcount = 0
                End If

                If totalcount <= ic Then
                    Dim cellImg As TableCell = New TableCell()
                    cellImg.Width = 100
                    Dim tablestyle As TableItemStyle = New TableItemStyle()
                    tablestyle.HorizontalAlign = HorizontalAlign.Left
                    tablestyle.VerticalAlign = VerticalAlign.Top
                    tablestyle.Font.Size = "9"
                    tablestyle.Font.Name = "Arial, helvetica, sans-serif"


                    htmlImg.ToolTip = "Klikk på bilde og lagre som for å lagre bildet til bruk i f.eks akkrediteringer"


                    cellImg.Controls.Add(htmlImg)

                    Dim cellText As TableCell = New TableCell()
                    If signature <> "" Then
                        cellText.Width = 650
                    Else
                        cellText.Width = 150
                    End If
                    cellText.Text = strText

                    rowNew.ApplyStyle(tablestyle)
                    rowNew.Controls.Add(cellImg)
                    rowNew.Controls.Add(cellText)
                    strText = ""

                End If


                strSignatur = ""


            Next





            ' This code will now only list out all the pictures. It shall in the near future show
            ' the employers related to the choosen page




        Catch nex As NullReferenceException
            Response.Write(nex.Message.ToString())
            Response.Write(nex.StackTrace.ToString())

        Catch ex As Exception
            Response.Write(ex.ToString())

        End Try

    End Sub
End Class
