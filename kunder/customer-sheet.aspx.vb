Imports System.Data.SqlClient
Imports System.Web.Configuration


Namespace intranett

    Class customer_sheet
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim tmp As String = ""

            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("CRMConString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("CRMConString"))
            Dim cd As New SqlCommand("select a.name,c.label,a.address1_name,a.address1_line1,a.address1_postalcode,a.address1_city,a.telephone1,a.websiteurl,a.emailaddress1,firstname,lastname,d.emailaddress1 as cemail, d.telephone1 as ctlf,CFPtar_status_prod1,CFPtar_status_prod2,CFPtar_status_prod3,CFPtar_status_prod4,CFPtar_status_prod5,CFPtar_status_prod6,CFPtar_status_prod7,CFPtar_status_prod8,CFPtar_status_prod9,CFPtar_status_prod10,CFPtar_status_prod11,CFPtar_status_prod12,CFPtar_status_prod13,CFPtar_status_prod14,CFPtar_status_prod15,CFPtar_status_prod16,CFPtar_status_prod17,CFPtar_status_prod18,CFPtar_status_prod19,CFPtar_status_prod20 from (account a join customergroups c on a.CFPNTB_kundegruppe = c.id) left join contact d on d.contactid = a.primarycontactID where a.accountID = '" & Request.QueryString("accountId") & "'", dbNTB)
            Dim dr As SqlDataReader

            dbNTB.Open()
            dr = cd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                Name.Text = dr("Name").ToString()
                group.Text = dr("label").ToString()
                streetadr.Text = dr("address1_name").ToString()
                postadr.Text = dr("address1_line1").ToString()
                poststed.Text = dr("address1_postalcode").ToString() & " " & dr("address1_city").ToString()

                telephone.Text = dr("Telephone1").ToString()
                website.Text = dr("websiteurl").ToString()
                email.Text = dr("emailaddress1").ToString()

                If dr("cemail").ToString.TrimEnd() <> "" Then
                    maincontact.Text = "<a href=""mailto:" & dr("cemail").ToString & """>" & dr("firstname").ToString() & " " & dr("lastname").ToString() & "</a>&nbsp;&nbsp;&nbsp;&nbsp;" & dr("ctlf").ToString()
                Else
                    maincontact.Text = dr("firstname").ToString() & " " & dr("lastname").ToString() & "&nbsp;&nbsp;&nbsp;&nbsp;" & dr("ctlf").ToString()
                End If


                Dim i = 1
                For i = 1 To 20
                    If Not dr("CFPtar_status_prod" & i) Is DBNull.Value Then
                        If dr("CFPtar_status_prod" & i) = 2 Then tmp &= i & ","
                    End If
                Next

            End If
            dr.Close()

            If (tmp <> "") Then
                cd = New SqlCommand("select prodname from products where prodid IN ( " & tmp.TrimEnd(",") & ")", dbNTB)
                dr = cd.ExecuteReader()
                While dr.Read
                    products.Text &= dr("prodname") & "<br>"
                End While
                dr.Close()
            End If


            dbNTB.Close()
        End Sub

    End Class

End Namespace
