﻿using System;
using System.Collections;
using System.Web.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.XPath;

public partial class search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // here we shall do something
        if (this.IsPostBack == true)
        {
            // Get the values from the form

        }
        else
        {
            SearchMethod(Request.QueryString["txtSearch"]);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strSearch = txtSearch.Text;
        SearchMethod(strSearch);

        
    }

    protected void SearchMethod(string strSearch)
    {

        string strSearchLower = "";
        string strSearchQuery = "";
        if (strSearch.Contains(" "))
        {
            String[] stringSearch = strSearch.Split(' ');
            if (stringSearch.Length == 2)
            {
                strSearch = stringSearch[1] + ", " + stringSearch[0];
            }
            else
            {
                strSearchQuery = stringSearch[stringSearch.Length-1] + ", ";
                for (int i=0;i<stringSearch.Length-1; i++)
                {
                    strSearchQuery += stringSearch[i] + " ";
                }

                
            }
            strSearch = strSearchQuery.Trim();
            strSearchLower = strSearchQuery.ToLower().Trim();
        }
        

        XPathDocument Doc = new XPathDocument(WebConfigurationManager.AppSettings.Get("PhoneListXML"));


        XPathNavigator Navigator;
        Navigator = Doc.CreateNavigator();



        String XPathQuer = @"/telefonliste/person[contains(navn, '" + strSearch + "') or contains(signatur, '" + strSearchLower + "') or contains(signatur, '" + strSearch + "') or contains(stilling, '" + strSearch + "') or contains(stilling, '" + strSearchLower + "') or contains(interesser, '" + strSearchLower + "') or contains(interesser, '" + strSearch + "') or contains(spesialfelt, '" + strSearchLower + "') or contains(spesialfelt, '" + strSearch + "') or contains(spraak, '" + strSearchLower + "') or contains(spraak, '" + strSearch + "')]";


        XPathExpression xPathExpr = Navigator.Compile(XPathQuer);
        xPathExpr.AddSort("navn", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Text);

        XPathNodeIterator iterator = Navigator.Select(xPathExpr);

        Label lblResult = new Label();

        if (iterator.Count > 0)
        {

            string strText = "";
            foreach (XPathNavigator item in iterator)
            {

                if (item.SelectSingleNode("navn") != null)
                {
                    string strNavn = item.SelectSingleNode("navn").Value.ToString();
                    string strSignature = "";
                    if (item.SelectSingleNode("signatur") != null)
                    {
                        strSignature = item.SelectSingleNode("signatur").Value.ToString();
                    }

                    string strAvdeling = "";
                    if (item.SelectSingleNode("avdeling") != null)
                    {
                        strAvdeling = item.SelectSingleNode("avdeling").Value.ToString();
                    }
                    if (strNavn.IndexOf(",") > 0)
                    {
                        // Array tmpNavn = strNavn.Split(@",").ToString();
                        string[] tmpNavn = strNavn.Split(',');
                        strNavn = tmpNavn[1].Trim() + " " + tmpNavn[0].Trim();
                        strText = "<a href=\"../SectionPictures.aspx?avdeling=" + strAvdeling + "&sign=" + strSignature + "&s=t\">" + strNavn + "</a><br>";

                        //                Dim pageUrl As String = Request.QueryString("avdeling")
                        //Dim sign As String = Request.QueryString("sign")
                    }
                    else
                    {
                        strText = "<strong>" + strNavn + "</strong><br>";
                    }

                    lblResult.Text += strText;
                }
                else
                {

                }

            }
        }
        else
        {
            lblResult.Text += "Dit søk: <strong style='color: red;'>" + strSearch + "</strong> resulterte ikke i noen treff";
        }
        lblResult.Visible = true;
        Search.Controls.Add(lblResult);

    }
}
