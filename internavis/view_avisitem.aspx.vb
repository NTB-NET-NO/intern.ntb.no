Imports System.Data.SqlClient
Imports System.Web.Configuration


Namespace intranett


    Partial Class view_avisitem

        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Protected topic_id As Integer

        Protected topicid As Integer
        Protected dt As DateTime
        Protected name As String
        Protected subject As String
        Protected content As String
        Protected count As Integer

        Protected comments As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            ' Dim dbForum As New SqlConnection(ConfigurationSettings.AppSettings("forumConString"))
            Dim dbForum As New SqlConnection(WebConfigurationManager.AppSettings("forumConString"))
            dbForum.Open()

            If Request.QueryString("TID") <> "" And Request.QueryString("TID") > 0 Then
                topic_id = Request.QueryString("TID")
            Else
                Dim cd2 As New SqlCommand
                cd2.Connection = dbForum
                cd2.CommandText = "internTopicItems"
                cd2.CommandType = CommandType.StoredProcedure

                cd2.Parameters.Add(New SqlParameter("@count", SqlDbType.Int))
                cd2.Parameters.Item("@count").Value = 1

                cd2.Parameters.Add(New SqlParameter("@forumID", SqlDbType.Int))
                cd2.Parameters.Item("@forumID").Value = 6

                cd2.Parameters.Add(New SqlParameter("@sortDesc", SqlDbType.Int))
                cd2.Parameters.Item("@sortDesc").Value = 1

                cd2.Parameters.Add(New SqlParameter("@topicOnly", SqlDbType.Int))
                cd2.Parameters.Item("@topicOnly").Value = 1

                Dim reader As SqlDataReader

                reader = cd2.ExecuteReader()
                If reader.HasRows() Then
                    reader.Read()
                    topic_id = reader("topic_id")
                End If
                reader.Close()

            End If

            Dim cd As New SqlCommand
            cd.Connection = dbForum
            cd.CommandText = "internTopicItems"
            cd.CommandType = CommandType.StoredProcedure

            cd.Parameters.Add(New SqlParameter("@forumID", SqlDbType.Int))
            cd.Parameters.Item("@forumID").Value = 6

            cd.Parameters.Add(New SqlParameter("@sortDesc", SqlDbType.Int))
            cd.Parameters.Item("@sortDesc").Value = 0

            cd.Parameters.Add(New SqlParameter("@topicOnly", SqlDbType.Int))
            cd.Parameters.Item("@topicOnly").Value = 0

            cd.Parameters.Add(New SqlParameter("@tid", SqlDbType.Int))
            cd.Parameters.Item("@tid").Value = topic_id

            Dim dr As SqlDataReader

            dr = cd.ExecuteReader()
            If dr.HasRows() Then
                dr.Read()

                topicid = dr("Topic_ID")
                dt = dr("start_date")
                subject = dr("subject")
                name = dr("real_name")
                content = dr("message")
                count = dr("No_Of_Views")
                count += 1

                'Image URL fix
                content = content.Replace("src=""uploads", "src=""/forum/uploads")
                content = content.Replace("src=""smileys", "src=""/forum/smileys")
            End If

            While dr.Read()
                Dim cmname As String = ""
                If Not dr.IsDBNull(dr.GetOrdinal("real_name")) Then
                    cmname = dr("real_name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("name")) And cmname = "" Then
                    cmname = dr("name")
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("username")) And cmname = "" Then
                    cmname = dr("username")
                End If

                comments &= dr("message_date") & " - " & cmname & " <br> " & System.Text.RegularExpressions.Regex.Replace(dr("message"), "<[^>]+>", "") & " <br><br> " & vbCrLf
            End While

            If comments = "" Then
                comments = "Det finnes ingen kommentarer til denne saken"
            End If

            dr.Close()

            'Update read count
            Dim cdCount As New SqlCommand
            cdCount.Connection = dbForum
            cdCount.CommandText = "wwfSpUpdateViewPostCount"
            cdCount.CommandType = CommandType.StoredProcedure

            cdCount.Parameters.Add(New SqlParameter("@lngNumberOfViews", SqlDbType.Int))
            cdCount.Parameters.Item("@lngNumberOfViews").Value = count

            cdCount.Parameters.Add(New SqlParameter("@lngTopicID", SqlDbType.Int))
            cdCount.Parameters.Item("@lngTopicID").Value = topicid

            cdCount.ExecuteNonQuery()

            dbForum.Close()
        End Sub

    End Class

End Namespace
