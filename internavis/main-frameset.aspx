<%@ Page Language="vb" AutoEventWireup="false" Inherits="intranett.main_frameset" CodeFile="main-frameset.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>intern.ntb.no</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
	<frameset rows="40,*" frameborder="0">
		<frame name="internavis_top" src="../section.aspx?section=INTERNAVIS_TOP" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" noresize>

		<frameset cols="170,505,*" border="0" frameborder="0" framespacing="0">
			<frame name="internavis_list" src="internavis_menu.aspx" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
			<frame name="internavis_main" src="view_avisitem.aspx?TID=<%= thread_ID %>" marginwidth="0" marginheight="0" scrolling="auto" frameborder="0" noresize>
		</frameset>
	
	</frameset>
</html>
