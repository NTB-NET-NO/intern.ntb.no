<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



%>	<tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
        </tr>
	<tr>
         <td align="right" height="22" class="text"><% = strTxtPollQuestion %>*:</td>
         <td height="22"><input name="pollQuestion" type="text" size="30" maxlength="70" /></td>
        </tr><%

'Loop around to display text boxes for the maximum amount of allowed poll questions
For intPollLoopCounter = 1 to intMaxPollChoices


	Response.Write(vbCrLf & "        <tr>" & _
        vbCrLf & "	 <td align=""right"" height=""22"" class=""text"">")

        'Display the poll choice text
        Response.Write(strTxtPollChoice & " ")

	'Display poll number
	Response.Write(intPollLoopCounter)

	'If this is choice 1 or 2 display a required astrerix
	If intPollLoopCounter < 3 Then Response.Write("*")

	Response.Write(":</td>" & _
	vbCrLf & "	<td height=""22""><input name=""choice" & intPollLoopCounter & """ type=""text"" size=""30"" maxlength=""60"" /></td>" & _
	vbCrLf & "        </tr>")
Next

%>	<tr>
         <td align="right" height="22" class="text">&nbsp;</td>
         <td height="22" class="text">&nbsp;<input type="checkbox" name="multiVote" value="True" /><% = strTxtAllowMultipleVotes %></td>
        </tr>
        <tr>
         <td align="right" height="22" class="text">&nbsp;</td>
         <td height="22" class="text">&nbsp;<input type="checkbox" name="pollReply" value="True" /><% = strTxtMakePollOnlyNoReplies %></td>
        </tr>
        <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
        </tr>