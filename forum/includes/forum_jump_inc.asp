<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Declare variables
Dim rsForumJumpForum		'Holds the recordset for the forum
Dim strJumpCatName		'Holds the name of the category
Dim intJumpCatID		'Holds the ID number of the category
Dim strJumpForumName		'Holds the name of the forum to jump to
Dim lngJumpFID			'Holds the forum id to jump to
Dim intReadPermission		'Holds the generic read permssions level on them forum

'These are used as tmep store for forum permissions as this will change them
Dim blnTempRead
Dim blnTempPost
Dim blnTempReply
Dim blnTempEdit
Dim blnTempDelete
Dim blnTempPriority
Dim blnTempPollCreate
Dim blnTempVote
Dim blnTempAttachments
Dim blnTempImageUpload 

'Put the forum permissions into the temp store while looping through the other forums
blnTempRead = blnRead
blnTempPost = blnPost
blnTempReply = blnReply
blnTempEdit = blnEdit
blnTempDelete = blnDelete
blnTempPriority = blnPriority
blnTempPollCreate = blnPollCreate
blnTempVote = blnVote
blnTempAttachments = blnAttachments
blnTempImageUpload = blnImageUpload


Response.Write(vbCrLf & "	<span class=""text"">" & strTxtForumJump & "</span>")
Response.Write(vbCrLf & "	 <select onChange=""ForumJump(this)"" name=""SelectJumpForum"">")
Response.Write(vbCrLf & "           <option value="""" selected>-- " & strTxtSelectForum & " --</option>")


'Create a recordset to hold the forum name and id number
Set rsForumJumpForum = Server.CreateObject("ADODB.Recordset")


'Read in the category name from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "CategoryAll"
Else
	strSQL = "SELECT " & strDbTable & "Category.Cat_name, " & strDbTable & "Category.Cat_ID FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'Loop through all the categories in the database
Do while NOT rsCommon.EOF 

	'Read in the deatils for the category
	strJumpCatName = rsCommon("Cat_name")
	intJumpCatID = Cint(rsCommon("Cat_ID"))	
	
	'Display a link in the link list to the forum
	Response.Write vbCrLf & "		<option value="""">" & strJumpCatName & "</option>"

	'Read in the forum name from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereCatIs @intCatID = " & intJumpCatID
	Else
		strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = " & intJumpCatID & " ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"
	End If
	
	'Query the database
	rsForumJumpForum.Open strSQL, adoCon
	
	'Loop through all the froum in the database
	Do while NOT rsForumJumpForum.EOF 
	
		'Read in the forum details from the recordset
		strJumpForumName = rsForumJumpForum("Forum_name")
		lngJumpFID = CLng(rsForumJumpForum("Forum_ID"))
		intReadPermission = CInt(rsForumJumpForum("Read"))

		'Call the function to check the users security level within this forum
		Call forumPermisisons(lngJumpFID, intGroupID, intReadPermission, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		'If the user can view the forum then display it in the forum jump box
		If blnRead OR blnAdmin OR blnModerator Then
			'Display a link in the link list to the forum
			Response.Write vbCrLf & "		<option value=""forum_topics.asp?FID=" & lngJumpFID & """>&nbsp;&nbsp;-&nbsp;" & strJumpForumName & "</option>"		
		End If
				
		'Move to the next record in the recordset
		rsForumJumpForum.MoveNext
	Loop
	
	'Close the forum recordset so another can be opened
	rsForumJumpForum.Close
	
	'Move to the next record in the recordset
	rsCommon.MoveNext
Loop

'Reset Server Objects
rsCommon.Close
Set rsForumJumpForum = Nothing


'Put the forum permissions back
blnRead = blnTempRead
blnPost = blnTempPost
blnReply = blnTempReply
blnEdit = blnTempEdit
blnDelete = blnTempDelete
blnPriority = blnTempPriority
blnPollCreate = blnTempPollCreate
blnVote = blnTempVote
blnAttachments = blnTempAttachments
blnImageUpload = blnTempImageUpload

Response.Write(vbCrLf & "	</select>")

%>