<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Function to build SQL query's for seach all or any words
Function BuildSQL (strTable, sarySearchWord)

	'Initilaise variables
	Dim intSQLLoopCounter


	'Initilaise variables
	intSQLLoopCounter = 0

	'Search for the first search word
	BuildSQL = BuildSQL & "(" & strTable & " LIKE '%" & sarySearchWord(0) & "%'"

	'Loop to search for each search word entered by the user
	For intSQLLoopCounter = 1 To UBound(sarySearchWord)

		'If the search is for all words then place AND between the words to be serached
		If strSearchMode = "1" Then
			BuildSQL = BuildSQL & " AND "

		'Else the user has choosen to search for any words so place OR between the words to be serached
		Else
			BuildSQL = BuildSQL & " OR "
		End If

		'Place the search word in the query
		BuildSQL = BuildSQL & strTable & " LIKE '%" & sarySearchWord(intSQLLoopCounter) & "%'"
	Next

	'Close the end of the search words in the SQL query by closing the bracket
	BuildSQL = BuildSQL & ")"
End Function




'Dimension variables
Dim rsTopic			'Holds the Recordset for the Topic details
Dim strSQLResultsQuery		'Holds the query built for the search
Dim intForumID			'Holds the forum ID number
Dim strForumName		'Holds the forum name
Dim strSearchKeywords		'Holds the keywords to search for
Dim sarySearchWord		'Array to hold the search words
Dim strSearchIn			'Holds where the serach is to be done
Dim intSearchForumID		'Holds the forum the result belongs to
Dim lngNumberOfReplies		'Holds the number of replies for a topic
Dim lngTopicID			'Holds the topic ID
Dim strSubject			'Holds the topic subject
Dim strTopicStartUsername 	'Holds the username of the user who started the topic
Dim lngTopicStartUserID		'Holds the users Id number for the user who started the topic
Dim lngNumberOfViews		'Holds the number of views a topic has had
Dim lngLastEntryMessageID	'Holds the message ID of the last entry
Dim strLastEntryUsername	'Holds the username of the last person to post a message in a topic
Dim lngLastEntryUserID		'Holds the user's ID number of the last person to post a meassge in a topic
Dim dtmLastEntryDate		'Holds the date the last person made a post in the topic
Dim intRecordPositionPageNum	'Holds the recorset page number to show the topics for
Dim intTotalNumOfPages		'Holds the total number of pages in the recordset
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intLinkPageNum		'Holss the page number to link to
Dim blnReturnedSearchResults	'Set to true if there are search results returned
Dim intTopicPageLoopCounter	'Holds the number of pages there are in the forum
Dim blnTopicLocked		'set to true if the topic is locked
Dim intPriority			'Holds the priority level of the topic
Dim intNumberOfTopicPages	'Holds the number of topic pages
Dim intTopicPagesLoopCounter	'Holds the number of loops
Dim intHighlightLoopCounter	'Loop counter to loop through words and hightlight them
Dim blnNewPost			'Set to true if the post is a new post since the users last visit
Dim lngPollID			'Holds the topic poll id number
Dim strSearchMode		'Holds the search mode
Dim dtmFirstEntryDate		'Holds the date of the first message



'Initialise variables
blnReturnedSearchResults = True



'Read in the search criteria
strSearchKeywords = Trim(Mid(Request.QueryString("KW"), 1, 35))
strSearchIn = Trim(Mid(Request.QueryString("SI"), 1, 3))
intForumID = CInt(Request.QueryString("FM"))
strSearchMode = Trim(Mid(Request.QueryString("SM"), 1, 3))


'Replace _ with a space so that these words are split into multiple words
If strSearchIn <> "AR" Then
	strSearchKeywords = Replace(strSearchKeywords, "_", " ")
End If

'Filter the search words with the same filters as the text is saved with
strSearchKeywords = formatSQLInput(strSearchKeywords)


'Split up the keywords to be searched
sarySearchWord = Split(Trim(strSearchKeywords), " ")


'If there is no keywords to search for then redirect to the forum homepage
If strSearchKeywords = "" Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect "default.asp"
End If

'If this is the first time the page is displayed then the Forum Topic record position is set to page 1
If Request.QueryString("SPN") = 0 Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the Forum Topic record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("SPN"))
End If

'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)


'If the user has selected to search in the Topic subjects then build the Where Clause of the Reseults Query with the Topics containg threads with the search words
'Search in topic subject
If strSearchIn = "TC"  Then

	'Make the search words the same as encoded stuff
	strSearchKeywords = removeAllTags(strSearchKeywords)
	strSearchKeywords = formatInput(strSearchKeywords)

	'Initalise the Results Query string with the select part of the query
	strSQLResultsQuery = "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject "
	strSQLResultsQuery = strSQLResultsQuery & "FROM " & strDbTable & "Topic WHERE "


	'If the user has selected to search the TOPICS and any words or all words then build the next part of the Results Query with the where cluase
	If (strSearchMode = "2" OR strSearchMode = "1") AND strSearchIn = "Topic" Then

		'Call the function to build the query
		strSQLResultsQuery = strSQLResultsQuery & BuildSQL ("" & strDbTable & "Topic.Subject", sarySearchWord)

	'Else they have choosen 3 in topic
	Else
		strSQLResultsQuery = strSQLResultsQuery & "" & strDbTable & "Topic.Subject LIKE '%" & strSearchKeywords & "%'"
	End If

	If intForumID <> 0 Then
		'If the user has selected to search a certain forum then build the Results Query to only look in that forum
		strSQLResultsQuery = strSQLResultsQuery & " AND " & strDbTable & "Topic.Forum_ID =" & intForumID & " "
	End If
End If



'If the user has selected to search in the message body then build the Where Clause of the Reseults Query with the Topics containg threads with the search words
'Search in posts
If strSearchIn = "PT"  Then

	'Initialise the sql query to get the with a select statment to get the topic ID
	strSQLResultsQuery = "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject "
	strSQLResultsQuery = strSQLResultsQuery & "FROM " & strDbTable & "Topic "
	strSQLResultsQuery = strSQLResultsQuery & "WHERE " & strDbTable & "Topic.Topic_ID IN "
	strSQLResultsQuery = strSQLResultsQuery & "	(SELECT " & strDbTable & "Thread.Topic_ID "
	strSQLResultsQuery = strSQLResultsQuery & "	FROM " & strDbTable & "Thread "
	strSQLResultsQuery = strSQLResultsQuery & "	WHERE ( "

	'If the user has selected all or any words then build the where clause with the words to be searched
	If strSearchMode = "2" OR strSearchMode = "1" Then

		'Call the function to build the query
		strSQLResultsQuery = strSQLResultsQuery & BuildSQL ("" & strDbTable & "Thread.Message", sarySearchWord)

	'Else the user has choosen to only search for a message containg the 3
	Else
		strSQLResultsQuery = strSQLResultsQuery & "" & strDbTable & "Thread.Message LIKE '%" & strSearchKeywords & "%'"

	End If

	strSQLResultsQuery = strSQLResultsQuery & ")) "

	If intForumID <> 0 Then
		'If the user has selected to search a certain forum then intitilaise the SQL query to search only that forum
		strSQLResultsQuery = strSQLResultsQuery & " AND (" & strDbTable & "Topic.Forum_ID=" & intForumID & ")"
	End If
End If


'MOD:  LCH
'Search in complete text (subject _and_ body)
If strSearchIn = "TXT"  Then

	'Initialise the sql query to get the with a select statment to get the topic ID
	strSQLResultsQuery = "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject "
	strSQLResultsQuery = strSQLResultsQuery & "FROM " & strDbTable & "Topic "
	strSQLResultsQuery = strSQLResultsQuery & "WHERE (" & strDbTable & "Topic.Topic_ID IN "
	strSQLResultsQuery = strSQLResultsQuery & "	(SELECT " & strDbTable & "Thread.Topic_ID "
	strSQLResultsQuery = strSQLResultsQuery & "	FROM " & strDbTable & "Thread "
	strSQLResultsQuery = strSQLResultsQuery & "	WHERE ( "

	'If the user has selected all or any words then build the where clause with the words to be searched
	If strSearchMode = "2" OR strSearchMode = "1" Then

		'Call the function to build the query
		strSQLResultsQuery = strSQLResultsQuery & BuildSQL ("" & strDbTable & "Thread.Message", sarySearchWord)

	'Else the user has choosen to only search for a message containg the 3
	Else
		strSQLResultsQuery = strSQLResultsQuery & "" & strDbTable & "Thread.Message LIKE '%" & strSearchKeywords & "%'"

	End If

	'-------- MOD ------------

	strSQLResultsQuery = strSQLResultsQuery & ") ) OR "


	'If the user has selected all or any words then build the where clause with the words to be searched
	If strSearchMode = "2" OR strSearchMode = "1" Then

		'Call the function to build the query
		strSQLResultsQuery = strSQLResultsQuery & BuildSQL ("" & strDbTable & "Topic.Subject", sarySearchWord)

	'Else the user has choosen to only search for a message containg the 3
	Else
		strSQLResultsQuery = strSQLResultsQuery & "" & strDbTable & "Topic.Subject LIKE '%" & strSearchKeywords & "%'"

	End If

	strSQLResultsQuery = strSQLResultsQuery & ") "

	'-------------------------

	If intForumID <> 0 Then
		'If the user has selected to search a certain forum then intitilaise the SQL query to search only that forum
		strSQLResultsQuery = strSQLResultsQuery & " AND (" & strDbTable & "Topic.Forum_ID=" & intForumID & ")"
	End If

End If



'If the user has selected to search in the message body then build the Where Clause of the Reseults Query with the Topics containg threads written by the author
'Search by author
If strSearchIn = "AR" Then

	'Take out parts of the username that are not permitted
	strSearchKeywords = disallowedMemberNames(strSearchKeywords)

	'Get rid of milisous code
	strSearchKeywords = formatSQLInput(strSearchKeywords)

	'Initalise the strSQL variable with an SQL statement to query the database
	strSQLResultsQuery = "SELECT DISTINCT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Last_entry_date, " & strDbTable & "Topic.Start_date "
	strSQLResultsQuery = strSQLResultsQuery & "FROM " & strDbTable & "Topic INNER JOIN (" & strDbTable & "Author INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID) ON " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID "
	strSQLResultsQuery = strSQLResultsQuery & "WHERE (((" & strDbTable & "Author.Username) LIKE '" & strSearchKeywords & "') "
	If intForumID <> 0 Then
		strSQLResultsQuery = strSQLResultsQuery & "AND ((" & strDbTable & "Topic.Forum_ID)=" & intForumID & ") "
	End If
	strSQLResultsQuery = strSQLResultsQuery & ") "
End If



'Tell the Results Query what order to place the results in
Select Case Trim(Mid(Request.QueryString("OB"), 1, 3))
	Case "3"
		strSQLResultsQuery = strSQLResultsQuery & " ORDER BY " & strDbTable & "Topic.Subject ASC;"
	Case "4"
		strSQLResultsQuery = strSQLResultsQuery & " ORDER BY " & strDbTable & "Topic.No_of_views DESC;"
	Case "2"
		strSQLResultsQuery = strSQLResultsQuery & " ORDER BY " & strDbTable & "Topic.Start_date ASC;"
	Case Else
		strSQLResultsQuery = strSQLResultsQuery & " ORDER BY " & strDbTable & "Topic.Last_entry_date DESC;"
End Select

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title><% = strMainForumName %> Search Results: <% = strSearchKeywords %></title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
<tr>
  <td align="left" class="heading"><% = strTxtSearchResults %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><%

Response.Write(strNavSpacer)

'Read in the forum name from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Forum.Forum_name FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID

'Query the database
rsCommon.Open strSQL, adoCon


'Check there are forum's to display
If rsCommon.EOF AND NOT intForumID = 0 Then

	'If there are no forum's to display then display the appropriate error message
	Response.Write vbCrLf & "<span class=""bold"">" & strTxtNoForums & "</span>"

'Else there the are forum's to write the HTML to display it the forum names and a discription
Else

	'Write the HTML of the forum descriptions as hyperlinks to the forums
	If intForumID > 0 Then
		'Read in the forum name
		strForumName = rsCommon("Forum_name")

		'Display the forum name
		Response.Write vbCrLf & "<a href=""forum_topics.asp?FID=" & intForumID & """ target=""_self"" class=""boldLink"">" & strForumName & "</a>" & strNavSpacer
	End If
%><a href="search.asp?KW=<% = Server.URLEncode(Request.QueryString("KW")) %>&SM=<% = strSearchMode %>&SI=<% = strSearchIn %>&FM=<% = intForumID %>&OB=<% = Trim(Mid(Request.QueryString("OB"), 1, 3)) %>" target="_self"  class="boldLink"><% = strTxtSearchResults %></a>
   <%
End If


'Clean up
rsCommon.Close
%>
  </td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr>
  <td width="90%" class="lgText" colspan="2"><%

'Get the Topics for the forum from the database

'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
rsCommon.CursorType = 1

'Query the database
rsCommon.Open strSQLResultsQuery, adoCon

'Set the number of records to display on each page
rsCommon.PageSize = intTopicPerPage



'If ther are records found say how many
If NOT rsCommon.EOF Then
	Response.Write (strTxtYourSearchFor & "&nbsp;'" & strSearchKeywords & "'&nbsp;" & strTxtHasFound & "&nbsp;" & rsCommon.RecordCount & "&nbsp;" & strTxtResults)
End If

  %></td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>"><%

'If there are no results display an error msg
If rsCommon.EOF Then

	Response.Write vbCrLf & "<tr><td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" align=""center"" height=""150"" class=""text"">" & strTxtNoSearchResults
	Response.Write vbCrLf & "<br /><br /><a href=""search_form.asp?RP=S&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&SM=" & strSearchMode & "&SI=" & strSearchIn & "&FM=" & intForumID & "&OB=" & Trim(Mid(Request.QueryString("OB"), 1, 3))  & "&SPN=1" & """ target=""_self"">" & strTxtClickHereToRefineSearch & "</a></td></tr>"

'If there the are topic's so write the HTML to display the topic names and a discription
Else
%>
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" width="3%" class="tHeading" background="<% = strTableTitleBgImage %>">&nbsp;</td>
     <td bgcolor="<% = strTableTitleColour %>" width="41%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtTopics %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="15%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtThreadStarter %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtReplies %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtViews %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="29%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLastPost %></td>
    </tr><%


	'Get the record poistion to display from
	rsCommon.AbsolutePage = intRecordPositionPageNum

	'Count the number of pages there are in the recordset calculated by the PageSize attribute set above
	intTotalNumOfPages = rsCommon.PageCount

	'Create a Recodset object for the topic details
	Set rsTopic = Server.CreateObject("ADODB.Recordset")


	'Loop round to read in all the Topics in the database
	For intRecordLoopCounter = 1 to intTopicPerPage

		'If there are no records left in the recordset to display then exit the for loop
		If rsCommon.EOF Then Exit For

		'Read in Topic details from the database
		intSearchForumID = CInt(rsCommon("Forum_ID"))
		lngTopicID = CLng(rsCommon("Topic_ID"))
		lngPollID = CLng(rsCommon("Poll_ID"))
		lngNumberOfViews = CLng(rsCommon("No_of_views"))
		strSubject = rsCommon("Subject")

		'Make search words in the subject highlighted
		For intHighlightLoopCounter = 0 To UBound(sarySearchWord)
			'Replace the search words with highlited ones
			strSubject = Replace(strSubject, sarySearchWord(intHighlightLoopCounter), "<span class=""highlight"">" & sarySearchWord(intHighlightLoopCounter) & "</span>", 1, -1, 1)
		Next

		'Query the database to get if the topic is locked
		strSQL = "SELECT TOP 1 " & strDbTable & "Topic.Locked, " & strDbTable & "Topic.Priority FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"

		'Query the database
		rsTopic.Open strSQL, adoCon

		'Read in if the topic is locked
		blnTopicLocked = CBool(rsTopic("Locked"))
		intPriority = CInt(rsTopic("Priority"))

		'Close the recordset
		rsTopic.Close

		'Initalise the strSQL variable with an SQL statement to query the database to get the Author and subject from the database for the topic
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "LastAndFirstThreadAuthor @lngTopicID = " & lngTopicID
		Else
			strSQL = "SELECT " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Author_ID, " & strDbTable & "Thread.Message_date, " & strDbTable & "Author.Username "
			strSQL = strSQL & "FROM " & strDbTable & "Author INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID "
			strSQL = strSQL & "WHERE " & strDbTable & "Thread.Topic_ID = " & lngTopicID & " "
			strSQL = strSQL & "ORDER BY " & strDbTable & "Thread.Message_date ASC;"
		End If

		'Set the cursor type property of the record set to forward only so we can navigate through the record set
		rsTopic.CursorType = 1

		'Query the database
		rsTopic.Open strSQL, adoCon

		'If there is info in the database relating to the topic then get them from the record set
		If NOT rsTopic.EOF Then

			'Read in the subject and author and number of replies from the record set
			strTopicStartUsername = rsTopic("Username")
			lngTopicStartUserID = CLng(rsTopic("Author_ID"))
			lngNumberOfReplies = CLng((rsTopic.RecordCount) - 1)
			dtmFirstEntryDate = CDate(rsTopic("Message_date"))

			'Move to the last record in the record set to get the date and username of the last entry
			rsTopic.MoveLast

			'Read in the username and date of the last entry from the record set
			lngLastEntryMessageID = CLng(rsTopic("Thread_ID"))
			strLastEntryUsername = rsTopic("Username")
			lngLastEntryUserID = CLng(rsTopic("Author_ID"))
			dtmLastEntryDate = CDate(rsTopic("Message_date"))
		End If

		'Set the booleon varible if this is a new post since the users last visit and has not been read
		If (CDate(Session("dtmLastVisit")) < dtmLastEntryDate) AND (Request.Cookies("RT")("TID" & lngTopicID) = "") Then

			blnNewPost = True

		'Else this is not a new post so don't set the booleon to true
		Else
			blnNewPost = False
		End If


		'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
		%>
    <tr>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="3%" align="center"><%

     	 	'If the topic is pinned then display the pinned icon
     	 	If intPriority = 1 Then
     	 		Response.Write("<img src=""" & strImagePath & "pinned_topic_icon.gif"" border=""0"" alt=""" & strTxtPinnedTopic & """>")

     	 	'If the topic is top priorty and locked then display top priporty locked icon
     	 	ElseIf blnTopicLocked = True AND intPriority > 0 Then
     	 	 	Response.Write("<img src=""" & strImagePath & "priority_post_locked_icon.gif"" border=""0"" alt=""" & strTxtHighPriorityPostLocked & """>")

     		'If the topic is top priorty then display top priporty icon
     	 	ElseIf intPriority > 0 Then
     			Response.Write("<img src=""" & strImagePath & "priority_post_icon.gif"" border=""0"" alt=""" & strTxtHighPriorityPost & """>")

     		'If the topic is closed display a closed topic icon
     	 	ElseIf blnTopicLocked = True Then
     			Response.Write("<img src=""" & strImagePath & "closed_topic_icon.gif"" border=""0"" alt=""" & strTxtLockedTopic & """>")

     		'If the topic is a hot topic and with new replies then display hot to new replies icon
     	 	ElseIf (lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews) AND (blnNewPost = True) Then
     			Response.Write("<img src=""" & strImagePath & "hot_topic_new_posts_icon.gif"" border=""0"" alt=""" & strTxtHotTopicNewReplies & """>")

     		'If this is a hot topic that contains a poll then display the hot topic poll icon
		ElseIf (lngPollID > 0) AND (lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews) Then
     			Response.Write("<img src=""" & strImagePath & "hot_topic_poll_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")

     		'If the topic is a hot topic display hot topic icon
     	 	ElseIf lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews Then
     			Response.Write("<img src=""" & strImagePath & "hot_topic_no_new_posts_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")

     		'If the topic is has new replies display new replies icon
     	 	ElseIf blnNewPost = True Then
     			Response.Write("<img src=""" & strImagePath & "new_posts_icon.gif"" border=""0"" alt=""" & strTxtOpenTopicNewReplies & """>")

     		'If there is a poll in the post display the poll post icon
		ElseIf lngPollID > 0 Then
     			Response.Write("<img src=""" & strImagePath & "poll_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")

     		'Display topic icon
     		Else
     			Response.Write("<img src=""" & strImagePath & "no_new_posts_icon.gif"" border=""0"" alt=""" & strTxtOpenTopic & """>")
     		End If
     %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="41%" class="text"><%



		 'If the user is the forum admin or a moderator then give let them delete the topic
		 If blnAdmin = True OR blnModerator = True Then

		  	Response.Write("      <a href=""javascript:openWin('pop_up_topic_admin.asp?TID=" & lngTopicID & "','admin','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')""><img src=""" & strImagePath & "small_admin_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtTopicAdmin & """></a>")
		End If

		'If there is a poll display a poll text
		If lngPollID <> 0 Then Response.Write(strTxtPoll)

		 'Disply link to topic and subject
		 Response.Write("	<a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & """ target=""_self"" title=""" & strTxtThisTopicWasStarted & DateFormat(dtmFirstEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmFirstEntryDate, saryDateTimeData) & """>" & strSubject & "</a>")


		 'Calculate the number of pages for the topic and display links if there are more than 1 page
		 intNumberOfTopicPages = ((lngNumberOfReplies + 1)\intThreadsPerPage)

		 'If there is a remainder from calculating the num of pages add 1 to the number of pages
		 If ((lngNumberOfReplies + 1) Mod intThreadsPerPage) > 0 Then intNumberOfTopicPages = intNumberOfTopicPages + 1

		 'If there is more than 1 page for the topic display links to the other pages
		 If intNumberOfTopicPages > 1 Then
		 	Response.Write("<br /><img src=""" & strImagePath & "pages_icon.gif"" align=""middle"" alt=""" & strTxtPages & """>")

		 	'Loop round to display the links to the other pages
		 	For intTopicPagesLoopCounter = 1 To intNumberOfTopicPages

		 		'If there is more than 7 pages display ... last page and exit the loop
		 		If intTopicPagesLoopCounter > 7 Then

		 			'If this is position 8 then display just the 8th page
		 			If intNumberOfTopicPages = 8 Then
		 				Response.Write(" <a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&TPN=8")
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">8</a>")

					'Else display the last 2 pages
					Else

						Response.Write(" ...")

						Response.Write(" <a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&TPN=" & intNumberOfTopicPages - 1)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">" & intNumberOfTopicPages - 1 & "</a>")

			 			Response.Write(" <a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&TPN=" & intNumberOfTopicPages)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">" & intNumberOfTopicPages & "</a>")

					End If

		 			Exit For
		 		End If

		 		'Display the links to the other pages
		 		Response.Write(" <a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&TPN=" & intTopicPagesLoopCounter)
		 		'If a priority topic need to make sure we don't change forum
		 		If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
		 		Response.Write(""" target=""_self"" class=""smLink"">" & intTopicPagesLoopCounter & "</a>")
		 	Next
		 End If
		  %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="15%" class="text"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngTopicStartUserID %>&FID=<% = intSearchForumID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')" title="<% Response.Write(strTxtThisTopicWasStarted & DateFormat(dtmFirstEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmFirstEntryDate, saryDateTimeData)) %>"><% = strTopicStartUsername %></a></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="7%" align="center" class="text"><% = lngNumberOfReplies %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="7%" align="center" class="text"><% = lngNumberOfViews %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="29%" align="right" class="smText" nowrap="nowrap"><% = DateFormat(dtmLastEntryDate, saryDateTimeData) %>&nbsp;<% = strTxtAt %>&nbsp;<% = TimeFormat(dtmLastEntryDate, saryDateTimeData) %>
      <br /><% = strTxtBy %>&nbsp;<a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngLastEntryUserID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')" class="smLink"><% = strLastEntryUsername %></a> <a href="forum_posts.asp?TID=<% = lngTopicID %>&get=last#<% = lngLastEntryMessageID %>" target="_self"><img src="<% = strImagePath %>right_arrow.gif" align="absmiddle" border="0" alt="<% = strTxtViewLastPost %>"></a></td>
    </tr>
    <%

		'Close the topic recordset
                rsTopic.Close

		'Move to the next database record
		rsCommon.MoveNext
	Next
End If

rsCommon.Close
        %>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<br />
<form>
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
  <tr>
   <td><!-- #include file="includes/forum_jump_inc.asp" --></td>
   <%
'If there is more than 1 page of topics then dispaly drop down list to the other topics
If intTotalNumOfPages > 1 Then

	'Display an image link to the last topic
	Response.Write (vbCrLf & "		<td align=""right"" class=""text"">")

	'Display a prev link if previous pages are available
	If intRecordPositionPageNum > 1 Then Response.Write("<a href=""search.asp?KW=" & Server.URLEncode(Request.QueryString("KW")) & "&SM=" & strSearchMode & "&SI=" & strSearchIn & "&FM=" & intForumID & "&OB=" & Trim(Mid(Request.QueryString("OB"), 1, 3)) & "&SPN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")

	Response.Write (strTxtPage & " " & _
	vbCrLf & "		  <select onChange=""ForumJump(this)"" name=""SelectTopicPage"">")

	'Loop round to display links to all the other pages
	For intTopicPageLoopCounter = 1 to intTotalNumOfPages

		'Display a link in the link list to the another topic page
		Response.Write (vbCrLf & "		  <option value=""search.asp?KW=" & Server.URLEncode(Request.QueryString("KW")) & "&SM=" & strSearchMode & "&SI=" & strSearchIn & "&FM=" & intForumID & "&OB=" & Trim(Mid(Request.QueryString("OB"), 1, 3)) & "&SPN=" & intTopicPageLoopCounter & """")

		'If this page number to display is the same as the page being displayed then make sure it's selected
		If intTopicPageLoopCounter = intRecordPositionPageNum Then
			Response.Write (" selected")
		End If

		'Display the link page number
		Response.Write (">" & intTopicPageLoopCounter & "</option>")

	Next

	'End the drop down list
	Response.Write (vbCrLf & "		</select> " & strTxtOf & " " & intTotalNumOfPages)

	'Display a next link if needed
	If intRecordPositionPageNum <> intTotalNumOfPages Then Response.Write("&nbsp;<a href=""search.asp?KW=" & Server.URLEncode(Request.QueryString("KW")) & "&SM=" & strSearchMode & "&SI=" & strSearchIn & "&FM=" & intForumID & "&OB=" & Trim(Mid(Request.QueryString("OB"), 1, 3)) & "&SPN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")

	Response.Write("</td>")
End If


'Reset Server Objects
Set rsTopic = Nothing
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
  </tr>
 </table>
</form>
<div align="center">
 <table width="617" border="0" cellspacing="0" cellpadding="2">
  <tr>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>no_new_posts_icon.gif" alt="<% = strTxtOpenTopic %>"> <% = strTxtOpenTopic %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>hot_topic_no_new_posts_icon.gif" alt="<% = strTxtHotTopic %>"> <% = strTxtHotTopic %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>priority_post_icon.gif" alt="<% = strTxtHighPriorityPost %>"> <% = strTxtHighPriorityPost %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>pinned_topic_icon.gif" alt="<% = strTxtPinnedTopic %>"> <% = strTxtPinnedTopic %></td>
  </tr>
  <tr>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>new_posts_icon.gif" alt="<% = strTxtOpenTopicNewReplies %>"> <% = strTxtOpenTopicNewReplies %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>hot_topic_new_posts_icon.gif" alt="<% = strTxtHotTopicNewReplies %>"> <% = strTxtHotTopicNewReplies %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>priority_post_locked_icon.gif" alt="<% = strTxtHighPriorityPostLocked %>"> <% = strTxtHighPriorityPostLocked %></td>
   <td class="smText" nowrap="nowrap"><img src="<% = strImagePath %>closed_topic_icon.gif" alt="<% = strTxtLockedTopic %>"> <% = strTxtLockedTopic %></td>
  </tr>
 </table>
 <br />
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->