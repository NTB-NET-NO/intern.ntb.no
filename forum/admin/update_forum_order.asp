<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Update the category and forum order
If Request.Form("Submit") = "Update Order" Then
		
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Category.* From " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
		
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Loop through the rs to change the cat order
	Do While NOT rsCommon.EOF
	
		rsCommon.Fields("Cat_order") = CInt(Request.Form("catOrder" & rsCommon("Cat_ID")))		
					
		'Add new forum to database
		rsCommon.Update
		
		'Move to the next record in the recordset
		rsCommon.MoveNext
	Loop
	
	'Close the recordset
	rsCommon.Close
	
	
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Forum.* From " & strDbTable & "Forum ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
		
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	
	'Loop through rs to change the forums order
	Do While NOT rsCommon.EOF
	
		rsCommon.Fields("Forum_Order") = CInt(Request.Form("forumOrder" & rsCommon("Forum_ID")))		
					
		'Add new forum to database
		rsCommon.Update
		
		'Move to the next record in the recordset
		rsCommon.MoveNext
	Loop
	
	'Close the recordset
	rsCommon.Close	
End If
	
'Reset main server variables
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'Return to the forum categories page
Response.Redirect "view_forums.asp"
%>