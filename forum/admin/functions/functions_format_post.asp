<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************




'******************************************
'***    Strip entites from IE posts   *****
'******************************************

Private Function WYSIWYGFormatPost(ByVal strMessage)

	'Format messages that use the WYSIWYG Editor
	strMessage = Replace(strMessage, " border=0>", ">", 1, -1, 1)
	strMessage = Replace(strMessage, " target=_blank>", ">", 1, -1, 1)
	strMessage = Replace(strMessage, " target=_top>", ">", 1, -1, 1)
	strMessage = Replace(strMessage, " target=_self>", ">", 1, -1, 1)
	strMessage = Replace(strMessage, " target=_parent>", ">", 1, -1, 1)
	strMessage = Replace(strMessage, " style=""CURSOR: hand""", "", 1, -1, 1)
	
	
	'Strip out add blocking injection code
	
	'Strip out Norton Internet Security pop up add blocking injected code
	strMessage = Replace(strMessage, "<SCRIPT> window.open=NS_ActualOpen; </SCRIPT>", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<SCRIPT language=javascript>postamble();</SCRIPT>", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<SCRIPT language=""javascript"">postamble();</SCRIPT>", "", 1, -1, 1)
	
	'Strip out Zone Alarm Pro's pop up add blocking injected code (bloody pain in the arse crap software)
	If Instr(1, strMessage, "<!-- ZoneLabs Popup Blocking Insertion -->", 1) Then
		strMessage = Replace(strMessage, "<!-- ZoneLabs Popup Blocking Insertion -->", "", 1, -1, 1)
		strMessage = Replace(strMessage, "<SCRIPT>" & vbCrLf & "window.open=NS_ActualOpen;" & vbCrLf & "orig_onload = window.onload;" & vbCrLf & "orig_onunload = window.onunload;" & vbCrLf & "window.onload = noopen_load;" & vbCrLf & "window.onunload = noopen_unload;" & vbCrLf & "</SCRIPT>", "", 1, -1, 1)
		strMessage = Replace(strMessage, "window.open=NS_ActualOpen; orig_onload = window.onload; orig_onunload = window.onunload; window.onload = noopen_load; window.onunload = noopen_unload;", "", 1, -1, 1)
	End If
	
	'Strip out Norton Personal Firewall 2003's pop up add blocking injected code
	strMessage = Replace(strMessage, "<!--" & vbCrLf & vbCrLf & "window.open = SymRealWinOpen;" & vbCrLf & vbCrLf & "//-->", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<!--" & vbCrLf & vbCrLf & "function SymError()" & vbCrLf & "{" & vbCrLf & "  return true;" & vbCrLf & "}" & vbCrLf & vbCrLf & "window.onerror = SymError;" & vbCrLf & vbCrLf & "//-->", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<!--" & vbCrLf & vbCrLf & "function SymError()" & vbCrLf & "{" & vbCrLf & "  return true;" & vbCrLf & "}" & vbCrLf & vbCrLf & "window.onerror = SymError;" & vbCrLf & vbCrLf & "var SymRealWinOpen = window.open;" & vbCrLf & vbCrLf & "function SymWinOpen(url, name, attributes)" & vbCrLf & "{" & vbCrLf & "  return (new Object());" & vbCrLf & "}" & vbCrLf & vbCrLf & "window.open = SymWinOpen;" & vbCrLf & vbCrLf & "//-->", "", 1, -1, 1)


	'Return the function
	WYSIWYGFormatPost = strMessage

End Function



'******************************************
'***        Format Post Function      *****
'******************************************

'Format Post Function to covert HTML tags into safe tags
Private Function FormatPost(ByVal strMessage)

	'Format spaces and HTML
	strMessage = Replace(strMessage, "<", "&lt;", 1, -1, 1)
	strMessage = Replace(strMessage, ">", "&gt;", 1, -1, 1)
	strMessage = Replace(strMessage, "       ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, "      ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, "     ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, "    ", "&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, "   ", "&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
	strMessage = Replace(strMessage, Chr(10), "<br />", 1, -1, 1)

	'Return the function
	FormatPost = strMessage

End Function





'******************************************
'***   Format Forum Codes Function    *****
'******************************************

'Format Forum Codes Function to covert forum codes to HTML
Private Function FormatForumCodes(ByVal strMessage)


	Dim strTempMessage	'Temporary word hold for e-mail, fonts, and url words
	Dim strMessageLink	'Holds the new mesage link that needs converting back into code
	Dim lngStartPos		'Holds the start position for a link
	Dim lngEndPos		'Holds the end position for a word
	Dim intLoop		'Loop counter



	'If emoticons are on then change the emotion symbols for the path to the relative smiley icon
	If blnEmoticons = True Then
		'Loop through the emoticons array
		For intLoop = 1 to UBound(saryEmoticons)
			strMessage = Replace(strMessage, saryEmoticons(intLoop,2), "<img border=""0"" src=""" & saryEmoticons(intLoop,3) & """>", 1, -1, 1)
		Next
	End If



	'Change forum codes for bold and italic HTML tags back to the normal satandard HTML tags
	strMessage = Replace(strMessage, "[B]", "<strong>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/B]", "</strong>", 1, -1, 1)
	strMessage = Replace(strMessage, "[STRONG]", "<strong>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/STRONG]", "</strong>", 1, -1, 1)
	strMessage = Replace(strMessage, "[I]", "<em>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/I]", "</em>", 1, -1, 1)
	strMessage = Replace(strMessage, "[EM]", "<em>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/EM]", "</em>", 1, -1, 1)
	strMessage = Replace(strMessage, "[U]", "<u>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/U]", "</u>", 1, -1, 1)
	
	strMessage = Replace(strMessage, "[HR]", "<hr />", 1, -1, 1)
	strMessage = Replace(strMessage, "[LIST=1]", "<ol>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/LIST=1]", "</ol>", 1, -1, 1)
	strMessage = Replace(strMessage, "[LIST]", "<ul>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/LIST]", "</ul>", 1, -1, 1)
	strMessage = Replace(strMessage, "[LI]", "<li>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/LI]", "</li>", 1, -1, 1)
	strMessage = Replace(strMessage, "[CENTER]", "<center>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/CENTER]", "</center>", 1, -1, 1)
	
	
	strMessage = Replace(strMessage, "[BR]", "<br>", 1, -1, 1)
	
	strMessage = Replace(strMessage, "[P]", "<p>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/P]", "</p>", 1, -1, 1)
	strMessage = Replace(strMessage, "[P ALIGN=CENTER]", "<p align=center>", 1, -1, 1)
	strMessage = Replace(strMessage, "[P ALIGN=LEFT]", "<p align=left>", 1, -1, 1)
	strMessage = Replace(strMessage, "[P ALIGN=RIGHT]", "<p align=right>", 1, -1, 1)
	
	strMessage = Replace(strMessage, "[DIV]", "<div>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/DIV]", "</div>", 1, -1, 1)
	strMessage = Replace(strMessage, "[DIV ALIGN=CENTER]", "<div align=center>", 1, -1, 1)
	strMessage = Replace(strMessage, "[DIV ALIGN=LEFT]", "<div align=left>", 1, -1, 1)
	strMessage = Replace(strMessage, "[DIV ALIGN=RIGHT]", "<div align=right>", 1, -1, 1)
	
	strMessage = Replace(strMessage, "[BLOCKQUOTE]", "<blockquote>", 1, -1, 1)
	strMessage = Replace(strMessage, "[/BLOCKQUOTE]", "</blockquote>", 1, -1, 1)

	strMessage = Replace(strMessage, "[SIZE=1]", "<font size=""1"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[SIZE=2]", "<font size=""2"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[SIZE=3]", "<font size=""3"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[SIZE=4]", "<font size=""4"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[SIZE=5]", "<font size=""5"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[SIZE=6]", "<font size=""6"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[/SIZE]", "</font>", 1, -1, 1)
	
	strMessage = Replace(strMessage, "[FONT=Arial]", "<font face=""Arial, Helvetica, sans-serif"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[FONT=Courier]", "<font face=""Courier New, Courier, mono"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[FONT=Times]", "<font face=""Times New Roman, Times, serif"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[FONT=Verdana]", "<font face=""Verdana, Arial, Helvetica, sans-serif"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[/FONT]", "</font>", 1, -1, 1)

	'These are for backward compatibility with old forum codes
	strMessage = Replace(strMessage, "[BLACK]", "<font color=""black"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[WHITE]", "<font color=""white"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[BLUE]", "<font color=""blue"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[RED]", "<font color=""red"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[GREEN]", "<font color=""green"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[YELLOW]", "<font color=""yellow"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[ORANGE]", "<font color=""orange"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[BROWN]", "<font color=""brown"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[MAGENTA]", "<font color=""magenta"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[CYAN]", "<font color=""cyan"">", 1, -1, 1)
	strMessage = Replace(strMessage, "[LIME GREEN]", "<font color=""limegreen"">", 1, -1, 1)


	'Loop through the message till all or any images are turned into HTML images
	Do While InStr(1, strMessage, "[IMG]", 1) > 0  AND InStr(1, strMessage, "[/IMG]", 1) > 0

		'Find the start position in the message of the [IMG] code
		lngStartPos = InStr(1, strMessage, "[IMG]", 1)

		'Find the position in the message for the [/IMG]] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/IMG]", 1) + 6
		
		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'Read in the code to be converted into a hyperlink from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink

		'Format the IMG tages into an HTML image tag
		strTempMessage = Replace(strTempMessage, "[IMG]", "<img src=""", 1, -1, 1)
		'If there is no tag shut off place a > at the end
		If InStr(1, strTempMessage, "[/IMG]", 1) Then
			strTempMessage = Replace(strTempMessage, "[/IMG]", """>", 1, -1, 1)
		Else
			strTempMessage = strTempMessage & ">"
		End If

		'Place the new fromatted hyperlink into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop




	'Loop through the message till all or any hyperlinks are turned into HTML hyperlinks
	Do While InStr(1, strMessage, "[URL=", 1) > 0 AND InStr(1, strMessage, "[/URL]", 1) > 0

		'Find the start position in the message of the [URL= code
		lngStartPos = InStr(1, strMessage, "[URL=", 1)

		'Find the position in the message for the [/URL] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/URL]", 1) + 6

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 7

		'Read in the code to be converted into a hyperlink from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink

		'Format the link into an HTML hyperlink
		strTempMessage = Replace(strTempMessage, "[URL=", "<a href=""", 1, -1, 1)
		
		'If there is no tag shut off place a > at the end
		If InStr(1, strTempMessage, "[/URL]", 1) Then
			strTempMessage = Replace(strTempMessage, "[/URL]", "</a>", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "]", """>", 1, -1, 1)
		Else
			strTempMessage = strTempMessage & ">"
		End If

		'Place the new fromatted hyperlink into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop
	
	
	
	
	'Loop through the message till all or any hyperlinks are turned into HTML hyperlinks
	Do While InStr(1, strMessage, "[URL]", 1) > 0  AND InStr(1, strMessage, "[/URL]", 1) > 0

		'Find the start position in the message of the [URL] code
		lngStartPos = InStr(1, strMessage, "[URL]", 1)

		'Find the position in the message for the [/URL]] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/URL]", 1) + 6
		
		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'Read in the code to be converted into a hyperlink from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink

		'Remove hyperlink BB codes
		strTempMessage = Replace(strTempMessage, "[URL]", "", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, "[/URL]", "", 1, -1, 1)
		
		'Format the URL tages into an HTML hyperlinks
		strTempMessage = "<a href=""" & strTempMessage & """>" & strTempMessage & "</a>"
		
		'Place the new fromatted hyperlink into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop




	'Loop through the message till all or any email links are turned into HTML mailto links
	Do While InStr(1, strMessage, "[EMAIL=", 1) > 0 AND InStr(1, strMessage, "[/EMAIL]", 1) > 0

		'Find the start position in the message of the [EMAIL= code
		lngStartPos = InStr(1, strMessage, "[EMAIL=", 1)

		'Find the position in the message for the [/EMAIL] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/EMAIL]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 9

		'Read in the code to be converted into a email link from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink

		'Format the link into an HTML mailto link
		strTempMessage = Replace(strTempMessage, "[EMAIL=", "<a href=""mailto:", 1, -1, 1)
		'If there is no tag shut off place a > at the end
		If InStr(1, strTempMessage, "[/EMAIL]", 1) Then
			strTempMessage = Replace(strTempMessage, "[/EMAIL]", "</a>", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "]", """>", 1, -1, 1)
		Else
			strTempMessage = strTempMessage & ">"
		End If


		'Place the new fromatted HTML mailto into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop




	'Loop through the message till all or any files are turned into HTML hyperlinks
	Do While InStr(1, strMessage, "[FILE=", 1) > 0 AND InStr(1, strMessage, "[/FILE]", 1) > 0

		'Find the start position in the message of the [FILE= code
		lngStartPos = InStr(1, strMessage, "[FILE=", 1)

		'Find the position in the message for the [/FILE] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/FILE]", 1) + 7

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 8

		'Read in the code to be converted into a hyperlink from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink

		'Format the link into an HTML hyperlink
		strTempMessage = Replace(strTempMessage, "[FILE=", "<a target=""_blank"" href=""", 1, -1, 1)
		'If there is no tag shut off place a > at the end
		If InStr(1, strTempMessage, "[/FILE]", 1) Then
			strTempMessage = Replace(strTempMessage, "[/FILE]", "</a>", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "]", """>", 1, -1, 1)
		Else
			strTempMessage = strTempMessage & ">"
		End If

		'Place the new fromatted hyperlink into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop
	
	
	
	'Loop through the message till all font colour codes are turned into fonts colours
	Do While InStr(1, strMessage, "[COLOR=", 1) > 0  AND InStr(1, strMessage, "[/COLOR]", 1) > 0

		'Find the start position in the message of the [COLOR= code
		lngStartPos = InStr(1, strMessage, "[COLOR=", 1)

		'Find the position in the message for the [/COLOR] closing code
		lngEndPos = InStr(lngStartPos, strMessage, "[/COLOR]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 9

		'Read in the code to be converted into a font colour from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))

		'Place the message colour into the tempoary message variable
		strTempMessage = strMessageLink

		'Format the link into an font colour HTML tag
		strTempMessage = Replace(strTempMessage, "[COLOR=", "<font color=", 1, -1, 1)
		'If there is no tag shut off place a > at the end
		If InStr(1, strTempMessage, "[/COLOR]", 1) Then
			strTempMessage = Replace(strTempMessage, "[/COLOR]", "</font>", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "]", ">", 1, -1, 1)
		Else
			strTempMessage = strTempMessage & ">"
		End If

		'Place the new fromatted colour HTML tag into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	Loop
	
	'Hear for backward compatability with old colour codes abive
	strMessage = Replace(strMessage, "[/COLOR]", "</font>", 1, -1, 1)


	'Return the function
	FormatForumCodes = strMessage
End Function





'******************************************
'***   	   Format User Quote		***
'******************************************

'This function formats quotes that contain usernames
Function formatUserQuote(ByVal strMessage)


	'Declare variables
	Dim strQuotedAuthor 	'Holds the name of the author who is being quoted
	Dim strQuotedMessage	'Hold the quoted message
	Dim lngStartPos		'Holds search start postions
	Dim lngEndPos		'Holds end start postions
	Dim strBuildQuote	'Holds the built quoted message
	Dim strOriginalQuote	'Holds the quote in original format

	'Loop through all the quotes in the message and convert them to formated quotes
	Do While InStr(1, strMessage, "[QUOTE=", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0

		'Get the start and end in the message of the author who is being quoted
		lngStartPos = InStr(1, strMessage, "[QUOTE=", 1) + 7
		lngEndPos = InStr(lngStartPos, strMessage, "]", 1)

		'If there is something returned get the authors name
		If lngStartPos > 6 AND lngEndPos > 0 Then
			strQuotedAuthor = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))
		End If



		'Get the start and end in the message of the message to quote
		lngStartPos = lngStartPos + Len(strQuotedAuthor) + 1
		lngEndPos = InStr(lngStartPos, strMessage, "[/QUOTE]", 1)

		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 0 Then lngEndPos = lngStartPos + Len(strQuotedAuthor)

		'If there is something returned get message to quote
		If lngEndPos > lngStartPos Then

			'Get the message to be quoted
			strQuotedMessage = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

			'Srip out any perenetis for those that are use to BBcodes that are different
			strQuotedAuthor = Replace(strQuotedAuthor, """", "", 1, -1, 1)

			'Build the HTML for the displying of the quoted message
			strBuildQuote = "<table width=""95%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
			strBuildQuote = strBuildQuote & vbCrLf & "<tr><td class=""bold"">" & strQuotedAuthor & " " & strTxtWrote & ":<br />"
			strBuildQuote = strBuildQuote & vbCrLf & "   <table width=""100%"" border=""0"" cellpadding=""1"" cellspacing=""0"" bgcolor=""" & strTableQuoteBorderColour & """>"
			strBuildQuote = strBuildQuote & vbCrLf & "    <tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "    <td><table width=""100%"" border=""0"" cellpadding=""2"" cellspacing=""0"" bgcolor=""" & strTableQuoteColour & """>"
			strBuildQuote = strBuildQuote & vbCrLf & "      <tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "       <td class=""text"">" & strQuotedMessage & "</td>"
			strBuildQuote = strBuildQuote & vbCrLf & "      </tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "     </table></td>"
			strBuildQuote = strBuildQuote & vbCrLf & "   </tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "  </table></td>"
			strBuildQuote = strBuildQuote & vbCrLf & "</tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "</table>"
		End If



		'Get the start and end position in the start and end position in the message of the quote
		lngStartPos = InStr(1, strMessage, "[QUOTE=", 1)
		lngEndPos = InStr(lngStartPos, strMessage, "[/QUOTE]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 7 Then lngEndPos = lngStartPos + Len(strQuotedAuthor) + 8

		'Get the original quote to be replaced in the message
		strOriginalQuote = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

		'Replace the quote codes in the message with the new formated quote
		If strBuildQuote <> "" Then
			strMessage = Replace(strMessage, strOriginalQuote, strBuildQuote, 1, -1, 1)
		Else
			strMessage = Replace(strMessage, strOriginalQuote, Replace(strOriginalQuote, "[", "&#91;", 1, -1, 1), 1, -1, 1)
		End If
	Loop

	'Return the function
	formatUserQuote = strMessage

End Function




'******************************************
'***   	   Format Quote			***
'******************************************

'This function formats the quote
Function formatQuote(ByVal strMessage)


	'Declare variables
	Dim strQuotedMessage	'Hold the quoted message
	Dim lngStartPos		'Holds search start postions
	Dim lngEndPos		'Holds end start postions
	Dim strBuildQuote	'Holds the built quoted message
	Dim strOriginalQuote	'Holds the quote in original format

	'Loop through all the quotes in the message and convert them to formated quotes
	Do While InStr(1, strMessage, "[QUOTE]", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0

		'Get the start and end in the message of the author who is being quoted
		lngStartPos = InStr(1, strMessage, "[QUOTE]", 1) + 7
		lngEndPos = InStr(lngStartPos, strMessage, "[/QUOTE]", 1)

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 7

		'If there is something returned get message to quote
		If lngEndPos > lngStartPos Then

			'Get the message to be quoted
			strQuotedMessage = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))


			'Build the HTML for the displying of the quoted message
			strBuildQuote = "<table width=""95%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
			strBuildQuote = strBuildQuote & vbCrLf & "<tr><td class=""bold"">" & strTxtQuote & ":<br>"
			strBuildQuote = strBuildQuote & vbCrLf & "   <table width=""100%"" border=""0"" cellpadding=""1"" cellspacing=""0"" bgcolor=""" & strTableQuoteBorderColour & """>"
			strBuildQuote = strBuildQuote & vbCrLf & "    <tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "    <td><table width=""100%"" border=""0"" cellpadding=""2"" cellspacing=""0"" bgcolor=""" & strTableQuoteColour & """>"
			strBuildQuote = strBuildQuote & vbCrLf & "      <tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "       <td class=""text"">" & strQuotedMessage & "</td>"
			strBuildQuote = strBuildQuote & vbCrLf & "      </tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "     </table></td>"
			strBuildQuote = strBuildQuote & vbCrLf & "   </tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "  </table></td>"
			strBuildQuote = strBuildQuote & vbCrLf & "</tr>"
			strBuildQuote = strBuildQuote & vbCrLf & "</table>"
		End If


		'Get the start and end position in the start and end position in the message of the quote
		lngStartPos = InStr(1, strMessage, "[QUOTE]", 1)
		lngEndPos = InStr(lngStartPos, strMessage, "[/QUOTE]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 7

		'Get the original quote to be replaced in the message
		strOriginalQuote = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

		'Replace the quote codes in the message with the new formated quote
		If strBuildQuote <> "" Then
			strMessage = Replace(strMessage, strOriginalQuote, strBuildQuote, 1, -1, 1)
		Else
			strMessage = Replace(strMessage, strOriginalQuote, Replace(strOriginalQuote, "[", "&#91;", 1, -1, 1), 1, -1, 1)
		End If
	Loop

	'Return the function
	formatQuote = strMessage

End Function





'******************************************
'***   	   Format Code Block		***
'******************************************

'This function formats the code blocks
Function formatCode(ByVal strMessage)


	'Declare variables
	Dim strCodeMessage		'Hold the coded message
	Dim lngStartPos			'Holds search start postions
	Dim lngEndPos			'Holds end start postions
	Dim strBuildCodeBlock		'Holds the built coded message
	Dim strOriginalCodeBlock	'Holds the code block in original format

	'Loop through all the codes in the message and convert them to formated code block
	Do While InStr(1, strMessage, "[CODE]", 1) > 0 AND InStr(1, strMessage, "[/CODE]", 1) > 0

		'Get the start and end in the message of the author who is being coded
		lngStartPos = InStr(1, strMessage, "[CODE]", 1) + 6
		lngEndPos = InStr(lngStartPos, strMessage, "[/CODE]", 1)

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'If there is something returned get message to code block
		If lngEndPos > lngStartPos Then

			'Get the message to be coded
			strCodeMessage = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))
			
			'Format the message
			strCodeMessage = Replace(strCodeMessage, "       ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, "      ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, "     ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, "    ", "&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, "   ", "&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			strCodeMessage = Replace(strCodeMessage, chr(9), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, -1, 1)
			'strCodeMessage = Replace(strCodeMessage, Chr(10), "<br />", 1, -1, 1)


			'Build the HTML for the displying of the coded message
			strBuildCodeBlock = "<table width=""95%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "<tr><td class=""bold"">" & strTxtCode & ":<br>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "   <table width=""100%"" border=""0"" cellpadding=""1"" cellspacing=""0"" bgcolor=""" & strTableQuoteBorderColour & """>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "    <tr>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "    <td><table width=""100%"" border=""0"" cellpadding=""2"" cellspacing=""0"" bgcolor=""" & strTableQuoteColour & """>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "      <tr>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "       <td class=""text"" style=""font-family: Courier New, Courier, mono;"">" & strCodeMessage & "</td>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "      </tr>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "     </table></td>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "   </tr>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "  </table></td>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "</tr>"
			strBuildCodeBlock = strBuildCodeBlock & vbCrLf & "</table>"
		End If


		'Get the start and end position in the start and end position in the message of the code block
		lngStartPos = InStr(1, strMessage, "[CODE]", 1)
		lngEndPos = InStr(lngStartPos, strMessage, "[/CODE]", 1) + 7

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'Get the original code to be replaced in the message
		strOriginalCodeBlock = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

		'Replace the code codes in the message with the new formated code block
		If strBuildCodeBlock <> "" Then
			strMessage = Replace(strMessage, strOriginalCodeBlock, strBuildCodeBlock, 1, -1, 1)
		Else
			strMessage = Replace(strMessage, strOriginalCodeBlock, Replace(strOriginalCodeBlock, "[", "&#91;", 1, -1, 1), 1, -1, 1)
		End If
	Loop

	'Return the function
	formatCode = strMessage

End Function





'******************************************
'***   	Format Flash File Support	***
'******************************************

'This function formats falsh codes
Function formatFlash(ByVal strMessage)


	'Declare variables
	Dim lngStartPos		'Holds search start postions
	Dim lngEndPos		'Holds end start postions
	Dim saryFlashAttributes 'Holds the features of the input flash file
	Dim intAttrbuteLoop	'Holds the attribute loop counter
	Dim strFlashWidth	'Holds the string value of the width of the Flash file
	Dim intFlashWidth	'Holds the interger value of the width of the flash file
	Dim strFlashHeight	'Holds the string value of the height of the Flash file
	Dim intFlashHeight	'Holds the interger value of the height of the flash file
	Dim strBuildFlashLink	'Holds the converted BBcode for the flash file
	Dim strTempFlashMsg	'Tempoary store for the BBcode
	Dim strFlashLink	'Holds the link to the flash file



	'Loop through all the codes in the message and convert them to formated flash links
	Do While InStr(1, strMessage, "[FLASH", 1) > 0 AND InStr(1, strMessage, "[/FLASH]", 1) > 0

		'Initiliase variables
		intFlashWidth = 50
		intFlashHeight = 50
		strFlashLink = ""
		strBuildFlashLink = ""
		strTempFlashMsg = ""

		'Get the Flash BBcode from the message
		lngStartPos = InStr(1, strMessage, "[FLASH", 1)
		lngEndPos = InStr(lngStartPos, strMessage, "[/FLASH]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'Get the original Flash BBcode from the message
		strTempFlashMsg = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))




		'Get the start and end in the message of the attributes of the Flash file
		lngStartPos = InStr(1, strTempFlashMsg, "[FLASH", 1) + 6
		lngEndPos = InStr(lngStartPos, strTempFlashMsg, "]", 1)

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos

		'If there is something returned get the details (eg. dimensions) of the flash file
		If strTempFlashMsg <> "" Then

			'Place any attributes for the flash file in an array
			saryFlashAttributes = Split(Trim(Mid(strTempFlashMsg, lngStartPos, lngEndPos-lngStartPos)), " ")

			'Get the dimensions of the Flash file
			'Loop through the array of atrributes that are for the falsh file to get the dimentions
			For intAttrbuteLoop = 0 To UBound(saryFlashAttributes)

				'If this is the width attribute then read in the width dimention
				If InStr(1, saryFlashAttributes(intAttrbuteLoop), "WIDTH=", 1) Then

					'Get the width dimention
					strFlashWidth = Replace(saryFlashAttributes(intAttrbuteLoop), "WIDTH=", "", 1, -1, 1)

					'Make sure we are left with a numeric number if so convert to an interger and place in an interger variable
					If isNumeric(strFlashWidth) Then intFlashWidth = CInt(strFlashWidth)
				End If

				'If this is the height attribute then read in the height dimention
				If InStr(1, saryFlashAttributes(intAttrbuteLoop), "HEIGHT=", 1) Then

					'Get the height dimention
					strFlashHeight = Replace(saryFlashAttributes(intAttrbuteLoop), "HEIGHT=", "", 1, -1, 1)

					'Make sure we are left with a numeric number if so convert to an interger and place in an interger variable
					If isNumeric(strFlashHeight) Then intFlashHeight = CInt(strFlashHeight)
				End If
			Next



			'Get the link to the flash file
			lngStartPos = InStr(1, strTempFlashMsg, "]", 1) + 1
			lngEndPos = InStr(lngStartPos, strTempFlashMsg, "[/FLASH]", 1)

			'Make sure the end position is not in error
			If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 8

			'Read in the code to be converted into a hyperlink from the message
			strFlashLink = Trim(Mid(strTempFlashMsg, lngStartPos, (lngEndPos - lngStartPos)))


			'Build the HTML for the displying of the flash file
			If strFlashLink <> "" Then
				strBuildFlashLink = "<embed src=""" & strFlashLink & """"
				strBuildFlashLink = strBuildFlashLink & " quality=high width=" & intFlashWidth & " height=" & intFlashHeight & " type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash""></embed>"
			End If
		End If



		'Replace the flash codes in the message with the new formated flash link
		If strBuildFlashLink <> "" Then
			strMessage = Replace(strMessage, strTempFlashMsg, strBuildFlashLink, 1, -1, 1)
		Else
			strMessage = Replace(strMessage, strTempFlashMsg, Replace(strTempFlashMsg, "[", "&#91;", 1, -1, 1), 1, -1, 1)
		End If
	Loop

	'Return the function
	formatFlash = strMessage

End Function




'******************************************
'***        Display edit author		***
'******************************************

'This function formats XML into the name of the author and edit date and time if a message has been edited
'XML is used so that the date can be stored as a double npresion number so that it can display the local edit time to the message reader
Function editedXMLParser(ByVal strMessage)

		'Declare variables
		Dim strEditedAuthor 	'Holds the name of the author who is editing the post
		Dim dtmEditedDate   	'Holds the date the post was edited
		Dim lngStartPos		'Holds search start postions
		Dim lngEndPos		'Holds end start postions


		'Get the start and end in the message of the author who edit the post
		lngStartPos = InStr(1, strMessage, "<editID>", 1) + 8
		lngEndPos = InStr(1, strMessage, "</editID>", 1)
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos
		

		'If there is something returned get the authors name
		strEditedAuthor = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

		'Get the start and end in the message of the date the message was edited
		lngStartPos = InStr(1, strMessage, "<editDate>", 1) + 10
		lngEndPos = InStr(1, strMessage, "</editDate>", 1)
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos

		'If there is something returned get the date the message was edited
		dtmEditedDate = Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos))

		'If it is a date then read convert it to a date otherwise set the variable to 0
		If isNumeric(dtmEditedDate) Then dtmEditedDate = CDate(dtmEditedDate) Else dtmEditedDate = 0


		'Get the start and end position in the string of the XML to remove
		lngStartPos = InStr(1, strMessage, "<edited>", 1)
		lngEndPos = InStr(1, strMessage, "</edited>", 1) + 9
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos

		'If there is something returned strip the XML from the message
		strMessage = Replace(strMessage, Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos)), "", 1, -1, 1)


		'Place the date and time into the message for when the post was edited
		If strEditedAuthor <> "" Then
			editedXMLParser = strMessage & "<span class=""smText""><br /><br />" & strTxtEditBy & " " & strEditedAuthor & " " & strTxtOn & " " & DateFormat(dtmEditedDate, saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(dtmEditedDate, saryDateTimeData) & "</span>"
		End If
End Function





'******************************************
'***    Convert Post to Text Function	***
'******************************************

'Function to romove icons and colurs to just leave plain text
Function ConvertToText(ByVal strMessage)

	Dim strTempMessage	'Temporary word hold for e-mail and url words
	Dim strMessageLink	'Holds the new mesage link that needs converting back into code
	Dim lngStartPos		'Holds the start position for a link
	Dim lngEndPos		'Holds the end position for a word
	Dim intLoop		'Loop counter

	'Remove hyperlinks
	strMessage = Replace(strMessage, " target=""_blank""", "", 1, -1, 1)
	
	
	'Loop through the message till all or any hyperlinks are turned into back into froum codes
	Do While InStr(1, strMessage, "<a href=""", 1) > 0 AND InStr(1, strMessage, "</a>", 1) > 0
						    	
		'Find the start position in the message of the HTML hyperlink
		lngStartPos = InStr(1, strMessage, "<a href=""", 1)
																	
		'Find the position in the message for the </a> closing code
		lngEndPos = InStr(lngStartPos, strMessage, "</a>", 1) + 4
		
		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 9 Then lngEndPos = lngStartPos + 9
						
		'Read in the code to be converted back into forum codes from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))	
		
		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink
		
		'Format the HTML hyperlink back into forum codes
		If InStr(1, strTempMessage, "src=""", 1) Then
			strTempMessage = Replace(strTempMessage, "<a href=""", " ", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "</a>", " ", 1, -1, 1)
		Else
			strTempMessage = Replace(strTempMessage, "<a href=""", " <font color='#0000FF'>", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, "</a>", " ", 1, -1, 1)
			strTempMessage = Replace(strTempMessage, """>", "</font> - ", 1, -1, 1)
		End If
		
		'Place the new fromatted codes into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)		
	Loop
	
	'Get any that may slip through (don't look as good but still has the same effect)
	strMessage = Replace(strMessage, "<a href= """, "", 1, -1, 1)
	strMessage = Replace(strMessage, "<a href='", "", 1, -1, 1)
	strMessage = Replace(strMessage, "</a>", "", 1, -1, 1)

	'Return the message with the icons and text colours removed
	ConvertToText = strMessage

End Function
%>