<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true
Response.Buffer = True



'Dimension variables
Dim strForumName	'Holds the name of the forum
Dim strGroupName	'Holds the name of the forum group
Dim intUserGroupID	'Holds the ID number of the group
Dim intForumID		'Holds the forum ID number
Dim blnRead
Dim blnPost
Dim blnReply
Dim blnEdit
Dim blnDelete
Dim blnPriority
Dim blnPollCreate
Dim blnVote
Dim blnAttachments
Dim blnImageUpload
Dim blnModerateForum



'Read in the details
intForumID = CInt(Request("FID"))
intUserGroupID = CInt(Request("GID"))



'Read in the forum name
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Forum.* From " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Forum_ID=" & intForumID & ";"

'Query the database
rsCommon.Open strSQL, adoCon

'Read in the forum name form the recordset
If NOT rsCommon.EOF Then

	'Read in the forums from the recordset
	strForumName = rsCommon("Forum_name")
End If

'Release server varaibles
rsCommon.Close



'Read in the user group name
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Group.Name FROM " & strDbTable & "Group WHERE " & strDbTable & "Group.Group_ID=" & intUserGroupID & ";"

'Query the database
rsCommon.Open strSQL, adoCon

'Read in the forum name form the recordset
If NOT rsCommon.EOF Then

	'Read in the user group name from the recordset
	strGroupName = rsCommon("Name")
End If

'Release server varaibles
rsCommon.Close




'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Permissions.* From " & strDbTable & "Permissions WHERE " & strDbTable & "Permissions.Forum_ID=" & intForumID & " AND " & strDbTable & "Permissions.Group_ID = " & intUserGroupID & ";"
	
'Set the Lock Type for the records so that the record set is only locked when it is updated
rsCommon.LockType = 3
	
'Query the database
rsCommon.Open strSQL, adoCon

'If this is a post back update the database
If Request.Form("postBack") Then

	With rsCommon

		'Update the recordset
		.Fields("Forum_ID") = intForumID
		.Fields("Group_ID") = intUserGroupID
		.Fields("Read") = CBool(Request.Form("read"))
		.Fields("Post") = CBool(Request.Form("post"))
		.Fields("Reply_posts") = CBool(Request.Form("reply"))
		.Fields("Edit_posts") = CBool(Request.Form("edit"))
		.Fields("Delete_posts") = CBool(Request.Form("delete"))
		.Fields("Priority_posts") = CBool(Request.Form("priority"))
		.Fields("Poll_create") = CBool(Request.Form("poll"))
		.Fields("Vote") = CBool(Request.Form("vote"))
		.Fields("Attachments") = CBool(Request.Form("files"))
		.Fields("Image_upload") = CBool(Request.Form("images"))
		.Fields("Moderate") = CBool(Request.Form("moderate"))

		'Update the database with the new user's details
		.Update
		
		'Read back in detials from the db
		.Requery
		
		'Release server varaibles
		rsCommon.Close
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing
	
		'Redirect to main permisions page
		Response.Redirect("forum_group_permissions.asp?GID=" & intUserGroupID)
	End With

End If



'Read in the forum details from the recordset
If NOT rsCommon.EOF Then

	'Read in the forums from the recordset
	blnRead = CBool(rsCommon("Read"))
	blnPost = CBool(rsCommon("Post"))
	blnReply = CBool(rsCommon("Reply_posts"))
	blnEdit = CBool(rsCommon("Edit_posts"))
	blnDelete = CBool(rsCommon("Delete_posts"))
	blnPriority = CBool(rsCommon("Priority_posts"))
	blnPollCreate = CBool(rsCommon("Poll_create"))
	blnVote = CBool(rsCommon("Vote"))
	blnAttachments = CBool(rsCommon("Attachments"))
	blnImageUpload = CBool(rsCommon("Image_upload"))
	blnModerateForum = CBool(rsCommon("Moderate"))
End If


'Release server varaibles
rsCommon.Close

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Edit Group Permissions</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"><span class="heading">Edit User Group Permissions for <% = strGroupName %></span><br />
 <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br>
 <a href="group_perm_forum.asp" target="_self">Select another Forum to Create, Edit, or Delete Forum Permissions on</a><br />
 <br>
 <span class="text">Use the form below to Edit the User Group Permissions for <% = strGroupName %> on the forum <% = strForumName %></span></div>
<form method="post" name="frmNewForum" action="edit_group_permissions.asp?FID=<% = intForumID %>&GID=<% = intUserGroupID %>">
 <br>
 <table width="450" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000">
  <tr>
   <td width="450"> <table width="100%" border="0" align="center" class="normal" cellpadding="4" cellspacing="1">
     <tr bgcolor="#CCCEE6">
      <td colspan="2" class="tHeading"><b><% = strForumName %> Forum<br /> <% = strGroupName %> User Group Permissions</b></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td width="57%" class="text">Forum Access:</td>
      <td width="43%" valign="top"> 
       <input type="checkbox" name="read" value="true"<% If blnRead = true Then Response.Write(" checked") %> /> </td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td width="57%" class="text">Post New Topics:</td>
      <td width="43%" valign="top"> 
       <input type="checkbox" name="post" value="true"<% If blnPost = true Then Response.Write(" checked") %> /> </td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td width="57%" class="text">Reply To Posts:<br /> </td>
      <td width="43%" valign="top"> 
       <input type="checkbox" name="reply" value="true"<% If blnReply = true Then Response.Write(" checked") %> /> </td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Edit Posts:</td>
      <td valign="top"> 
       <input type="checkbox" name="edit" value="true"<% If blnEdit = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Delete Posts:</td>
      <td valign="top"> 
       <input type="checkbox" name="delete" value="true"<% If blnDelete = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Sticky Topics:</td>
      <td valign="top"> 
       <input type="checkbox" name="priority" value="true"<% If blnPriority = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Create Poll's:</td>
      <td valign="top"> 
       <input type="checkbox" name="poll" value="true"<% If blnPollCreate = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Vote in Poll's:</td>
      <td valign="top"> 
       <input type="checkbox" name="vote" value="true"<% If blnVote = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Upload Images in Posts:<br>
       <span class="smText">Only enable this function if you have first setup the <a href="upload_configure.asp" target="_self" class="smLink">upload configuration</a>, if your web space supports it.</span></td>
      <td valign="top"> 
       <input type="checkbox" name="images" value="true"<% If blnImageUpload = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td class="text">Attach Files to Posts:<br>
       <span class="smText">Only enable this function if you have first setup the <a href="upload_configure.asp" target="_self" class="smLink">upload configuration</a>, if your web space supports it.</span> 
      </td>
      <td valign="top"> 
       <input type="checkbox" name="files" value="true"<% If blnAttachments = true Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="#F5F5FA"> 
      <td width="57%" class="text">Moderate Forum:<br />
       <span class="smText">Only enable this if you wish this User Group to be able to have moderator functions on this forum.</span></td>
      <td width="43%" valign="top" class="smText"> 
       <input type="checkbox" name="moderate" value="true"<% If blnModerateForum = true Then Response.Write(" checked") %> /></td>
     </tr>
    </table></td>
  </tr>
 </table>
 <div align="center"><br />
  <input type="hidden" name="postBack" value="true" />
  <input type="submit" name="Submit" value="Update User Group Permissions" />
  <input type="reset" name="Reset" value="Reset Form" />
  <br />
 </div>
</form><%


'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
</body>
</html>