<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************
%>
<html>
<head>
<title>Admin Navigation</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>

<body  background="images/main_bg.gif">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr>
  <td align="center" class="heading">Admin Menu<br>
   <br>
   <table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
    <tr> 
     <td width="690"> <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
       <tr> 
        <td> <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr> 
           <td bgcolor="#CCCEE6" class="text"><span class="bold"><b>Admin</b></span></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA"><a href="admin_menu.asp" target="mainFrame" class="smLink">Admin Menu</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="view_forums.asp" target="mainFrame" class="smLink">Forum Admin</a></td>
          </tr>
          <tr>
           <td bgcolor="#F5F5FA" class="text"><a href="close_forums.asp" target="mainFrame" class="smLink">Lock Forums</a></td>
          </tr>
          <tr>
           <td bgcolor="#F5F5FA" class="text"><a href="http://www.webwizforums.com/update.asp?v=<% = Server.URLEncode(strVersion) %>" target="_blank" class="smLink">Check for Updates </a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="http://www.webwizforums.com" target="_blank" class="smLink">About</a></td>
          </tr>
          <tr> 
           <td bgcolor="#CCCEE6" class="text"><span class="bold"><b>User Group</b></span><b> Admin</b></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="view_groups.asp" target="mainFrame" class="smLink">Group Admin</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="group_perm_forum.asp" target="mainFrame" class="smLink">Group Permissions Admin</a></td>
          </tr>
          <tr> 
           <td bgcolor="#CCCEE6" class="bold">Member Admin</td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="select_members.asp" target="mainFrame" class="smLink">Membership Admin</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="find_user.asp" target="mainFrame" class="smLink">Member Permissions Admin</a></td>
          </tr>
          <tr>
           <td bgcolor="#F5F5FA" class="text"><a href="add_member.asp" target="mainFrame" class="smLink">Add New Member </a></td>
          </tr>
          <tr>
           <td bgcolor="#F5F5FA" class="text"><a href="change_username.asp" target="mainFrame" class="smLink">Change Username </a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="suspend_registration.asp" target="mainFrame" class="smLink">Suspend Registration</a></td>
          </tr>
          <tr> 
           <td bgcolor="#CCCEE6" class="text"><span class="bold">General Admin</span></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="forum_configure.asp" target="mainFrame" class="smLink">Forum Configuration</a></td>
          </tr>
          <tr><%
          
'If this is the main forum admin let him change the admin username and password
If lngLoggedInUserID = 1 Then %>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="change_admin_username.asp" target="mainFrame" class="smLink">Change Admin Username and Password</a></td>
          </tr><%
End If

%>
           <td bgcolor="#F5F5FA" class="text"><a href="date_time_configure.asp" target="mainFrame" class="smLink">Forum Date and Time Settings</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="email_notify_configure.asp" target="mainFrame" class="smLink">Email Configuration Setup</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="member_mailier.asp" target="mainFrame" class="smLink">Mass Email Members</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="upload_configure.asp" target="mainFrame" class="smLink">File and Image Upload Setup and Configuration </a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="spam_configure.asp" target="mainFrame" class="smLink">Anti-Spam Configuration</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="bad_word_filter_configure.asp" target="mainFrame" class="smLink">Configure the Bad Word Filter</a></td>
          </tr><%
       
'If this is an access database show the compact and repair feature
If strDatabaseType = "Access" Then %>  
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="compact_access_db.asp" target="mainFrame" class="smLink">Compact Database</a></td>
          </tr><%
End If %>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="statistics.asp" target="mainFrame" class="smLink">Forum Statistics</a></td>
          </tr> <%

'If the forum is using an SQL server then display stats oabout the server link
If strDatabaseType = "SQLServer" Then %>
           <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="sql_server_db_stats.asp" target="mainFrame" class="smLink">SQL DB Statistics</a></td>
          </tr><%
End If %>
          <tr> 
           <td bgcolor="#CCCEE6" class="text"><span class="bold"><b>Ban Admin</b></span></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="ip_blocking.asp" target="mainFrame" class="smLink">IP Address Banning</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="email_domain_blocking.asp" target="mainFrame" class="smLink">Email Address Banning</a></td>
          </tr>
          <tr> 
           <td bgcolor="#CCCEE6" class="text"><span class="bold">Forum Clearout and Archive</span></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="resync_forum_post_count.asp" target="mainFrame" class="smLink">Re-sync Topic and Post Count</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="archive_topics_form.asp" target="mainFrame" class="smLink">Batch Close Old Topics</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_posts_form.asp" target="mainFrame" class="smLink">Batch Delete Topics</a></td>
          </tr>
          <tr>
           <td bgcolor="#F5F5FA" class="text"><a href="batch_move_topics_form.asp" target="mainFrame" class="smLink">Batch Move Topics</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_pm_form.asp" target="mainFrame" class="smLink">Batch Delete Private Messages</a></td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_members_form.asp" target="mainFrame" class="smLink">Batch Delete Members</a></td>
          </tr>
          <tr> 
           <td bgcolor="#CCCEE6" class="bold">Removing links</td>
          </tr>
          <tr> 
           <td bgcolor="#F5F5FA"><a href="remove_link_buttons.asp" target="mainFrame" class="smLink">Remove Powered By links</a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table> </td>
 </tr>
</table>
</body>
</html>
