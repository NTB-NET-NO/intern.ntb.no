<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the timeout of the page
Server.ScriptTimeout =  1000


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True




'Dimension variables
Dim rsTopic		'Holds the database recordset for the Topic to be delete
Dim intCatID		'Holds the forum ID to be deleted
Dim lngPollID		'Holds the poll ID if there is one to delete
Dim intForumID		'Holds the forum ID to be deleted
Dim rsPost		'Recordset to get the post ID from db to check against guest post names


'Get the category ID to delete
intCatID = CInt(Request.QueryString("CatID"))



'Get all the Forums from the database to be deleted

'Create a recordset object for the Topic in the database
Set rsTopic = Server.CreateObject("ADODB.Recordset")
Set rsPost = Server.CreateObject("ADODB.Recordset")


'Initalise the strSQL variable with an SQL statement to get the topic from the database
strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID ="  & intCatID & ";"

'Query the database
rsCommon.Open strSQL, adoCon



'Loop through all the forums for the topics and delete them
Do While NOT rsCommon.EOF

	'Read in the forum ID
	intForumID = CInt(rsCommon("Forum_ID"))

	'Get all the Topics from the database to be deleted
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "SELECT " & strDbTable & "Topic.* FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Forum_ID ="  & intForumID & ";"

	'Query the database
	rsTopic.Open strSQL, adoCon

	'Loop through all the threads for the topics and delete them
	Do While NOT rsTopic.EOF
	
	
	
		'First we need to delete any entry in the GuestName table incase this was a guest poster posting the message
		
		'Initalise the strSQL variable with an SQL statement to get thread ID from the database
		strSQL = "SELECT " & strDbTable & "Thread.Thread_ID FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID ="  & CLng(rsTopic("Topic_ID")) & ";"
		
		'Query the database
		rsPost.Open strSQL, adoCon
		
		'Loop through thread ID's
		Do While NOT rsPost.EOF
		
			'First we need to delete any entry in the GuestName table incase this was a guest poster posting the message
			strSQL = "DELETE FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID=" & CLng(rsPost("Thread_ID")) & ";"
		
			'Excute SQL
			adoCon.Execute(strSQL)
			
			'Movenext rs
			rsPost.MoveNext
		Loop
		
		'Close rs
		rsPost.Close
	
	

		'Delete the posts in this topic
		strSQL = "DELETE FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID ="  & CLng(rsTopic("Topic_ID")) & ";"

		'Write to database
		adoCon.Execute(strSQL)


		'Delete any polls

		'Get the Poll ID
		lngPollID = CLng(rsTopic("Poll_ID"))

		'If there is a poll delete that as well
		If lngPollID > 0 Then

			'Delete the poll choice
			strSQL = "DELETE FROM " & strDbTable & "PollChoice WHERE " & strDbTable & "PollChoice.Poll_ID =" & lngPollID & ";"

			'Delete the threads
			adoCon.Execute(strSQL)

			'Delete the poll choice
			strSQL = "DELETE FROM " & strDbTable & "Poll WHERE " & strDbTable & "Poll.Poll_ID =" & lngPollID & ";"

			'Delete the threads
			adoCon.Execute(strSQL)
		End If


		'Move to the next record
		rsTopic.MoveNext
	Loop




	'Delete the topics in this forum
	strSQL = "DELETE FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Forum_ID ="  & intForumID & ";"

	'Write to database
	adoCon.Execute(strSQL)



	'Delete any group permissions set for the forum
	strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE " & strDbTable & "Permissions.Forum_ID ="  & intForumID & ";"
	
	'Write to database
	adoCon.Execute(strSQL)


	'close recordset
	rsTopic.Close



	'Move to the next record
	rsCommon.MoveNext
Loop



'Delete the forum
strSQL = "DELETE FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID ="  & intCatID & ";"

'Write to database
adoCon.Execute(strSQL)



'Delete the category
strSQL = "DELETE FROM " & strDbTable & "Category WHERE " & strDbTable & "Category.Cat_ID ="  & intCatID & ";"

'Write to database
adoCon.Execute(strSQL)



'Reset Server Objects
Set rsPost = Nothing
rsCommon.Close
Set rsCommon = Nothing
Set rsTopic = Nothing
adoCon.Close
Set adoCon = Nothing


'Return to the forum categories page
Response.Redirect "view_forums.asp"
%>