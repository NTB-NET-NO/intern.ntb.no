<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Dimension variables
Dim strPofileUsername		'Holds the users username
Dim lngUserID			'Holds the new users ID number
Dim lngNumOfPosts		'Holds the number of posts the user has made
Dim strGroupName		'Holds the group name
Dim dtmRegisteredDate		'Holds the date the usre registered
Dim intTotalNumMembersPages	'Holds the total number of pages
Dim intTotalNumMembers		'Holds the total number of forum members
Dim intRecordPositionPageNum	'Holds the page number we are on
Dim intRecordLoopCounter	'Recordset loop counter
Dim dtmLastVisit		'Holds the date of the users las post
Dim intLinkPageNum		'Holds the page number to link to
Dim strSearchCriteria		'Holds the search critiria
Dim strSortBy			'Holds the way the records are sorted
Dim intSortSelectField		'Holds the sort selection to be shown in the sort list box

'Initalise variables
strSearchCriteria = "%"


'If this is the first time the page is displayed then the members record position is set to page 1
If Request.QueryString("MemPN") = "" Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the members page record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("MemPN"))
End If


'Get the search critiria for the members to display
If NOT Request.QueryString("find") = "" Then
	strSearchCriteria = Request.QueryString("find") & "%"
End If

'Get rid of milisous code
strSearchCriteria = formatSQLInput(strSearchCriteria)

'Get the sort critiria
Select Case Request.QueryString("Sort")
	Case "post"
		strSortBy = "No_of_posts DESC"
		intSortSelectField = 1
	Case "latestUsers"
		strSortBy = "Join_date DESC"
		intSortSelectField = 2
	Case "oldestUsers"
		strSortBy = "Join_date ASC"
		intSortSelectField = 3
	Case "location"
		strSortBy = "Location ASC"
		intSortSelectField = 4
	Case Else
		strSortBy = "Username ASC"
		intSortSelectField = 0
End Select


%>
<html>
<head>
<title>Forum Member Adminstration</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Check for a somthing to search for
	if (document.frmMemberSearch.find.value==""){
		alert("Please enter a member to Search for");
		return false;
	}

	return true;
}

//Function to choose how the members list is sorted
function MembersSort(SelectSort){

   	if (SelectSort != "") self.location.href = "select_members.asp?find=<% = Server.URLEncode(Request.QueryString("find")) %>&sort=" + SelectSort.options[SelectSort.selectedIndex].value;
	return true;
}

//Function to move to another page of members
function MembersPage(SelectPage){

   	if (SelectPage != -1) self.location.href = "select_members.asp?find=<% = Request.QueryString("find") %>&sort=<% = Request.QueryString("sort") %>&MemPN=" + SelectPage.options[SelectPage.selectedIndex].value;
	return true;
}

// -->
</script>

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">Forum Member Administration</span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br>
  Click on the members name to administer their forum membership account, <br />
  from where you can, change their details, member group, reset password, suspend, delete, etc. from the Forum.<br />
 </p>
 <form name="frmMemberSearch" method="get" action="select_members.asp" onSubmit="return CheckForm();">
  <table width="490" border="0" cellspacing="0" cellpadding="1" height="24" align="center" bgcolor="#000000">
   <tr>
    <td align="center" height="2"> <table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#FFFFFF">
      <tr>
       <td width="100%" height="20" align="center" bgcolor="#F5F5FA" class="text"><span class="text">Search:</span> 
        <input type="text" name="find" size="15" maxlength="15"> <input type="submit" name="Submit" value="Search">
       </td>
      </tr>
      <tr>
       <td align="center" bgcolor="#F5F5FA" width="100%"><a href="select_members.asp" target="_self">All</a> <a href="select_members.asp?find=A" target="_self">A</a> <a href="select_members.asp?find=B" target="_self">B</a> 
        <a href="select_members.asp?find=C" target="_self">C</a> <a href="select_members.asp?find=D" target="_self">D</a> <a href="select_members.asp?find=E" target="_self">E</a> <a href="select_members.asp?find=F" target="_self">F</a> 
        <a href="select_members.asp?find=G" target="_self">G</a> <a href="select_members.asp?find=H" target="_self">H</a> <a href="select_members.asp?find=I" target="_self">I</a> <a href="select_members.asp?find=J" target="_self">J</a> 
        <a href="select_members.asp?find=K" target="_self">K</a> <a href="select_members.asp?find=L" target="_self">L</a> <a href="select_members.asp?find=M" target="_self">M</a> <a href="select_members.asp?find=N" target="_self">N</a> 
        <a href="select_members.asp?find=O" target="_self">O</a> <a href="select_members.asp?find=P" target="_self">P</a> <a href="select_members.asp?find=Q" target="_self">Q</a> <a href="select_members.asp?find=R" target="_self">R</a> 
        <a href="select_members.asp?find=S" target="_self">S</a> <a href="select_members.asp?find=T" target="_self">T</a> <a href="select_members.asp?find=U" target="_self">U</a> <a href="select_members.asp?find=V" target="_self">V</a> 
        <a href="select_members.asp?find=W" target="_self">W</a> <a href="select_members.asp?find=X" target="_self">X</a> <a href="select_members.asp?find=Y" target="_self">Y</a> <a href="select_members.asp?find=Z" target="_self">Z</a></td>
      </tr>
     </table></td>
   </tr>
  </table>
 </form>
</div>
<div align="center"><%


'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Author.*, " & strDbTable & "Group.Name "
strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Group "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Author.Username Like '" & strSearchCriteria & "' "
strSQL = strSQL & "ORDER BY " & strDbTable & "Author." & strSortBy & ";"

'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
rsCommon.CursorType = 1

'Query the database
rsCommon.Open strSQL, adoCon

'Set the number of records to display on each page
rsCommon.PageSize = 33


'If there are no memebers to display then show an error message
If rsCommon.EOF Then
	Response.Write "<span class=""text"">Sorry, your search found no forum members that match your criteria</span>"

'If there is a recorset returned by the query then read in the details
Else

	'Set the page number to display records for
	rsCommon.AbsolutePage = intRecordPositionPageNum


	'Count the number of members there are in the database by returning the number of records in the recordset
	intTotalNumMembers = rsCommon.RecordCount

	'Count the number of pages there are in the database calculated by the PageSize attribute set above
	intTotalNumMembersPages = rsCommon.PageCount


	'Display the HTML for the total number of pages and total number of records in the database for the users
	Response.Write vbCrLf & "	<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">"
	Response.Write vbCrLf & " 	  <tr>"
	Response.Write vbCrLf & " 	    <td align=""center""><span class=""text"">"

	'If we are showing all the forum memebers then display how many members there are
	If Request.QueryString("find") = "" Then
		Response.Write vbCrLf & "	      Thre are " & intTotalNumMembers & " forum members on " & intTotalNumMembersPages & " pages and you are on page number " & intRecordPositionPageNum
	'Else display how many results were fround from the search
	Else
		Response.Write vbCrLf & "	      Your search of the forum members found " & intTotalNumMembers & " matches"
	End If

	Response.Write vbCrLf & "	    </span></td>"
	Response.Write vbCrLf & "	  </tr>"
	Response.Write vbCrLf & "	</table>"
	Response.Write vbCrLf & "	<br />"
%>
 <form>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="32">
   <tr>
    <td align="right" height="28" valign="top"><span class="text">Sort Results By</span> <select name="SelectSort" onChange="MembersSort(this)">
      <option value="username" <% If intSortSelectField = 0 Then Response.Write "selected" %>>Username Alphabetically</option>
      <option value="post" <% If intSortSelectField = 1 Then Response.Write "selected" %>>No of Posts</option>
      <option value="latestUsers" <% If intSortSelectField = 2 Then Response.Write "selected" %>>New Forum Members First</option>
      <option value="oldestUsers" <% If intSortSelectField = 3 Then Response.Write "selected" %>>Old Forum Members First</option>
      <option value="location" <% If intSortSelectField = 4 Then Response.Write "selected" %>>Location Alphabetically</option>
     </select> </td>
   </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000" align="center">
   <tr>
    <td width="983" height="46"> <table border="0" cellspacing="1" cellpadding="3" bgcolor="#000000" width="100%">
      <tr bgcolor="#CCCEE6" class="tHeading">
       <td width="26%" height="2"><b>Username</b></td>
       <td width="16%" height="2"><b>Group</b></td>
       <td width="10%" height="2" align="center"><b>Posts</b></td>
       <td width="23%" height="2" align="left"><b>Last Visit</b></td>
       <td width="25%" height="2" align="left"><b>Registered</b></td>
      </tr><%

	'For....Next Loop to loop through the recorset to display the forum members
	For intRecordLoopCounter = 1 to 33

		'If there are no member's records left to display then exit loop
		If rsCommon.EOF Then Exit For

		'Initialise varibles
		dtmLastVisit = ""

		'Read in the profile from the recordset
		lngUserID = CLng(rsCommon("Author_ID"))
		strPofileUsername = rsCommon("Username")
		lngNumOfPosts = CLng(rsCommon("No_of_posts"))
		dtmRegisteredDate = CDate(rsCommon("Join_date"))
		If NOT isNull(rsCommon("Last_visit")) Then dtmLastVisit = CDate(rsCommon("Last_visit"))
		strGroupName = rsCommon("Name")


		'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
		%>
      <tr bgcolor="#F5F5FA"> 
       <td width="26%" height="30" class="text"><a href="../register.asp?PF=<% = lngUserID %>&M=A" target="_top"> 
        <% = strPofileUsername %>
        </a></td>
       <td width="16%" height="30" class="text"> 
        <% = strGroupName %>
       </td>
       <td width="10%" height="30" align="center" class="text"> 
        <% = lngNumOfPosts %>
       </td>
       <td width="23%" height="30" align="left" class="text"> 
        <%

                	If dtmLastVisit = "" Then
             			Response.Write "&nbsp;"
             	 	Else
             	 		Response.Write FormatDateTime(dtmLastVisit, VbLongDate)
             	 	End If
             	 %>
       </td>
       <td width="25%" height="30" align="left" class="text"> 
        <% = FormatDateTime(dtmRegisteredDate, VbLongDate) %>
       </td>
      </tr><%

		'Move to the next record in the database
   		rsCommon.MoveNext

	'Loop back round
	Next
End If


%>
     </table></tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="32">
   <tr><%

   'If there is more than 1 page of members then dispaly drop down list to the other members
	If intTotalNumMembersPages > 1 Then

		'Display an drop down list to the other members in list
		Response.Write vbCrLf & "		<td align=""right"" height=""28"" valign=""bottom""><span class=""text"">Page"
		Response.Write vbCrLf & "		 <select onChange=""MembersPage(this)"" name=""SelectPage"">"

		Dim intTopicPageLoopCounter

		'Loop round to display links to all the other pages
		For intTopicPageLoopCounter = 1 to intTotalNumMembersPages

			'Display a link in the link list to the another members page
			Response.Write vbCrLf & "		  <option value=""" & intTopicPageLoopCounter & """"

			'If this page number to display is the same as the page being displayed then make sure it's selected
			If intTopicPageLoopCounter = intRecordPositionPageNum Then
				Response.Write " selected"
			End If

			'Display the link page number
			Response.Write ">" & intTopicPageLoopCounter & "</option>"

		Next

		'End the drop down list
		Response.Write vbCrLf & "		</select> of " & intTotalNumMembersPages & "</span></td>"
End If
%>
   </tr>
  </table><%

'Reset Server Variables
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
  <br />
 </form><br />
</div>
<br />
</body>
</html>