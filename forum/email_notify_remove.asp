<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Declare variables
Dim intForumID		'Holds the forum ID
Dim laryWatchID		'Array to hold the ID number for each email noti to be deleted
Dim strMode		'Holds the mode of the page
Dim lngEmailUserID		'Holds the user ID to look at email notification for

'Read in the mode of the page
strMode = Trim(Mid(Request.QueryString("M"), 1, 1))


'If this is not an admin but in admin mode then see if the user is a moderator
If blnAdmin = False AND strMode = "A" Then
	
	'Initalise the strSQL variable with an SQL statement to query the database
        strSQL = "SELECT " & strDbTable & "Permissions.Moderate "
        strSQL = strSQL & "FROM " & strDbTable & "Permissions "
        If strDatabaseType = "SQLServer" Then
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = 1;"
        Else
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = True;"
        End If
               

        'Query the database
         rsCommon.Open strSQL, adoCon

        'If a record is returned then the user is a moderator in one of the forums
        If NOT rsCommon.EOF Then blnModerator = True

        'Clean up
        rsCommon.Close
End If


'Get the user ID of the email notifications to look at
If (blnAdmin OR (blnModerator AND CLng(Request.QueryString("PF")) > 2)) AND strMode = "A" Then
	
	lngEmailUserID = CLng(Request.QueryString("PF"))

'Get the logged in ID number
Else
	lngEmailUserID = lngLoggedInUserID
End If



'Run through till all checked email notifications are deleted
For each laryWatchID in Request.Form("chkDelete")
	
	'Delete the poll choice
	strSQL = "DELETE FROM " & strDbTable & "EmailNotify WHERE " & strDbTable & "EmailNotify.Watch_ID =" & CLng(laryWatchID) & " AND " & strDbTable & "EmailNotify.Author_ID=" & lngEmailUserID & ";"

	'Delete the threads
	adoCon.Execute(strSQL)
Next
	 
'Reset Server Variables
Set rsCommon = Nothing	
adoCon.Close
Set adoCon = Nothing

'Redirect back
Response.Redirect("email_notify_subscriptions.asp?" & Request.QueryString)
%>