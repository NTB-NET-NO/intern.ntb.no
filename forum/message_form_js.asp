<!--#include file="language_files/language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

Response.AddHeader "pragma","cache"
Response.AddHeader "cache-control","public"
Response.CacheControl = "Public"

%>
//Have the propmt box turned on by default
var promptOn = true;

//Function to turn on or off the prompt box
function PromptMode(selectMode){
	
	if (selectMode.options[selectMode.selectedIndex].value == 0){
		promptOn = false;
	}
	else{
		promptOn = true;
	}
}


// Function to add the code for bold italic centre and underline, to the message
function AddMessageCode(code, promptText, InsertText) {

	if (code != "") {
		if (promptOn == true){
			insertCode = prompt(promptText + "\n[" + code + "]xxx[/" + code + "]", InsertText);
				if ((insertCode != null) && (insertCode != "")){
					document.frmAddMessage.message.value += "[" + code + "]" + insertCode + "[/" + code + "]";
				}
		}
		else{
			document.frmAddMessage.message.value += "[" + code + "][/" + code + "]";
		}
	}
				
	document.frmAddMessage.message.focus();
}

// Function to add the font colours, sizes, type to the message
function FontCode(code, endCode) {

	if (code != "") {
		if (promptOn == true){
			insertCode = prompt("<% = strTxtEnterTextYouWouldLikeIn %> " + code + "\n[" + code + "]xxx[/" + endCode + "]", '');
				if ((insertCode != null) && (insertCode != "")){
					document.frmAddMessage.message.value += "[" + code + "]" + insertCode + "[/" + endCode + "]";
				}
		}
		else{
			document.frmAddMessage.message.value += "[" + code + "][/" + endCode + "]";
		}
	}	
	document.frmAddMessage.message.focus();
}



//Function to add the URL, indent, list, and Email code to the message
function AddCode(code) {

	//For the URL code
	if ((code != "") && (code == "URL")) {
		if (promptOn == true){
			insertText = prompt("<% = strTxtEnterHyperlinkText %>", "");
				
				if ((insertText != null) && (insertText != "") && (code == "URL")){
					insertCode = prompt("<% = strTxtEnterHeperlinkURL %>", "http://");
						
						if ((insertCode != null) && (insertCode != "") && (insertCode != "http://")){					
							document.frmAddMessage.message.value += "[" + code + "=" + insertCode + "]" + insertText + "[/" + code + "]";
						}
				}
		}
		else {
			document.frmAddMessage.message.value += "[" + code + "= ][/" + code + "]";
		}
	}
	
	
	//For the email code
	if ((code != "") && (code == "EMAIL")) {
		if (promptOn == true){
			insertText = prompt("<% = strTxtEnterEmailText %>", "");
				
				if ((insertText != null) && (insertText != "")){
					insertCode = prompt("<% = strTxtEnterEmailMailto %>", "");
						
						if ((insertCode != null) && (insertCode != "")){					
						document.frmAddMessage.message.value += "[" + code + "=" + insertCode + "]" + insertText + "[/" + code + "]";
					}
				}
		}
		else {
			document.frmAddMessage.message.value += "[" + code + "= ][/" + code + "]";
		}
	}
	
	//For the image code
	if ((code != "") && (code == "IMG")) {
		if (promptOn == true){	
			insertCode = prompt("<% = strTxtEnterImageURL %>", "http://");
						
				if ((insertCode != null) && (insertCode != "")){					
				document.frmAddMessage.message.value += "[" + code + "]" + insertCode + "[/" + code + "]";
			}			
		}
		else {
			document.frmAddMessage.message.value += "[" + code + "][/" + code + "]";
		}
	}
	
	//For the list code
	if ((code != "") && (code == "LIST")) {
		if (promptOn == true){
			listType = prompt("<% = strTxtEnterTypeOfList %> \n<% = strTxtEnterEnter %> \'1\' <% = strTxtEnterNumOrBlankList %>", "");
			
			while ((listType != null) && (listType != "") && (listType != "1")) {
				listType = prompt("<% = strTxtEnterListError %> \'1\' <% = strTxtEnterNumOrBlankList %>","");               
			}
			
			if (listType != null) {			
				var listItem = "1";
				var insertCode = "";
				
				while ((listItem != "") && (listItem != null)) {
					listItem = prompt("<% = strEnterLeaveBlankForEndList %>",""); 
					if (listItem != "") {             
						insertCode += "[LI]" + listItem + "[/LI]"; 
					}                   
				} 
				
				if (listType == "") {
					document.frmAddMessage.message.value += "[" + code + "]" + insertCode + "[/" + code + "]";
				} else {
					document.frmAddMessage.message.value += "[" + code + "=" + listType + "]" + insertCode + "[/" + code + "=" + listType + "]";
				} 
				
			}
		}
		else{
			document.frmAddMessage.message.value += "[" + code + "][LI] [/LI][LI] [/LI][LI] [/LI][/" + code + "]";
		}			
	}
	
	
	//For the indent
	if ((code != "") && (code == "INDENT")) {
						
			document.frmAddMessage.message.value += "      ";				
	}
				
	document.frmAddMessage.message.focus();
}

//Function to add the code to the message for the smileys
function AddSmileyIcon(iconCode) {
 	var txtarea = document.frmAddMessage.message;
 	iconCode = ' ' + iconCode + ' ';
 	if (txtarea.createTextRange && txtarea.caretPos) {
  		var caretPos = txtarea.caretPos;
  		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? iconCode + ' ' : iconCode;
  		txtarea.focus();
 	} else {
  		txtarea.value  += iconCode;
  		txtarea.focus();
 	}
}

//Insert at Claret position.
function storeCaret(cursorPosition) {
 	if (cursorPosition.createTextRange) cursorPosition.caretPos = document.selection.createRange().duplicate();
}
