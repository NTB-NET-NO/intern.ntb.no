<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Declare variables
Dim rsAddBuddyList	'Db recorset to add the new buddy
Dim strUsername		'Holds the usrename of the new buddy
Dim strDescription	'Holds a short description of the buddy
Dim blnBlocked		'Set to true if the users is blocked from messaging
Dim intCode		'Return page code
Dim intErrorNum		'Holds the error number
Dim lngAuthorID		'Holds the authors user ID

'Set the return page code
intCode = 1

'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If


'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If



'Read in the details from the form
strUsername = Trim(Mid(Request.Form("username"), 1, 15))
strDescription = Trim(Mid(Request.Form("description"), 1, 30))
blnBlocked = CBool(Request.Form("blocked"))


'Take out parts of the username that are not permitted
strUsername = disallowedMemberNames(strUsername)


'Clean up user input
strUsername = formatSQLInput(strUsername)
strDescription = formatInput(strDescription)



'Check that the new buddy exsists
	
'Initalise the SQL string to query the database to see if the uername exists
strSQL = "SELECT " & strDbTable & "Author.Author_ID FROM " & strDbTable & "Author "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Username = '" & strUsername & "';"

'Open the recordset
rsCommon.Open strSQL, adoCon



'If the user exsist check there not in the list and then add them
If NOT rsCommon.EOF Then
	
	'Get the author ID
	lngAuthorID = CLng(rsCommon("Author_ID"))
	
	'Intialise the ADO recordset object
	Set rsAddBuddyList = Server.CreateObject("ADODB.Recordset")
		
	'Initalise the SQL string with a query to check to see if user is already in list
	strSQL = "SELECT " & strDbTable & "BuddyList.* FROM " & strDbTable & "BuddyList "
	strSQL = strSQL & "WHERE " & strDbTable & "BuddyList.Buddy_ID = " & lngAuthorID & " AND " & strDbTable & "BuddyList.Author_ID = " & lngLoggedInUserID & ";"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsAddBuddyList.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsAddBuddyList.LockType = 3
	
	'Open the recordset
	rsAddBuddyList.Open strSQL, adoCon
	
	'If no record is returned the buddy is not already in the buddy list so eneter them
	If rsAddBuddyList.EOF Then
		
		'Add the new buddy
		rsAddBuddyList.AddNew
		rsAddBuddyList.Fields("Author_ID") = lngLoggedInUserID
		rsAddBuddyList.Fields("Buddy_ID") = lngAuthorID
		rsAddBuddyList.Fields("Description") = strDescription
		rsAddBuddyList.Fields("Block") = blnBlocked
		rsAddBuddyList.Update
		
		'Set the msg varaible to let the user know the buddy has been added
		intCode = 2
		
	'Else the buddy is alreay entered so set the msg varaiable to tell the user
	Else
		intErrorNum = 1
	End If

	'Clear up
	rsAddBuddyList.Close
	Set rsAddBuddyList = Nothing

Else
	'Tell the next page to display an error msg as user is not found
	intErrorNum = 2
End If

'Clear up
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'Remove anti SQL injection code
strUsername = Replace(strUsername, "''", "'", 1, -1, 1)

'Return to the page showing the threads
Response.Redirect "pm_buddy_list.asp?name=" & Server.URLEncode(strUsername) & "&desc=" & Server.URLEncode(strDescription) & "&code=" & intCode & "&ER=" & intErrorNum
%>