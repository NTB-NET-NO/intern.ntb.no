<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Dimension variables
Dim rsNumOfPosts		'Holds the database recordset for the number of posts the user has made
Dim rsForum			'Holds the forum for order
Dim strMode			'Holds the mode of the page
Dim lngTopicID 			'Holds the topic ID number to return to
Dim intForumID			'Holds the forum ID number
Dim lngPollID			'Holds the poll ID number if there is one
Dim lngDelMsgAuthorID		'Holds the deleted message Author ID
Dim lngNumOfPosts		'Holds the number of posts the user has made



'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")
End If



'Read in the message ID number to be deleted
lngTopicID = CLng(Request.Form("TID"))




'Initliase the SQL query to get the topic and forumID from the database
strSQL = "SELECT " & strDbTable & "Topic.Forum_ID "
strSQL = strSQL & "FROM " & strDbTable & "Topic "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
End If

'Clean up
rsCommon.Close



'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)



'Check to make sure the user is deleting the post is a moderator or the forum adminstrator
If blnAdmin = True OR blnModerator = True Then

	'See if there is a poll, if there is get the poll ID and delete

	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "SELECT " & strDbTable & "Topic.Poll_ID "
	strSQL = strSQL & "FROM " & strDbTable & "Topic "
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID="  & lngTopicID  & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Get the Poll ID
	If NOT rsCommon.EOF Then lngPollID = CLng(rsCommon("Poll_ID"))

	'Close the recordset
	rsCommon.Close


	'Get the Posts to be deleted from the database
	strSQL = "SELECT " & strDbTable & "Thread.* "
	strSQL = strSQL & "FROM " & strDbTable & "Thread "
	strSQL = strSQL & "WHERE " & strDbTable & "Thread.Topic_ID= "  & lngTopicID  & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Get the number of posts the user has made and take one away
	Set rsNumOfPosts = Server.CreateObject("ADODB.Recordset")


	'Loop through all the posts for the topic and delete them
	Do While NOT rsCommon.EOF
	
	
		'First we need to delete any entry in the GuestName table incase this was a guest poster posting the message
		strSQL = "DELETE FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID=" & CLng(rsCommon("Thread_ID")) & ";"
	
		'Excute SQL
		adoCon.Execute(strSQL)
		

		'Initalise the strSQL variable with an SQL statement to query the database to get the number of posts the user has made
		strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.No_of_posts FROM " & strDbTable & "Author WHERE " & strDbTable & "Author.Author_ID = " & CLng(rsCommon("Author_ID")) & ";"

		'Query the database
		rsNumOfPosts.Open strSQL, adoCon

		'If there is a record returned by the database then read in the no of posts and decrement it by 1
		If NOT rsNumOfPosts.EOF Then

			'Read in the no of posts the user has made and username
			lngNumOfPosts = CLng(rsNumOfPosts("No_of_posts"))

			'Decrement by 1 unless the number of posts is already 0
			If NOT lngNumOfPosts = 0 Then

				'decrement the number of posts by 1
				lngNumOfPosts = lngNumOfPosts - 1

				'Initalise the SQL string with an SQL update command to update the number of posts the user has made
				strSQL = "UPDATE " & strDbTable & "Author SET "
				strSQL = strSQL & "" & strDbTable & "Author.No_of_posts = " & lngNumOfPosts
				strSQL = strSQL & " WHERE " & strDbTable & "Author.Author_ID= " & CLng(rsCommon("Author_ID")) & ";"

				'Write the updated number of posts to the database
				adoCon.Execute(strSQL)
			End If
		End If

		'Close the recordset
		rsNumOfPosts.Close

		'Move to the next record
		rsCommon.MoveNext
	Loop
	

	'Delete the posts in this topic
	strSQL = "DELETE FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID="  & lngTopicID & ";"

	'Write to database
	adoCon.Execute(strSQL)


	'Delete the Poll in this topic, if there is one
	If lngPollID > 0 Then

		'Delete the Poll choices
		strSQL = "DELETE FROM " & strDbTable & "PollChoice WHERE " & strDbTable & "PollChoice.Poll_ID="  & lngPollID & ";"

		'Write to database
		adoCon.Execute(strSQL)

		'Delete the Poll
		strSQL = "DELETE FROM " & strDbTable & "Poll WHERE " & strDbTable & "Poll.Poll_ID="  & lngPollID & ";"

		'Write to database
		adoCon.Execute(strSQL)
	End If


	'Delete the topic from the database
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "DELETE FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Topic_ID="  & lngTopicID & ";"

	'Write the updated date of last post to the database
	adoCon.Execute(strSQL)


	'Reset Server Objects
	rsCommon.Close
	Set rsNumOfPosts = Nothing
End If


'Update the number of topics and posts in the database
Call updateTopicPostCount(intForumID)


'Reset main server variables
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>
<html>
<head>
<script language="JavaScript">
	window.opener.location.href = "forum_topics.asp?FID=<% = intForumID %>&DL=1"
	window.close();
</script>
</head>
</html>