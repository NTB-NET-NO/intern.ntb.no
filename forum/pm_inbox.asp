<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"

'Declare variables
Dim rsPmMessage			'ADO recordset object holding the users private messages
Dim intRecordPositionPageNum	'Holds the recorset page number to show the other pm message
Dim intTotalNumOfPages		'Holds the total number of pages in the recordset
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intPageLoopCounter	'Holds the number of pages there are of pm messages
Dim intNumOfPMs			'Holds the number of private messages the user has
Dim intForumID

'Initilise varaibles
intNumOfPMs = 0

'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If


'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If


'If this is the first time the page is displayed then the pm message record position is set to page 1
If Request.QueryString("PN") = "" Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the pm message record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("PN"))
End If	


'Intialise the ADO recordset object
Set rsPmMessage = Server.CreateObject("ADODB.Recordset")
	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "PMMessage.*, " & strDbTable & "Author.Username "
strSQL = strSQL & "FROM " & strDbTable & "Author INNER JOIN " & strDbTable & "PMMessage ON " & strDbTable & "Author.Author_ID = " & strDbTable & "PMMessage.From_ID "
strSQL = strSQL & "WHERE " & strDbTable & "PMMessage.Author_ID=" & lngLoggedInUserID & " "
strSQL = strSQL & "ORDER BY " & strDbTable & "PMMessage.PM_Message_Date DESC;"
	
'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
rsPmMessage.CursorType = 1

'Query the database
rsPmMessage.Open strSQL, adoCon

'Set the number of records to display on each page
rsPmMessage.PageSize = 10

'If not eof then get some details
If NOT rsPmMessage.EOF Then 
	'Get the record poistion to display from
	rsPmMessage.AbsolutePage = intRecordPositionPageNum

	'Get the total number of pm's this user has
	intNumOfPMs = rsPmMessage.RecordCount
	
	'Count the number of pages there are in the recordset calculated by the PageSize attribute set above
	intTotalNumOfPages = rsPmMessage.PageCount
End If

'If there are no records on this page and it's above the frist page then set the page position to 1
If rsPmMessage.EOF AND intRecordPositionPageNum > 1 Then Response.Redirect "pm_inbox.asp?PN=1"

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Private Messenger: Inbox</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<%
'If there are more than 1 pm's write the function
If intNumOfPMs > 1 Then

	Response.Write("<script  language=""JavaScript"">")

	Response.Write(vbCrLf & "//Funtion to check or uncheck all the delete boxes")
	Response.Write(vbCrLf & "function checkAll(){")
		
	Response.Write(vbCrLf & vbCrLf & "	for (i=0; i < document.frmDelete.chkDelete.length; i++){")
	Response.Write(vbCrLf & "		document.frmDelete.chkDelete[i].checked = document.frmDelete.chkAll.checked;")
	Response.Write(vbCrLf & "	}")
	Response.Write(vbCrLf & "}")
	Response.Write(vbCrLf & "</script>")
End If

%>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtPrivateMessenger %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtPrivateMessenger %><br /></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><form name="frmDelete" method="post" action="pm_delete_message.asp?Page=<% = intRecordPositionPageNum %>" OnSubmit="return confirm('<% = strTxtDeletePrivateMessageAlert %>')">
 <td>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr> 
  <td width="60%"><span class="lgText"><img src="<% = strImagePath %>subject_folder.gif" alt="<% = strTxtSubjectFolder %>" align="absmiddle"> <% = strTxtPrivateMessenger & ": " & strTxtInbox %></span></td>
  <td align="right" width="40%" nowrap="nowrap"><a href="pm_inbox.asp" target="_self"><img src="<% = strImagePath %>inbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtInbox %>" border="0"></a><a href="pm_outbox.asp" target="_self"><img src="<% = strImagePath %>outbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtOutbox %>" border="0"></a><a href="pm_buddy_list.asp" target="_self"><img src="<% = strImagePath %>buddy_list.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtBuddyList %>" border="0"></a><a href="pm_new_message_form.asp" target="_self"><img src="<% = strImagePath %>new_private_message.gif" alt="<% = strTxtNewPrivateMessage %>" border="0"></a></td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" width="3%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>" align="center"><% = strTxtRead %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="39%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtMessageTitle %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="22%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtMessageFrom %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="31%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtDate %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="5%" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><%

'If there are more than 1 pm's writethe check all box
If intNumOfPMs > 1 Then Response.Write("<input type=""checkbox"" name=""chkAll"" onClick=""checkAll();"">") Else Response.Write("&nbsp;")

Response.Write("         </td>")
Response.Write("    </tr>")
    
'Check there are PM messages to display
If rsPmMessage.EOF Then

	'If there are no pm messages to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtNoPrivateMessages & " " & strTxtInbox & "<input type=""hidden"" name=""chkDelete"" value=""-1""></td>"

'Else there the are topic's so write the HTML to display the topic names and a discription
Else 
	
	'Loop round to read in all the Topics in the database
	For intRecordLoopCounter = 1 to 10 
	
		'Exit loop if run out of records
		If rsPmMessage.EOF Then Exit For
	%>
    <tr> 
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="3%" class="text" align="center"><% 
      
     		If CBool(rsPmMessage("Read_Post")) = False Then
     			Response.Write("<img src=""" & strImagePath & "unread_private_message.gif"" alt=""" & strTxtUnreadMessage & """>")
     		Else
     			Response.Write("<img src=""" & strImagePath & "read_private_message.gif"" alt=""" & strTxtReadMessage & """>")
     		End If
     
     %>
     </td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="39%" class="text"><% Response.Write("<a href=""pm_show_message.asp?ID=" & rsPmMessage("PM_ID") & """ target=""_self"">" & rsPmMessage("PM_Tittle") & "</a>") %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="22%" class="text"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = rsPmMessage("From_ID") %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"><% = rsPmMessage("Username") %></a></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="31%" class="smText" nowrap="nowrap"><% Response.Write(DateFormat(rsPmMessage("PM_Message_Date"), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(rsPmMessage("PM_Message_Date"), saryDateTimeData)) %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="5%" class="text" align="center"><input type="checkbox" name="chkDelete" value="<% = rsPmMessage("PM_ID") %>"></td>
    </tr><%
		
		'Move to the next recordset
		rsPmMessage.MoveNext
	Next
End If
%>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
    <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="2" align="center">
     <tr>
      <td width="94%" class="text"><img src="<% = strImagePath %>unread_private_message.gif" alt="<% = strTxtUnreadMessage %>"> <% = strTxtUnreadMessage %>&nbsp;&nbsp;<img src="<% = strImagePath %>read_private_message.gif" alt="<% = strTxtReadMessage %>"> <% = strTxtReadMessage %></td>
      <td width="6%" align="right"><% 

'Display delete buttons      
If intNumOfPMs > 0 Then  
      	
      	Response.Write("<input type=""submit"" name=""delAll"" value=""" & strTxtDelete & " " & strTxtAll & """ />")
      	Response.Write("&nbsp;<input type=""submit"" name=""delSel"" value=""" & strTxtDelete & " " & strTxtSelected & """ />") 
      	
Else 
      	Response.Write("&nbsp;") 
      	
End If

%></td>
     </tr>
    </table>
    <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="2" align="center">
     <tr>
      <td class="text" colspan="2"><% Response.Write(strLoggedInUsername & ", " & strTxtYouHave & " " & intNumOfPMs & " " & strTxtPrivateMessagesYouCanReceiveAnother & " " & (intNumPrivateMessages-intNumOfPMs) & " " & strTxtOutOf & " " & intNumPrivateMessages) %></td>
     </tr>
     <tr> 
      <td class="text"><table width="300" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>">
        <tr> 
         <td> <table width="100%" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
           <tr><td colspan="3" align="center" class="smText"><% = strTxtYourInboxIs & " " & FormatPercent((intNumOfPMs / intNumPrivateMessages), 0) & " " & strTxtFull %></td></tr>
           <tr><td colspan="3"><img src="<% = strImagePath %>bar_graph_image.gif" width="<% = FormatPercent((intNumOfPMs / intNumPrivateMessages), 0) %>" height="11"></td></tr>
           <tr>
            <td width="30%" class="smText">0%</td>
            <td width="41%" align="center" class="smText">50%</td>
            <td width="29%" align="right" class="smText">100%</td>
           </tr>
          </table></tr>
       </table> 
       </td><%

'If there is more than 1 page of topics then dispaly drop down list to the other topics
If intTotalNumOfPages > 1 Then   

	'Display an drop down list to the other members in list
	Response.Write (vbCrLf & "		<td align=""right"" class=""text"">")
		
	'Display a prev link if previous pages are available
	If intRecordPositionPageNum > 1 Then Response.Write("<a href=""pm_inbox.asp?PN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")
		
	Response.Write (strTxtPage & " " & _
	vbCrLf & "		 <select onChange=""ForumJump(this)"" name=""SelectTopicPage"">")
	
	'Loop round to display links to all the other pages
	For intPageLoopCounter = 1 to intTotalNumOfPages  
	
		'Display a link in the link list to the another topic page
		Response.Write (vbCrLf & "		  <option value=""pm_inbox.asp?PN=" & intPageLoopCounter & """")
		
		'If this page number to display is the same as the page being displayed then make sure it's selected
		If intPageLoopCounter = intRecordPositionPageNum Then
			Response.Write (" selected")
		End If
		
		'Display the link page number
		Response.Write (">" & intPageLoopCounter & "</option>")
	
	Next
	
	'End the drop down list
	Response.Write (vbCrLf & "		</select> " & strTxtOf & " " & intTotalNumOfPages)
		
	'Display a next link if needed
	If intRecordPositionPageNum <> intTotalNumOfPages Then Response.Write("&nbsp;<a href=""pm_inbox.asp?PN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")
		
	Response.Write("</td>")
End If
%>
     </tr>
    </table>
</td>
</form></tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
  <tr><form>
   <td><!-- #include file="includes/forum_jump_inc.asp" --></td>
   </form>
  </tr>
 </table>
<div align="center"><br />
 <%
'Clear server objects
rsPmMessage.Close
Set rsPmMessage = Nothing
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******


'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"

'Display a msg if a PM has been deleted
If Request.QueryString("MSG") = "DEL" Then
		Response.Write("<script  language=""JavaScript"">")
		Response.Write("alert('" & strTxtMeassageDeleted & ".');")
		Response.Write("</script>")
End If
%>
 <!-- #include file="includes/footer.asp" -->