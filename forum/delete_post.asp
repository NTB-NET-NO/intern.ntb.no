<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Dimension variables
Dim rsTopicDelete		'Holds the deleting recordset
Dim intForumID			'Holds the forum ID number
Dim strMode			'Holds the mode of the page
Dim lngTopicID			'Holds the Topic ID number
Dim lngMessageID		'Holds the message ID to be deleted
Dim lngDelMsgAuthorID		'Holds the deleted message Author ID
Dim lngNumOfPosts		'Holds the number of posts the user has made


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If



'Read in the message ID number to be deleted
lngMessageID = CLng(Request.QueryString("PID"))



'Read in the forum and topic ID from the database for this message

'Initliase the SQL query to get the topic and forumID from the database
strSQL = "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID "
strSQL = strSQL & "FROM " & strDbTable & "Topic, " & strDbTable & "Thread "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID AND " & strDbTable & "Thread.Thread_ID=" & lngMessageID & ";"
	
'Query the database
rsCommon.Open strSQL, adoCon 

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	lngTopicID = CLng(rsCommon("Topic_ID"))
	intForumID = CInt(rsCommon("Forum_ID"))
End If

'Clean up
rsCommon.Close
	



'Read in the forum name and forum permissions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'Read in wether the forum is locked or not
If NOT rsCommon.EOF Then 
	
	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, 0, 0, 0, 0, CInt(rsCommon("Delete_posts")), 0, 0, 0, 0, 0)
End If

'Clean up
rsCommon.Close




'Get the Post to be deleted from the database
	
'Initalise the strSQL variable with an SQL statement to get the post from the database
strSQL = "SELECT " & strDbTable & "Thread.* FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Thread_ID ="  & lngMessageID & ";"

'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
rsCommon.CursorType = 2

'Set set the lock type of the recordset to optomistic while the record is deleted
rsCommon.LockType = 3

'Query the database
rsCommon.Open strSQL, adoCon  

'Read in the author ID of the message to be deleted
If NOT rsCommon.EOF Then lngDelMsgAuthorID = CLng(rsCommon("Author_ID"))




'Check to make sure the user is deleting the post enetered the post or a moderator with detlete rights or the forum adminstrator
If (lngDelMsgAuthorID = lngLoggedInUserID OR blnAdmin = True OR blnModerator = True) AND (blnDelete = True OR blnAdmin = True) Then


	'First we need to delete any entry in the GuestName table incase this was a guest poster posting the message
	strSQL = "DELETE FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID="  & lngMessageID & ";"

	'Excute SQL
	adoCon.Execute(strSQL)
	
	


	'Delete the record set
	rsCommon.Delete
	
	'We need to requry the database before moving on as Access can take a few moments to delete the record
	rsCommon.Requery
	
	'Close the recordset
	rsCommon.Close
	
	
		
	'Initalise the strSQL variable with an SQL statement to query the database to get the number of posts the user has made
	strSQL = "SELECT " & strDbTable & "Author.No_of_posts " 
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID= " & lngDelMsgAuthorID & ";"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
			
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
			
	'Query the database
	rsCommon.Open strSQL, adoCon
		
	'If there is a record returned by the database then read in the no of posts and decrement it by 1
	If NOT rsCommon.EOF Then
		
		'Read in the no of posts the user has made and username
		lngNumOfPosts = CLng(rsCommon("No_of_posts"))
		
		'decrement the number of posts by 1
		lngNumOfPosts = lngNumOfPosts - 1
			
		'Place the new number of posts in the recordset
		rsCommon.Fields("No_of_posts") =  lngNumOfPosts
		
		'Update the database
		rsCommon.Update	
	End If
		
	'Close the recordset
	rsCommon.Close
	
	
	
	
	'Check there are other Threads for the Topic 	
	'Initalise the strSQL variable with an SQL statement to get the Threads from the database
	strSQL = "SELECT " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Message_Date FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID ="  & lngTopicID & " ORDER BY " & strDbTable & "Thread.Message_date ASC;"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Get the Topic from the database to be deleted
	'Create a recordset object for the Topic in the database
	Set rsTopicDelete = Server.CreateObject("ADODB.Recordset")
		
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "SELECT " & strDbTable & "Topic.* FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Topic_ID ="  & lngTopicID & ";"
		
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsTopicDelete.CursorType = 2
		
	'Set set the lock type of the recordset to optomistic while the record is deleted
	rsTopicDelete.LockType = 3
		
	'Query the database
	rsTopicDelete.Open strSQL, adoCon  
	
	'If there are threads left make sure the last topic date has the date of the last entry
	If NOT rsCommon.EOF Then
		
		'Place the date of the start date of the first message in the topic table
		rsTopicDelete("Start_date") = CDate(rsCommon("Message_date"))
		
		'Move to the last message in the topic to get the last message date
		rsCommon.MoveLast
		
		'Place the date of the last post in the last enry date
		rsTopicDelete("Last_entry_date") = CDate(rsCommon("Message_date"))
		rsTopicDelete.Update
		
	
	
	
	'If there are no more posts in the database for the topic then delete the topic from the database
	Else	
		
		'If there is a poll and no more posts left delete the poll as well
		If CLng(rsTopicDelete("Poll_ID")) <> 0 Then 

		          'Delete the Poll choices 
		          strSQL = "DELETE FROM " & strDbTable & "PollChoice WHERE " & strDbTable & "PollChoice.Poll_ID=" & CLng(rsTopicDelete("Poll_ID")) & ";" 
		
		          'Write to database 
		          adoCon.Execute(strSQL) 
		
		          'Delete the Poll 
		          strSQL = "DELETE FROM " & strDbTable & "Poll WHERE " & strDbTable & "Poll.Poll_ID=" & CLng(rsTopicDelete("Poll_ID")) & ";" 
		
		          'Write to database 
		          adoCon.Execute(strSQL) 
    		End If
		
		
		'Delete the record set
		rsTopicDelete.Delete
		
		'Update the number of topics and posts in the database
		Call updateTopicPostCount(intForumID)
		
		'Reset Server Objects
		rsCommon.Close
		Set rsCommon = Nothing
		rsTopicDelete.Close
		Set rsTopicDelete = Nothing
		adoCon.Close
		Set adoCon = Nothing
		
		'Return to the page showing the the topics in the forum
		Response.Redirect "forum_topics.asp?FID=" & intForumID & "&PN=" & Request.QueryString("PN")
	End If
	
	
	'Release server objects
	rsTopicDelete.Close
	Set rsTopicDelete = Nothing
End If



'Update the number of topics and posts in the database
Call updateTopicPostCount(intForumID)



'Reset Server Objects
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing




'Return to the page showing the threads
Response.Redirect "forum_posts.asp?FID=" & intForumID & "&TID=" & lngTopicID & "&PN=" & Request.QueryString("PN") & "&TPN=" & Request.QueryString("TPN")
%>