<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by email ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'member_control_panel.asp
'---------------------------------------------------------------------------------

Const strTxtChangeProfile = "Make changes to your profile and forum preferences"
Const strTxtAlterEmailSubscriptions = "Manage your Forum and Topic email subscriptions"
Const strTxtReadandSendPMs = "Read and send Private Messages between yourself and other forum members"
Const strTxtListOfYourForumBuddies = "List of your forum buddies"
Const strTxtBuddyList = "Buddy List"
Const strTxtForumHelp = "Forum Help"
Const strTxtForumHelpFilesandFAQsToHelpYou = "Forum Help files and FAQ's to help you with any difficulties you may have when using the forum"
Const strTxtAdminAndModFunc = "Admin and Moderator functions"
Const strTxtAdminFunctionsTo = "Admin and Moderator functions to delete, suspend, change user group etc. of member"
Const strTxtChangePassAndEmail = "Make changes to your login password and/or email address"
Const strTxtProfileInfo = "Profile Information"
Const strTxtChangeProfileInfo = "Make changes to the information on yourself held in your forum profile"
Const strTxtChangeForumPreferences = "Make changes to your forum preferences"

%>