<p>Hvis du godtar f�lgende regler klikker du p� 'Godta' nederst p� siden. Hvis ikke klikker du p� 'Avbryte'.</p>
<p>N�r du registrerer deg m� du gi enkelte opplysninger. Noen av disse er valgfrie. De opplysningene du gir m� anses som offentlige.</p>
<p>Du godtar � IKKE bruke dette forum til � publisere vulg�re, un�yaktige, truende, nedlatende eller andre innlegg som rammes av norsk lov. Du m� heller ikke publisere kopibeskyttet materiale som ikke eies av deg selv.</p>
<p>Du er selv ansvarlig for innholdet i dine innlegg. Forumadministrator, eier eller distribut�r kan ikke holdes ansvarlig for ulovlig bruk.</p>
<p>Vi forbeholder oss retten til � slette enthvert innlegg og/eller bruker uten forvarsel.</p>
<p>Ved � registrere deg som bruker i dette forum godtar du � f�lge disse reglene.</p>