<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<!--#include file="functions/functions_format_post.asp" -->
<!--#include file="functions/functions_edit_post.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True


'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"






'Dimension variables
Dim rsCommonT			'Holds the forum permisisons to be checked
Dim strForumName		'Holds the forum name
Dim strForumDescription		'Holds the description of the forum
Dim lngTopicID			'Holds the topic number
Dim lngMessageID		'Holds the message ID number
Dim strSubject			'Holds the topic subject
Dim strUsername 		'Holds the Username of the thread
Dim lngUserID			'Holds the ID number of the user
Dim strAuthorSignature		'Holds the authors signature
Dim dtmTopicDate		'Holds the date the thread was made
Dim strMessage			'Holds the message body of the thread
Dim intForumID			'Holds the ID number of the forum


'Initialise variables
lngTopicID = 0	


'Read in the Forum ID to display the Topics for
lngTopicID = CLng(Request.QueryString("TID"))


'If there no Topic ID then redirect the user to the main forum page
If lngTopicID = 0 Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect "default.asp"
End If


'Get the posts from the database

'Initalise the strSQL variable with an SQL statement to query the database get the thread details
strSQL = "SELECT " & strDbTable & "Forum.Forum_ID, " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Message, " & strDbTable & "Thread.Message_date, " & strDbTable & "Thread.Show_signature, " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum.Forum_description, " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username, " & strDbTable & "Author.Signature, " & strDbTable & "Topic.Subject "
strSQL = strSQL & "FROM (" & strDbTable & "Forum INNER JOIN " & strDbTable & "Topic ON " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID) INNER JOIN (" & strDbTable & "Author INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID) ON " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID "
strSQL = strSQL & "WHERE (((" & strDbTable & "Thread.Topic_ID)=" & lngTopicID & ")) "
strSQL = strSQL & "ORDER by " & strDbTable & "Thread.Message_Date ASC;"

'Query the database
rsCommon.Open strSQL, adoCon

'If there is no topic in the database then display the appropraite mesasage
If rsCommon.EOF Then
	'If there are no thread's to display then display the appropriate error message
	strSubject = strNoThreads

Else
	'Read in the thread subject
	strSubject = rsCommon("Subject")
	
	'Read in the forum ID to check if the user can view the post
	intForumID = rsCommon("Forum_ID")
End If





'Create a recordset to check if the user is allowe to view posts in this forum
Set rsCommonT = Server.CreateObject("ADODB.Recordset")

'Read in the forum name and forum permssions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommonT.Open strSQL, adoCon


'If there is a record returned by the recordset then check to see if you need a password to enter it
If NOT rsCommonT.EOF Then

	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsCommonT("Read")), CInt(rsCommonT("Post")), CInt(rsCommonT("Reply_posts")), CInt(rsCommonT("Edit_posts")), CInt(rsCommonT("Delete_posts")), 0, CInt(rsCommonT("Poll_create")), CInt(rsCommonT("Vote")), CInt(rsCommonT("Attachments")), CInt(rsCommonT("Image_upload")))

	'If the user has no read writes then kick them
	If blnRead = False Then

		'Reset Server Objects
		rsCommonT.Close
		Set rsCommonT = Nothing
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing


		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "insufficient_permission.asp"
	End If

	'If the forum requires a password and a logged in forum code is not found on the users machine then send them to a login page
	If rsCommonT("Password") <> "" AND Request.Cookies(strCookieName)("Forum" & intForumID) <> rsCommonT("Forum_code") Then

		'Reset Server Objects
		rsCommonT.Close
		Set rsCommonT = Nothing
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing

		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "forum_password_form.asp?RP=PT&FID=" & intForumID & "&TID=" & lngTopicID
	End If
End If

'Clean up
rsCommonT.Close
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title><% = strMainForumName %>: <% = strSubject %></title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!--#include file="includes/skin_file.asp" -->
<style type="text/css">
<!--
.text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color : #000000;
	font-size: 13px;
	font-weight: normal;
}
.bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color : #000000;
	font-size: 13px;
	font-weight: bold;
}
a  {
	color : #0000FF;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	text-decoration: underline;
	font-size: 13px;
	font-weight: normal;
}

a:hover {
	color : #FF0000;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	text-decoration : underline;
	font-size: 13px;
	font-weight: normal;
}

a:visited {
	color : #990099;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	text-decoration : underline;
	font-size: 13px;
	font-weight: normal;
}

a:visited:hover {
	color : #FF0000;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	text-decoration : underline;
	font-size: 13px;
	font-weight: normal;
}
-->
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#0000CC" vlink="#0000CC" alink="#FF0000" OnLoad="self.focus();">
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center"><a href="javascript:onClick=window.print()"><% = strTxtPrintPage %></a> | <a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a></td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td class="text"> <br />
      <%
strTableQuoteBorderColour = "#CCCCCC"
strTableQuoteColour = "#FFFFFF"	      

'If there are no threads returned by the qury then display an error message
If rsCommon.EOF Then
%>
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td align="center" height="66"><font size="4"><% = strNoThreads %></font></td>
        </tr>
      </table><%
'Display the threads
Else

	'Read in threads details for the topic from the database
	lngMessageID = CLng(rsCommon("Thread_ID"))
	strMessage = rsCommon("Message")
	lngUserID = CLng(rsCommon("Author_ID"))
	strUsername = rsCommon("Username")
	dtmTopicDate = CDate(rsCommon("Message_date"))
	strForumName = rsCommon("Forum_name")
	strForumDescription = rsCommon("Forum_description")
	strAuthorSignature = rsCommon("Signature")
	
	'If the poster is a guest see if they have entered their name in the GuestName table and get it
	If lngUserID = 2 Then
		
		'Initalise the strSQL variable with an SQL statement to query the database
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "GuestPoster @lngThreadID = " & lngMessageID
		Else
			strSQL = "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = " & lngMessageID & ";"
		End If
		
		'Query the database
		rsCommonT.Open strSQL, adoCon
		
		'Read in the guest posters name
		If NOT rsCommonT.EOF Then strUsername = rsCommonT("Name")
		
		'Close the recordset
		rsCommonT.Close
	End If

	'If the message has been edited remove who edited the post
	If InStr(1, strMessage, "<edited>", 1) Then strMessage = removeEditorAuthor(strMessage)

	'Convert message to text
	strMessage = ConvertToText(strMessage)

	'If the post contains a quote or code block then format it
	If InStr(1, strMessage, "[QUOTE=", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatUserQuote(strMessage)
	If InStr(1, strMessage, "[QUOTE]", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatQuote(strMessage)
	If InStr(1, strMessage, "[CODE]", 1) > 0 AND InStr(1, strMessage, "[/CODE]", 1) > 0 Then strMessage = formatCode(strMessage)


	'If the post contains a flash link then format it
	If blnFlashFiles Then
		If InStr(1, strMessage, "[FLASH", 1) > 0 AND InStr(1, strMessage, "[/FLASH]", 1) > 0 Then strMessage = formatFlash(strMessage)
		If InStr(1, strAuthorSignature, "[FLASH", 1) > 0 AND InStr(1, strAuthorSignature, "[/FLASH]", 1) > 0 Then strAuthorSignature = formatFlash(strAuthorSignature)
	End If

	'If the user wants there signature shown then attach it to the message
	If rsCommon("Show_signature") AND strAuthorSignature <> "" Then 
		strAuthorSignature = ConvertToText(strAuthorSignature)
		strMessage = strMessage & "<!-- Signature --><br /><br />-------------<br />" & strAuthorSignature
	End If

	'Strip long text strings from message
	strMessage = removeLongText(strMessage)
    %>
      <span class="bold" style="font-size: 16px;"><% = strSubject %></span> <br />
      <br />
      <span class="bold"><% = strTxtPrintedFrom %>: </span><% = strWebsiteName %>
      <br /><span class="bold"><% = strTxtForumName %>: </span> <% = strForumName %>
      <br /><span class="bold"><% = strTxtForumDiscription %>: </span> <% = strForumDescription %>
      <br /><span class="bold"><% = strTxtURL %>: </span><% = strForumPath %>/forum_posts.asp?TID=<% = lngTopicID %>
      <br /><span class="bold"><% = strTxtPrintedDate %>: </span><% = DateFormat(Now(), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(Now(), saryDateTimeData) %>
      <% If blnLCode = True Then %><br /><span class="bold"><% = strTxtSoftwareVersion %>:</span> Web Wiz Forums <% = strVersion %> - http://www.webwizforums.com<% End If %>
      <br /><br /><br />
      <span class="bold"><% = strTxtTopic %>:</span> <% = strSubject %>
      <hr /><br />
      <span class="bold"><% = strTxtPostedBy %>:</span> <% = strUsername %>
      <br /><span class="bold"><% = strTxtSubjectFolder %>:</span> <% = strSubject %>
      <br /><span class="bold"><% = strTxtDatePosted %>:</span> <% = DateFormat(dtmTopicDate, saryDateTimeData) %>&nbsp;<% = strTxtAt %>&nbsp;<% = TimeFormat(dtmTopicDate, saryDateTimeData) %>
      <br /><br />
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="text">
<%  Response.Write("	<!-- Message body -->" & vbCrLf & strMessage & vbCrLf &  "<!-- Message body ''"""" -->") %>
         </td>
        </tr>
       </table>
      <br /><br /><%
      	'Move to the next database record
	rsCommon.MoveNext

      	If NOT rsCommon.EOF Then Response.Write "<span class=""bold"">" & strTxtReplies & ": </span>"
      	%>
      <hr /><%


      	'Loop round to read in all the thread's in the database
	Do While NOT rsCommon.EOF

		'Read in threads details for the topic from the database
		lngMessageID = CLng(rsCommon("Thread_ID"))
		strMessage = rsCommon("Message")
		lngUserID = CLng(rsCommon("Author_ID"))
		strUsername = rsCommon("Username")
		dtmTopicDate = CDate(rsCommon("Message_date"))
		strAuthorSignature = rsCommon("Signature")
		
		'If the poster is a guest see if they have entered their name in the GuestName table and get it
		If lngUserID = 2 Then
			
			'Initalise the strSQL variable with an SQL statement to query the database
			If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "GuestPoster @lngThreadID = " & lngMessageID
			Else
				strSQL = "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = " & lngMessageID & ";"
			End If
			
			'Query the database
			rsCommonT.Open strSQL, adoCon
			
			'Read in the guest posters name
			If NOT rsCommonT.EOF Then strUsername = rsCommonT("Name")
			
			'Close the recordset
			rsCommonT.Close
		End If

		'If the message has been edited remove who edited the post
		If InStr(1, strMessage, "<edited>", 1) Then strMessage = removeEditorAuthor(strMessage)

		'Convert message to text
		strMessage = ConvertToText(strMessage)
		

		'If the post contains a quote or code block then format it
		If InStr(1, strMessage, "[QUOTE=", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatUserQuote(strMessage)
		If InStr(1, strMessage, "[QUOTE]", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatQuote(strMessage)
		If InStr(1, strMessage, "[CODE]", 1) > 0 AND InStr(1, strMessage, "[/CODE]", 1) > 0 Then strMessage = formatCode(strMessage)


		'If the post contains a flash link then format it
		If blnFlashFiles Then
			If InStr(1, strMessage, "[FLASH", 1) > 0 AND InStr(1, strMessage, "[/FLASH]", 1) > 0 Then strMessage = formatFlash(strMessage)
			If InStr(1, strAuthorSignature, "[FLASH", 1) > 0 AND InStr(1, strAuthorSignature, "[/FLASH]", 1) > 0 Then strAuthorSignature = formatFlash(strAuthorSignature)
		End If
		
		'If the user wants there signature shown then attach it to the message
		If rsCommon("Show_signature") Then 
			strAuthorSignature = ConvertToText(strAuthorSignature)
			strMessage = strMessage & "<!-- Signature --><br /><br />-------------<br />" & strAuthorSignature
		End If
		
		'Strip long text strings from message
		strMessage = removeLongText(strMessage)

	      %>
      <br />
      <span class="bold"><% = strTxtPostedBy %>:</span> <% = strUsername %>
      <br />
      <span class="bold"><% = strTxtDatePosted %>:</span> <% = DateFormat(dtmTopicDate, saryDateTimeData) %>&nbsp;<% = strTxtAt %>&nbsp;<% = TimeFormat(dtmTopicDate, saryDateTimeData) %>
      <br /><br />
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="text">
<%  Response.Write("	<!-- Message body -->" & vbCrLf & strMessage & vbCrLf &  "<!-- Message body ''"""" -->") %>
         </td>
        </tr>
       </table>
      <br />
      <hr />
      <%
	      	'Move to the next database record
		rsCommon.MoveNext
	Loop
%>
    </td>
  </tr>
</table>
<br />
<%
End If

'Reset server variables
rsCommon.Close
Set rsCommon = Nothing
Set rsCommonT = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center"><a href="javascript:onClick=window.print()"><% = strTxtPrintPage %></a> | <a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a>
    <br /><br /><%
     
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	Response.Write("<span class=""text"" style=""font-size:10px"">Powered by Web Wiz Forums version " & strVersion & " - http://www.webwizforums.com</span>")
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 Web Wiz Guide - http://www.webwizguide.info</span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

%>
</td>
  </tr>
</table>
</body>
</html>