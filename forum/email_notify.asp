<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Declare variables
Dim lngTopicID		'Holds the topic ID
Dim intForumID		'Holds the forum ID
Dim strReturnValue	'Holds the return value of the page
Dim strMode		'Holds the mode of the page
Dim strReturnPage	'Holds the return page
Dim intReadPermission	'Holds if the user has permission to read in this forum


'Read in the forum or topic ID
intForumID = CInt(Request("FID"))
lngTopicID = CLng(Request.QueryString("TID"))
strMode = Request.QueryString("M")





'If there is no forum ID or Topic ID then send to the main forum page
If intForumID = "" AND lngTopicID = "" Then 
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	Response.Redirect("default.asp")
End If






'If this is a Topic to watch then watch or unwatch this topic
If lngTopicID AND blnEmail = True AND intGroupID <> 2 AND strMode = "" Then Call WatchUnWatchTopic(lngTopicID)

'If this is a Topic to watch then watch or unwatch this topic
If intForumID AND blnEmail = True AND intGroupID <> 2 AND strMode = "" Then Call WatchUnWatchForum(intForumID)

'If this is a link form an unsubscribe email notify link in an email unwatch this topic or forum
If strMode = "Unsubscribe" AND intForumID <> "" AND lngTopicID <> "" Then Call UnsubscribeEmailNotify(intForumID, lngTopicID)

'If this is from the subscription page then add to the forum watch list
If intForumID AND blnEmail = True AND intGroupID <> 2 AND strMode = "SP" Then Call WatchUnWatchForum(intForumID)




'******************************************
'***  	  Watch or Unwatch Topic        ***
'******************************************

Private Function WatchUnWatchTopic(lngTopicID)

	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Forum.[Read] FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Forum_ID = " & intForumID & ";"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Read in the read permission for the forum
	intReadPermission = CInt(rsCommon("Read"))
	
	'Close recordset
	rsCommon.Close
	
	
	'Initalise the SQL string with a query to get the email notify topic details
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "TopicEmailNotify @lngAuthorID = " & lngLoggedInUserID & ", @lngTopicID= " & lngTopicID
	Else
		strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
		strSQL = strSQL & "FROM " & strDbTable & "EmailNotify "
		strSQL = strSQL & "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ";"
	End If

	With rsCommon

		'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3
		
		'Query the database
		.Open strSQL, adoCon


		'If the user no-longer wants email notification for this topic then remove the entry form the db
		If NOT .EOF Then
			
			Do while NOT .EOF

				'Delete the db entry
				.Delete
				
				'Move to next record
				.MoveNext
				
				'Set the return value
				strReturnValue = "&EN=TU"
			Loop

		'Else if this is a new post and the user wants to be notified add the new entry to the database
		Else

			'Check to see if the user is allowed to view posts in this forum
			Call forumPermisisons(intForumID, intGroupID, intReadPermission, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			
			'If the user can read in this forum the add them
			If blnRead Then
				'Add new rs
				.AddNew
	
				'Create new entry
				.Fields("Author_ID") = lngLoggedInUserID
				.Fields("Topic_ID") = lngTopicID
	
				'Upade db with new rs
				.Update
				
				'Set the return value
				strReturnValue = "&EN=TS"
			End If
		End If

		'Clean up
		.Close

	End With
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Return to Topic Page
	Response.Redirect("forum_posts.asp?TID=" & lngTopicID & "&PN=" & Request.QueryString("PN") & "&TPN=" & Request.QueryString("TPN") & strReturnValue)
End Function






'******************************************
'***  	  Watch or Unwatch Forum        ***
'******************************************

Private Function WatchUnWatchForum(intForumID)

	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Forum.[Read] FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Forum_ID = " & intForumID & ";"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Read in the read permission for the forum
	intReadPermission = CInt(rsCommon("Read"))
	
	'Close recordset
	rsCommon.Close

	'Initalise the SQL string with a query to get the email notify forum details
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "ForumEmailNotify @lngAuthorID = " & lngLoggedInUserID & ", @intForumID= " & intForumID
	Else
		strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
		strSQL = strSQL & "FROM " & strDbTable & "EmailNotify "
		strSQL = strSQL & "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "EmailNotify.Forum_ID=" & intForumID & ";"
	End If

	With rsCommon

		'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3
		
		'Query the database
		.Open strSQL, adoCon


		'If the user no-longer wants email notification for this topic then remove the entry form the db
		If NOT .EOF Then

			'If this is not from teh subscription page then delete
			If strMode <> "SP" Then 
				Do while NOT .EOF
					'Delete the db entry
					.Delete
					
					'Move to next record
					.MoveNext
					
					'Set the return value
					strReturnValue = "&EN=FU"
				Loop
			End If

		'Else if this is a new post and the user wants to be notified add the new entry to the database
		Else

			'Check to see if the user is allowed to view posts in this forum
			Call forumPermisisons(intForumID, intGroupID, intReadPermission, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			
			'If the user can read in this forum the add them
			If blnRead Then
				
				'Add new rs
				.AddNew
	
				'Create new entry
				.Fields("Author_ID") = lngLoggedInUserID
				.Fields("Forum_ID") = intForumID
	
				'Upade db with new rs
				.Update
				
				'Set the return value
				strReturnValue = "&EN=FS"
			End If
		End If

		'Clean up
		.Close

	End With
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Return to Forum Page
	If strMode = "SP" Then
		Response.Redirect("email_notify_subscriptions.asp")
	Else
		Response.Redirect("forum_topics.asp?FID=" & intForumID & "&PN=" & Request.QueryString("PN") & strReturnValue)
	End If
End Function






'******************************************
'*** Unsubscribe from email notify link ***
'******************************************

Private Function UnsubscribeEmailNotify(intForumID, lngTopicID)

Response.Write("run")

	'If the user is not logged in then send them to the login page
	If intGroupID = 2 Then Response.Redirect("login_user.asp?FID=" & intForumID & "&TID=" & lngTopicID & "&M=Unsubscribe")
	
	'Initalise the SQL string with a query to get the email notify topic details
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "TopicEmailNotify @lngAuthorID = " & lngLoggedInUserID & ", @lngTopicID= " & lngTopicID
	Else
		strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
		strSQL = strSQL & "FROM " & strDbTable & "EmailNotify "
		strSQL = strSQL & "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ";"
	End If

	With rsCommon

		'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3
		
		'Query the database
		.Open strSQL, adoCon


		'If a record is returned then the user is subscribed to the topic so delete their email notification
		If NOT .EOF Then

			Do while NOT .EOF
				'Delete the db entry
				.Delete
				
				'Move to next record
				.MoveNext
				
				'Set the return value
				strReturnValue = "&EN=TU"
				strReturnPage = "forum_posts.asp?TID=" & lngTopicID & strReturnValue
			Loop

		'Else the user is probally got forum post notification so check the db and delete that if they do
		Else
			'Clean up
			.Close
			
			'Initalise the SQL string with a query to get the poll details
			If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "ForumEmailNotify @lngAuthorID = " & lngLoggedInUserID & ", @intForumID= " & intForumID
			Else
				strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
				strSQL = strSQL & "FROM " & strDbTable & "EmailNotify "
				strSQL = strSQL & "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "EmailNotify.Forum_ID=" & intForumID & ";"
			End If
		
		
			'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
			.CursorType = 2
		
			'Set the Lock Type for the records so that the record set is only locked when it is updated
			.LockType = 3
				
			'Query the database
			.Open strSQL, adoCon
		
			'If the user no-longer wants email notification for this topic then remove the entry form the db
			If NOT .EOF Then
		
				Do while NOT .EOF
					'Delete the db entry
					.Delete
					
					'Move to next record
					.MoveNext
						
					'Set the return value
					strReturnValue = "&EN=FU"
					strReturnPage = "forum_topics.asp?FID=" & intForumID & strReturnValue
				Loop
			End If
		
		End If

		'Clean up
		.Close

	End With
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'If there is no return page value then return to the forum
	If strReturnPage = "" Then
		Response.Redirect("forum_topics.asp?FID=" & intForumID)
	'Else return just to forum or topic that is related to
	Else
		Response.Redirect(strReturnPage)
	End If
End Function
%>