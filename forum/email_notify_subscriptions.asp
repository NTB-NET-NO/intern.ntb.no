<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Declare variables
Dim intForumColourNumber	'Holds the number to calculate the table row colour
Dim rsForumSelect		'Recordset for teh forum selection	
Dim strCatName			'Holds the category name
Dim intCatID			'Holds the cat ID
Dim strSelectForumName		'Holds the forum  name
Dim intSelectForumID		'Holds the forum ID
Dim intForumID			'Holds forum ID
Dim lngEmailUserID		'Holds the user ID to look at email notification for
Dim strMode			'Holds the mode of the page
Dim dtmLastEntryDate		'Holds the last entry date

'Initialise variable
intForumColourNumber = 0



'If emial notify is not on then send them away
If blnEmail = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If


'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If



'Read in the mode of the page
strMode = Trim(Mid(Request.QueryString("M"), 1, 1))


'If this is not an admin but in admin mode then see if the user is a moderator
If blnAdmin = False AND strMode = "A" Then
	
	'Initalise the strSQL variable with an SQL statement to query the database
        strSQL = "SELECT " & strDbTable & "Permissions.Moderate "
        strSQL = strSQL & "FROM " & strDbTable & "Permissions "
        If strDatabaseType = "SQLServer" Then
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = 1;"
        Else
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = True;"
        End If
               

        'Query the database
         rsCommon.Open strSQL, adoCon

        'If a record is returned then the user is a moderator in one of the forums
        If NOT rsCommon.EOF Then blnModerator = True

        'Clean up
        rsCommon.Close
End If


'Get the user ID of the email notifications to look at
If (blnAdmin OR (blnModerator AND CLng(Request.QueryString("PF")) > 2)) AND strMode = "A" AND CLng(Request.QueryString("PF")) <> 2 Then
	
	lngEmailUserID = CLng(Request.QueryString("PF"))

'Get the logged in ID number
Else
	lngEmailUserID = lngLoggedInUserID
End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Email Notification Subscriptions</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script  language="JavaScript">






//Funtion to check or uncheck all topic delete boxes
function checkAllTopic(){

 	if (document.frmTopicDel.chkDelete.length > 0) {
  		for (i=0; i < document.frmTopicDel.chkDelete.length; i++){
   				document.frmTopicDel.chkDelete[i].checked = document.frmTopicDel.chkAll.checked;
  		}
 	}
 	else {
  		document.frmTopicDel.chkDelete.checked = document.frmTopicDel.chkAll.checked;
 	}
}


//Funtion to check or uncheck all forum delete boxes
function checkAllForum(){

 	if (document.frmForumDel.chkDelete.length > 0) {
  		for (i=0; i < document.frmForumDel.chkDelete.length; i++){
   			document.frmForumDel.chkDelete[i].checked = document.frmForumDel.chkAll.checked;
  		}
 	}
 	else {
  		document.frmForumDel.chkDelete.checked = document.frmForumDel.chkAll.checked;
 	}
}

//Function to check form is filled in correctly before submitting
function CheckForm () {
	
	var errorMsg = "";
	
	//Check for a forum
	if (document.frmBuddy.FID.value==""){
		errorMsg += "\n\t<% = strTxtSelectForumErrorMsg %>";
	}
	
	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
		
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
	
	return true;
}
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td class="heading"><% = strTxtEmailNotificationSubscriptions %></td>
</tr>
 <tr> 
  <td width="60%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtEmailNotificationSubscriptions %>
  <td width="40%" align="right" nowrap=""nowrap""><a href="member_control_panel.asp<% If strMode = "A" Then Response.Write("?PF=" & lngEmailUserID & "&M=A") %>" target="_self"><img src="<% = strImagePath %>cp_menu.gif" border="0" alt="<% = strTxtMemberCPMenu %>"></a><a href="register.asp<% If strMode = "A" Then Response.Write("?PF=" & lngEmailUserID & "&M=A") %>" target="_self"><img src="<% = strImagePath %>profile.gif" border="0" alt="<% = strTxtEditProfile %>"></a><a href="email_notify_subscriptions.asp<% If strMode = "A" Then Response.Write("?PF=" & lngEmailUserID & "&M=A") %>" target"_self"><img src="<% = strImagePath %>/email_notify.gif" border="0" alt="<% = strTxtEmailNotificationSubscriptions %>"></a></td>
   </td>
  </tr>
</table>
<form name="frmForumDel" method="post" action="email_notify_remove.asp<% If strMode = "A" Then Response.Write("?PF=" & lngEmailUserID & "&M=A") %>" OnSubmit="return confirm('<% = strTxtAreYouWantToUnsubscribe & " " & strTxtForums %>')">
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
      <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" width="97%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"> <% = strTxtForums & " " & strTxtThatYouHaveSubscribedTo %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="3%" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><input type="checkbox" name="chkAll" onClick="checkAllForum();"></td>
       </tr><%
	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "Forum.Forum_name, " & strDbTable & "EmailNotify.Forum_ID, " & strDbTable & "EmailNotify.Watch_ID "
strSQL = strSQL & "FROM " & strDbTable & "Forum, " & strDbTable & "EmailNotify "
strSQL = strSQL & "WHERE " & strDbTable & "Forum.Forum_ID=" & strDbTable & "EmailNotify.Forum_ID AND " & strDbTable & "EmailNotify.Author_ID=" & lngEmailUserID & " "
strSQL = strSQL & "ORDER BY " & strDbTable & "Forum.Forum_Order;"


'Query the database
rsCommon.Open strSQL, adoCon
    
'Check there are email subscriptions to show
If rsCommon.EOF Then

	'If there are no pm messages to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtYouHaveNoSubToEmailNotify & "<input type=""hidden"" name=""chkDelete"" value=""-1""></td>"

'Else there the are email subs so show em
Else 	
	'Loop round to read in all the email notifys
	Do WHILE NOT  rsCommon.EOF 
	
		'Get the row number
		intForumColourNumber = intForumColourNumber + 1
	
	%><tr> 
          <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="97%" class="text"><a href="forum_topics.asp?FID=<% = rsCommon("Forum_ID") %>"><% = rsCommon("Forum_name") %></a></td>
          <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="3%" class="text" align="center"><input type="checkbox" name="chkDelete" value="<% = rsCommon("Watch_ID") %>"></td>
       </tr><%
		
		'Move to the next recordset
		rsCommon.MoveNext
	Loop
End If

'clear up
rsCommon.Close
%>
      </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="2" align="center"><tr>
<td width="6%" align="right"><input type="submit" name="Submit" value="<% = strTxtUnsusbribe %>"></td></tr>
</table>
</form>
<form name="frmTopicDel" method="post" action="email_notify_remove.asp<% If strMode = "A" Then Response.Write("?PF=" & lngEmailUserID & "&M=A") %>" OnSubmit="return confirm('<% = strTxtAreYouWantToUnsubscribe & " " & strTxtTopics %>')">
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
  <tr> 
   <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
     <tr> 
      <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
        <tr> 
         <td bgcolor="<% = strTableTitleColour %>" width="67%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtTopics & " " & strTxtThatYouHaveSubscribedTo %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="30%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLastPost %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="3%" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><input type="checkbox" name="chkAll" onClick="checkAllTopic();"></td>
        </tr><%
	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Last_entry_date, " & strDbTable & "EmailNotify.Topic_ID, " & strDbTable & "EmailNotify.Watch_ID "
strSQL = strSQL & "FROM " & strDbTable & "Topic, " & strDbTable & "EmailNotify "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID=" & strDbTable & "EmailNotify.Topic_ID AND " & strDbTable & "EmailNotify.Author_ID=" & lngEmailUserID & " "
strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Last_entry_date  DESC;"


'Query the database
rsCommon.Open strSQL, adoCon
    
'Check there are email subscriptions to show
If rsCommon.EOF Then

	'If there are no pm messages to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtYouHaveNoSubToEmailNotify & "<input type=""hidden"" name=""chkDelete"" value=""-1""></td>"

'Else there the are email subs so show em
Else 	
	'Loop round to read in all the email notifys
	Do WHILE NOT  rsCommon.EOF 
	
		'Get the date of the last entry
		dtmLastEntryDate = CDate(rsCommon("Last_entry_date"))
	
		'Get the row number
		intForumColourNumber = intForumColourNumber + 1
	%>
        <tr> 
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="67%" class="text"><a href="forum_posts.asp?TID=<% = rsCommon("Topic_ID") %>"><% = rsCommon("Subject") %></a></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="30%" class="text" nowrap="nowrap"><% Response.Write(DateFormat(dtmLastEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmLastEntryDate, saryDateTimeData)) %></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="3%" class="text" align="center"><input type="checkbox" name="chkDelete" value="<% = rsCommon("Watch_ID") %>"></td>
        </tr><%
		
		'Move to the next recordset
		rsCommon.MoveNext
	Loop
End If

'clear up
rsCommon.Close
%>
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="2" align="center">
  <tr>
   <td width="6%" align="right"><input type="submit" name="Submit" value="<% = strTxtUnsusbribe %>"></td>
  </tr>
 </table>
</form><%

'If this is not in admin mode then see if the user wants email notification of a forum
If strMode <> "A" Then 
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr><form method="post" name="frmBuddy" action="email_notify.asp?M=SP" onSubmit="return CheckForm();">
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
         <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSubscribeToForum %></td>
    </tr>
    <tr> 
     <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
            <td class="text"> <% = strTxtSelectForumToSubscribeTo %> <%
            
Response.Write(vbCrLf & "	 <select name=""FID"">")
Response.Write(vbCrLf & "           <option value="""" selected>-- " & strTxtSelectForum & " --</option>")


'Create a recordset to hold the forum name and id number
Set rsForumSelect = Server.CreateObject("ADODB.Recordset")


'Read in the category name from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "CategoryAll"
Else
	strSQL = "SELECT " & strDbTable & "Category.Cat_name, " & strDbTable & "Category.Cat_ID FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"
End If

'Query the database
rsCommon.Open strSQL, adoCon




'Loop through all the categories in the database
Do while NOT rsCommon.EOF 

	'Read in the deatils for the category
	strCatName = rsCommon("Cat_name")
	intCatID = Cint(rsCommon("Cat_ID"))	
	
	'Display a link in the link list to the forum
	Response.Write vbCrLf & "		<option value="""">" & strCatName & "</option>"

	'Read in the forum name from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereCatIs @intCatID = " & intCatID
	Else
		strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = " & intCatID & " ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"
	End If
	
	'Query the database
	rsForumSelect.Open strSQL, adoCon
	
	'Loop through all the froum in the database
	Do while NOT rsForumSelect.EOF 
	
		'Read in the forum details from the recordset
		strSelectForumName = rsForumSelect("Forum_name")
		intSelectForumID = CLng(rsForumSelect("Forum_ID"))
		intReadPermission = CInt(rsForumSelect("Read"))

		'Call the function to check the users security level within this forum
		Call forumPermisisons(intSelectForumID, intGroupID, intReadPermission, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		'If the user can view the forum then display it in the select box
		If blnRead OR blnAdmin OR blnModerator Then
			'Display a link in the link list to the forum
			Response.Write vbCrLf & "		<option value=""" & intSelectForumID & """>&nbsp;&nbsp;-&nbsp;" & strSelectForumName & "</option>"		
		End If
				
		'Move to the next record in the recordset
		rsForumSelect.MoveNext
	Loop
	
	'Close the forum recordset so another can be opened
	rsForumSelect.Close
	
	'Move to the next record in the recordset
	rsCommon.MoveNext
Loop

'Reset Server Objects
rsCommon.Close
Set rsForumSelect = Nothing


Response.Write(vbCrLf & "	</select>")

%>
	<input type="submit" name="Submit" value="Submit"></tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</td>
</form></tr>
</table><%
End If
%>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
  <tr><form>
   <td><!-- #include file="includes/forum_jump_inc.asp" --></td>
   </form>
  </tr>
 </table>
<div align="center"><br /><%

'Clear server objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"


Response.Write("</div>")

%>
<!-- #include file="includes/footer.asp" -->