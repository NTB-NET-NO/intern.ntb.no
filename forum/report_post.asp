<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim lngPostID		'Holds the post ID number
Dim lngTopicID		'Holds the topic ID number
Dim intTopicPageNum	'Holds the topic page number
Dim intForumID		'Holds the forum ID
Dim strPostedMessage	'Holds the posted message



Dim lngToUserID		'Holds the user ID of who the email is to
Dim strToUser		'Holds the user name of the person the email is to
Dim blnShowEmail	'set to true if the user allws emailing to them
Dim strToEmail		'Holds the email address of who the email is to
Dim strFromEmail	'Holds the email address of who the email is from
Dim blnEmailSent	'Set to true if the email has been sent
Dim strEmailBody
Dim strSubject
Dim strMessagePoster


'Read in the details
lngPostID = CLng(Request("PID"))
lngTopicID = CLng(Request("TID"))
intTopicPageNum = CInt(Request("TPN"))
intForumID = CInt(Request("FID"))


'If there is no recopinet for the email then send em to homepage
If lngPostID = 0 OR lngTopicID = 0 OR blnEmail = False Then
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	Response.Redirect("default.asp")
End If


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If



'Initlise variables
blnEmailSent = False


'If this is a post back send the mail
If Request.Form("postBack") Then


	'Check the session ID to stop spammers using the email form
	Call checkSessionID(Request.Form("sessionID"))



	'Get the post to send with the email
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Thread.Message "
	strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Thread  "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Thread.Thread_ID=" & lngPostID & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Read in the message to be sent with the report
	If NOT rsCommon.EOF Then
		strPostedMessage = rsCommon("Message")
		strMessagePoster = rsCommon("Username")

		'Change	the path to the	emotion	symbols	to include the path to the images
		strPostedMessage = Replace(strPostedMessage, "src=""smileys/smiley", "src=""" & strForumPath & "/smileys/smiley", 1, -1, 1)
	End If

	'Clean up
	rsCommon.Close



	'Inititlaise the subject of the e-mail
	strSubject = strTxtIssueWithPostOn & " " & strMainForumName

	'Initilalse the body of the email message
	strEmailBody = strTxtHi & ","
	strEmailBody = strEmailBody & "<br /><br />" & strTxtTheFollowingReportSubmittedBy & " " & decodeString(strLoggedInUsername) & ", " & strTxtOn & " " & strMainForumName & " " & strTxtWhoHasTheFollowingIssue & " : -"
	strEmailBody = strEmailBody & "<br /><br /><hr />"
	strEmailBody = strEmailBody & "<br />" & Replace(Request.Form("report"), vbCrLf, "<br />",	1, -1, 1) & "<br /><br />"
	strEmailBody = strEmailBody & "<hr />"
	strEmailBody = strEmailBody & "<br />" & strTxtToViewThePostClickTheLink & " : -"
	strEmailBody = strEmailBody & "<br /><a href=""" & strForumPath & "/forum_posts.asp?TID=" & lngTopicID & "&TPN=" & intTopicPageNum & """>" & strForumPath & "/forum_posts.asp?TID=" & lngTopicID & "&TPN=" & intTopicPageNum & "</a>"
	strEmailBody = strEmailBody & "<br /><br /><hr /><br /><b>" & strTxtPostedBy & ":</b> " & strMessagePoster & "<br /><br />"
	strEmailBody = strEmailBody & strPostedMessage



	'Get the email address of the boards admins to send the email to
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_email "
	strSQL = strSQL & "FROM " & strDbTable & "Author  "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Group_ID=1 AND " & strDbTable & "Author.Author_email <> '';"


	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Send an email to the forum email address if there are no email addresses of admins in the database
	If rsCommon.EOF Then blnEmailSent = SendMail(strEmailBody, strTxtForumAdministrator, strForumEmailAddress, strLoggedInUsername, strForumEmailAddress, strSubject, strMailComponent, true)
	
	'If there are email addresses returned send email to the forum admins
	Do while not rsCommon.EOF

		'Send the e-mail using the Send Mail function created on the send_mail_function.inc file
		blnEmailSent = SendMail(strEmailBody, rsCommon("Username"), rsCommon("Author_email"), strLoggedInUsername, strForumEmailAddress, strSubject, strMailComponent, true)

		'Move to next record
		rsCommon.MoveNext
	Loop

	'Clean up
	rsCommon.Close




	'Get the email address of the moderators to send the email to
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_email "
	strSQL = strSQL & "FROM " & strDbTable & "Permissions, " & strDbTable & "Author  "
	If strDatabaseType = "SQLServer" Then
		strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & strDbTable & "Author.Group_ID AND (" & strDbTable & "Permissions.Forum_ID=" & intForumID & " AND " & strDbTable & "Permissions.Moderate=1) AND " & strDbTable & "Author.Author_email <> '';"
	Else
		strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & strDbTable & "Author.Group_ID AND (" & strDbTable & "Permissions.Forum_ID=" & intForumID & " AND " & strDbTable & "Permissions.Moderate=True) AND " & strDbTable & "Author.Author_email <> '';"
	End If

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Send an email to the moderators
	Do while not rsCommon.EOF

		'Send the e-mail using the Send Mail function created on the send_mail_function.inc file
		blnEmailSent = SendMail(strEmailBody, rsCommon("Username"), rsCommon("Author_email"), strLoggedInUsername, strForumEmailAddress, strSubject, strMailComponent, true)

		'Move to next record
		rsCommon.MoveNext
	Loop

	'Clean up
	rsCommon.Close
End If
%>
<html>
<head>
<title>Report Post</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtReportPost %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtReportPost %><br /></td>
  </tr>
</table>
 <%
'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'If the users account is suspended then let them know
If blnActiveMember = False Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtForumMemberSuspended & "</span><br /><br /><br /><br /><br /></div>"

'Else if the user is not logged in so let them know to login
ElseIf intGroupID = 2 Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtMustBeRegistered & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "register.gif""  alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle""></a>&nbsp;&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "login.gif""  alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle""></a><br /><br /><br /><br /></div>"

'If the email has been sent display the appropriate message
ElseIf blnEmailSent Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtYourReportEmailHasBeenSent & "</span><br /><br /><a href=""default.asp"" target=""_self"">" & strTxtReturnToDiscussionForum & "</a><br /><br /><br /><br /><br /></div>"

'Else show the form so the person can be emailed
Else

%><form method="post" name="frmReport" action="report_post.asp" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="600" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" height="230" align="center">
  <tr>
   <td height="66" width="967">
    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" height="201">
     <tr>
      <td>
       <table width="600" border="0" align="center" cellpadding="4" cellspacing="0">
        <tr>
         <td colspan="2" class="text"><% = strTxtPleaseStateProblemWithPost %><br /><br /></td>
        </tr>
         <td valign="top" align="right" width="20%" class="text"><% = strTxtProblemWithPost %>*:</td>
         <td width="65%" valign="top"><textarea name="report" cols="45" rows="9"></textarea>
         </td>
        </tr>
         <td><input name="PID" type="hidden" id="PID" value="<% = lngPostID %>">
             <input name="FID" type="hidden" id="FID" value="<% = intForumID %>">
             <input name="TID" type="hidden" id="TID" value="<% = lngTopicID %>">
             <input name="TPN" type="hidden" id="TPN" value="<% = intTopicPageNum %>">
             <input name="postBack" id="postBack" type="hidden" value="true">&nbsp;</td>
        <td width="65%" align="left">
           <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
	   <input type="submit" name="Submit" value="<% = strTxtSendReport %>">
           <input type="reset" name="Reset" value="<% = strTxtResetForm %>">
        </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</form><%

End If
%>
<br />
<div align="center">
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
<!-- #include file="includes/footer.asp" -->