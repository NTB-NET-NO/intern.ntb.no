<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

Response.Buffer = True 


'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Dimension variables
Dim intForumID
Dim strReturnPage
Dim blnLoggedInOK
Dim strReferer			'Holds the page to return to


'Initilise variables
blnLoggedInOK = True

'read in the forum ID number
If isNumeric(Request.QueryString("FID")) Then
	intForumID = CInt(Request.QueryString("FID"))
Else
	intForumID = 0
End If


'Logged in cookie test
If strLoggedInUserCode = "" Then blnLoggedInOK = False
If InStr(1, strLoggedInUserCode, "LOGGED-OFF", 1) Then blnLoggedInOK = False



'Get the forum page to return to
If blnLoggedInOK = False Then
	strReturnPage = "login_user.asp?" & removeAllTags(Request.QueryString)
ElseIf Request.QueryString("M") = "Unsubscribe" Then
	strReturnPage = "email_notify.asp?" & removeAllTags(Request.QueryString)
'Redirect the user back to the forum they have just come from
ElseIf intForumID > 0 Then
	strReturnPage = "forum_topics.asp?" & removeAllTags(Request.QueryString)
'Return to forum homepage
Else
	strReturnPage = "default.asp"
End If

%>  
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Login User</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<%
If blnLoggedInOK AND blnActiveMember = True Then Response.Write("<meta http-equiv=""refresh"" content=""3;URL=" & strReturnPage & """>")	
%>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtLoginUser %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtLoginUser %><br /></td>
  </tr>
</table>
<div align="center">
  <br /><br />
 <span class="lgText"><% 
'Display heading text
If blnLoggedInOK = False Then Response.Write(strTxtUn) 
 
Response.Write(strTxtSuccessfulLogin) %></span><br />
  <br /><%
  
  
'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing



'If the account has not been activated have a link to resend activation email	
If InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) = False AND blnEmailActivation AND blnLoggedInUserEmail AND blnActiveMember = False Then

	Response.Write("<span class=""lgText"">" & strTxtForumMembershipNotAct & "</span><br /><br /><span class=""text"">" & strTxtToActivateYourForumMem & "<br /><br /><a href=""JavaScript:openWin('resend_email_activation.asp','actMail','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=475,height=200')"">" & strTxtResendActivationEmail & "</a><br /><br /><br /><a href=""" & strReturnPage & """ target=""_self"">" & strTxtReturnToDiscussionForum & "</a></span>")

'If the member is suspened then tell them so
ElseIf InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) AND blnActiveMember = False Then

	Response.Write("<span class=""text"">" & strTxtForumMemberSuspended & "<br /><br /><a href=""" & strReturnPage & """ target=""_self"">" & strTxtReturnToDiscussionForum & "</a></span>")

'If this is a successful login then display some text
ElseIf blnLoggedInOK = True Then
	
	Response.Write("<span class=""text"">" & strTxtSuccessfulLoginReturnToForum & "<br /><br /><a href=""" & strReturnPage & """ target=""_self"">" & strTxtReturnToDiscussionForum & "</a></span>")


'Display that the login was not successful
Else
	Response.Write("<span class=""text"">" & strTxtUnSuccessfulLoginText & "</span>")
	Response.Write vbCrLf & "<br /><br /><a href=""login_user.asp?" &  removeAllTags(Request.QueryString) & """ target=""_self"">" & strTxtUnSuccessfulLoginReTry & "</a>"
End If

%>
<br /><br /><br /><br /><br /><br />
</div>
<div align="center">
<% 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div> 
<!-- #include file="includes/footer.asp" -->