<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True


'Dimension variables
Dim strAuthorEmail	'Holds the users e-mail address
Dim strFormMessage	'Holds the message in the form
Dim strEmailBody	'Holds the body of the e-mail
Dim blnSentEmail	'Set to true when the e-mail is sent
Dim strSubject		'Holds the subject of the e-mail
Dim strRealName		'Holds the authors real name
Dim intForumID

'Initialise variables
blnSentEmail = False


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If


'If the user has not logged in then  or the page has not been passed with a topic id the redirect to the forum start page
If Request.QueryString("TID") = "" OR blnEmail = False OR intGroupID = 2 Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect "default.asp"
End If


'Initilise the message in the form
strFormMessage = strTxtEmailFriendMessage & " " & strMainForumName & " " & strTxtAt & ": -"
strFormMessage = strFormMessage & vbCrLf & vbCrLf & strForumPath & "/forum_posts.asp?TID=" & CLng(Request.QueryString("TID"))
strFormMessage = strFormMessage & vbCrLf & vbCrLf & strTxtRegards & "," & vbCrLf & strLoggedInUsername & vbCrLf


'Read in the users e-mail address

'Initalise the strSQL variable with an SQL statement to query the database get the thread details
strSQL = "SELECT " & strDbTable & "Author.Real_name, " & strDbTable & "Author.Author_email "
strSQL = strSQL & "FROM " & strDbTable & "Author "
strSQL = strSQL & "WHERE (((" & strDbTable & "Author.Author_ID)=" & lngLoggedInUserID & "));"

'Query the database
rsCommon.Open strSQL, adoCon

'If there is an e-mail address for the user then read it in
If NOT rsCommon.EOF Then
	'Read in authors detals from the database
	strAuthorEmail = rsCommon("Author_email")
	strRealName = rsCommon("Real_name")
End If


'If the form has been filled in then send the form
If NOT Request.Form("ToName") = "" AND NOT Request.Form("ToEmail") = "" AND NOT Request.Form("FromName") = "" AND NOT Request.Form("FromEmail") = "" AND NOT Request.Form("message") = "" Then

	'Check the session ID to stop spammers using the email form
	Call checkSessionID(Request.Form("sessionID"))

	'Initilalse the body of the email message
	strEmailBody = strTxtHi & " " & Request.Form("ToName") & ","
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtTheFollowingEmailHasBeenSentToYouBy & " " & Request.Form("FromName") & " " & strTxtFrom & " " & strMainForumName & "."
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtIfThisMessageIsAbusive & ": - "
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strForumEmailAddress
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtIncludeThisEmailAndTheFollowing & ": - " & "BBS=" & strMainForumName & ";ID=" & lngLoggedInUserID & ";USR= " & strLoggedInUsername & ";"
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtReplyToEmailSetTo & " " & Request.Form("FromName") & "."
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtMessageSent & ": -"
	strEmailBody = strEmailBody & vbCrLf & "---------------------------------------------------------------------------------------"
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtHi & " " & Request.Form("ToName")
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & Request.Form("message")

	'Inititlaise the subject of the e-mail
	strSubject = strTxtInterestingForumPostOn & " " & strWebsiteName

	'Send the e-mail using the Send Mail function created on the send_mail_function.inc file
	blnSentEmail = SendMail(strEmailBody, Request.Form("ToName"), Request.Form("ToEmail"), Request.Form("FromName"), Request.Form("FromEmail"), strSubject, strMailComponent, false)
End If

'Reset server objects
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Email Topic To a Friend</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm() {

	var errorMsg = "";

	//Check for a Friends Name
	if (document.frmEmailTopic.ToName.value == ""){
		errorMsg += "\n\t<% = strTxtErrorFrinedsName %>";
	}

	//Check that the friends e-mail and it is valid address is valid
	if ((document.frmEmailTopic.ToEmail.value=="") || (document.frmEmailTopic.ToEmail.value.length > 0 && (document.frmEmailTopic.ToEmail.value.indexOf("@",0) == - 1 || document.frmEmailTopic.ToEmail.value.indexOf(".",0) == - 1))) {
		errorMsg += "\n\t<% = strTxtErrorFriendsEmail %>";
	}

	//Check for a Users Name
	if (document.frmEmailTopic.FromName.value==""){
		errorMsg += "\n\t<% = strTxtErrorYourName %>";
	}

	//Check for the users e-mail address and it is valid
	if ((document.frmEmailTopic.FromEmail.value=="") || (document.frmEmailTopic.FromEmail.value.length > 0 && (document.frmEmailTopic.FromEmail.value.indexOf("@",0) == - 1 || document.frmEmailTopic.FromEmail.value.indexOf(".",0) == - 1))) {
		errorMsg += "\n\t<% = strTxtErrorYourEmail %>";
	}

	//Check for a Message
	if (document.frmEmailTopic.message.value==""){
		errorMsg += "\n\t<% = strTxtErrorEmailMessage %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
// -->
</script>
<!--#include file="includes/skin_file.asp" -->
</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
 <tr>
  <td align="center"><span class="heading"><% = strTxtEmailTopicToFriend %></span></td>
 </tr>
</table><%

'If the email has been sent then display a message saying
If blnSentEmail = True Then
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
 <tr>
  <td align="center" height="79" class="text"><% = strTxtFriendSentEmail %></td>
 </tr>
</table>
<%
'Else the e-mail has not been sent so display the form
Else
%><br />
<form name="frmEmailTopic" method="post" action="email_topic.asp?FID=<% = CInt(Request.QueryString("FID")) %>&TID=<% = CLng(Request.QueryString("TID")) %>" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="350" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td height="174">
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="350">
     <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>">
      <td colspan="2" bgcolor="<% = strTableTitleColour %>" background="<% = strTableTitleBgImage %>" class="text">*<% = strTxtRequiredFields %></td>
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" width="115" class="text" background="<% = strTableBgImage %>"><% = strTxtUsername %></td>
      <td align="left" width="234" background="<% = strTableBgImage %>" class="text"><% = strLoggedInUsername %></td>
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" width="115" class="text" background="<% = strTableBgImage %>"><% = strTxtYourName %>*</td>
      <td align="left" width="234" background="<% = strTableBgImage %>">
       <input type="text" name="FromName" size="20" maxlength="20" value="<% = strRealName %>" onFocus="FromName.value = ''" />
      </td>
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" width="115" class="text" background="<% = strTableBgImage %>"><% = strTxtYourEmail %>*</td>
      <td align="left" width="234" background="<% = strTableBgImage %>">
       <input type="text" name="FromEmail" size="20" maxlength="50" value="<% = strAuthorEmail %>" onFocus="FromEmail.value = ''" />
      </td>
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" width="115" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtFriendsName %>*</td>
      <td align="left" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" width="234" nowrap>
       <input type="text" name="ToName" size="20" maxlength="20" />
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" width="115" class="text" background="<% = strTableBgImage %>"><% = strTxtFriendsEmail %>*</td>
      <td align="left" width="234" background="<% = strTableBgImage %>">
       <input type="text" name="ToEmail" size="20" maxlength="50">
      </td>
     </tr>
     <tr  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="left" colspan="2" background="<% = strTableBgImage %>"><span class="text"><% = strTxtMessage %>:</span><br />
       <textarea name="message" cols="40" rows="4" wrap="OFF"><% = strFormMessage %></textarea>
      </td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" align="center">
      <td valign="top" colspan="2" background="<% = strTableBgImage %>">
       <p>
        <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
        <input type="submit" name="Submit" value="<% = strTxtSendEmail %>" />
        <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
       </p>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
</form><%

End If
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
 <tr>
  <td align="center" height="34"><a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a>
  <br /><br />
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
  </td>
 </tr>
</table>
</body>
</html>