<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"

'Dimension variables
Dim strUsername			'Holds the users username
Dim strHomepage			'Holds the users homepage if they have one
Dim strEmail			'Holds the users e-mail address
Dim blnShowEmail		'Boolean set to true if the user wishes there e-mail address to be shown
Dim lngUserID			'Holds the new users ID number
Dim lngNumOfPosts		'Holds the number of posts the user has made
Dim intMemberGroupID		'Holds the users interger group ID
Dim strMemberGroupName		'Holds the umembers group name
Dim intRankStars		'holds the number of rank stars the user holds
Dim dtmRegisteredDate		'Holds the date the usre registered
Dim intTotalNumMembersPages	'Holds the total number of pages
Dim intTotalNumMembers		'Holds the total number of forum members
Dim intRecordPositionPageNum	'Holds the page number we are on
Dim intRecordLoopCounter	'Recordset loop counter
Dim dtmLastPostDate		'Holds the date of the users las post
Dim intLinkPageNum		'Holds the page number to link to
Dim strSearchCriteria		'Holds the search critiria
Dim strSortBy			'Holds the way the records are sorted
Dim intSortSelectField		'Holds the sort selection to be shown in the sort list box
Dim intForumID			'Holds the forum ID if within a forum
Dim intGetGroupID		'Holds the group ID
Dim strRankCustomStars		'Holds custom stars for the user group

'Initalise variables
blnShowEmail = False
intGetGroupID = CInt(Request.QueryString("GID"))


'If this is the first time the page is displayed then the members record position is set to page 1
If Request.QueryString("MemPN") = "" Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the members page record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("MemPN"))
End If


'Get the search critiria for the members to display
If NOT Request.QueryString("SF") = "" Then
	strSearchCriteria = Trim(Mid(Request.QueryString("SF"), 1, 15))
End If


'Take out parts of the username that are not permitted
strSearchCriteria = disallowedMemberNames(strSearchCriteria)

'Get rid of milisous code
strSearchCriteria = formatSQLInput(strSearchCriteria)

'Get the sort critiria
Select Case Request.QueryString("SO")
	Case "PT"
		strSortBy = "" & strDbTable & "Author.No_of_posts DESC"
		intSortSelectField = 1
	Case "LU"
		strSortBy = "" & strDbTable & "Author.Join_date DESC"
		intSortSelectField = 2
	Case "OU"
		strSortBy = "" & strDbTable & "Author.Join_date ASC"
		intSortSelectField = 3
	Case "GP"
		strSortBy = "" & strDbTable & "Group.Name ASC"
		intSortSelectField = 4
	Case "SR"
		strSortBy = "" & strDbTable & "Group.Stars ASC"
		intSortSelectField = 5
	Case Else
		strSortBy = "" & strDbTable & "Author.Username ASC"
		intSortSelectField = 0
End Select


%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title><% = strMainForumName %> Members</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Check for a somthing to search for
	if (document.frmMemberSearch.SF.value==""){

		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		alert(msg + "\n\t<% = strTxtErrorMemberSerach %>\n\n");
		document.frmMemberSearch.SF.focus();
		return false;
	}

	return true;
}

//Function to choose how the members list is sorted
function MembersSort(SelectSort){

   	if (SelectSort != "") self.location.href = "members.asp?SF=<% = Server.URLEncode(Request.QueryString("SF")) %>&GID=<% = intGetGroupID %>&SO=" + SelectSort.options[SelectSort.selectedIndex].value;
	return true;
}

//Function to move to another page of members
function MembersPage(SelectPage){

   	if (SelectPage != -1) self.location.href = "members.asp?SF=<% = Server.URLEncode(Request.QueryString("SF")) %>&GID=<% = intGetGroupID %>&SO=<% = Trim(Mid(Request.QueryString("SO"),1,2)) %>&MemPN=" + SelectPage.options[SelectPage.selectedIndex].value;
	return true;
}
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtForumMembers %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtForumMembers %></td>
 </tr>
</table>
    <br />
    <form name="frmMemberSearch" method="get" action="members.asp" onSubmit="return CheckForm();">
     <table width="490" border="0" cellspacing="0" cellpadding="1" height="24" align="center" bgcolor="<% = strTableBorderColour %>">
      <tr>
       <td align="center" height="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="<% = strTableBorderColour %>">
         <tr>
          <td align="center" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" width="100%" height="20"><span class="text"><% = strTxtMemberSearch %>:</span>
           <input type="text" name="SF" size="15" maxlength="15" value="<% = Server.HTMLEncode(Request.QueryString("SF")) %>">
           <input type="submit" name="Submit" value="<% = strTxtSearch %>">
          </td>
         </tr>
         <tr>
          <td align="center" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" width="100%"><a href="members.asp" target="_self"><% = strTxtAll %></a> <a href="members.asp?SF=A" target="_self">A</a> <a href="members.asp?SF=B" target="_self">B</a> <a href="members.asp?SF=C" target="_self">C</a>
           <a href="members.asp?SF=D" target="_self">D</a> <a href="members.asp?SF=E" target="_self">E</a> <a href="members.asp?SF=F" target="_self">F</a>
           <a href="members.asp?SF=G" target="_self">G</a> <a href="members.asp?SF=H" target="_self">H</a> <a href="members.asp?SF=I" target="_self">I</a>
           <a href="members.asp?SF=J" target="_self">J</a> <a href="members.asp?SF=K" target="_self">K</a> <a href="members.asp?SF=L" target="_self">L</a>
           <a href="members.asp?SF=M" target="_self">M</a> <a href="members.asp?SF=N" target="_self">N</a> <a href="members.asp?SF=O" target="_self">O</a>
           <a href="members.asp?SF=P" target="_self">P</a> <a href="members.asp?SF=Q" target="_self">Q</a> <a href="members.asp?SF=R" target="_self">R</a>
           <a href="members.asp?SF=S" target="_self">S</a> <a href="members.asp?SF=T" target="_self">T</a> <a href="members.asp?SF=U" target="_self">U</a>
           <a href="members.asp?SF=V" target="_self">V</a> <a href="members.asp?SF=W" target="_self">W</a> <a href="members.asp?SF=X" target="_self">X</a>
           <a href="members.asp?SF=Y" target="_self">Y</a> <a href="members.asp?SF=Z" target="_self">Z</a></td>
         </tr>
        </table>
       </td>
      </tr>
     </table>
    </form>
   </div>
   <div align="center"><%
'If the users account is suspended then let them know
If blnActiveMember = False Then
		
	Response.Write (vbCrLf & "<span class=""text"">")
	
	'If mem suspended display message
	If  InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) Then
		Response.Write(strTxtForumMemberSuspended)
	'Else account not yet active
	Else
		Response.Write("<span class=""lgText"">" & strTxtInsufficientPermison & "<br /><br />" & strTxtForumMembershipNotAct & "</span><br /><br />" & strTxtToActivateYourForumMem)
	End If
	'If email is on then place a re-send activation email link
	If InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) = False AND blnEmailActivation AND blnLoggedInUserEmail Then Response.Write("<br /><br /><a href=""JavaScript:openWin('resend_email_activation.asp','actMail','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=475,height=200')"">" & strTxtResendActivationEmail & "</a>")
	
	Response.Write("</span><br /><br /><br /><br />")
	
'If the user has not logged in dispaly an error message
ElseIf intGroupID = 2 Then

	Response.Write (vbCrLf & "<span class=""bold"">" & strTxtMustBeRegistered & "</span><br /><br />")
	Response.Write (vbCrLf & "<a href=""registration_rules.asp"" target=""_self""><img src=""" & strImagePath & "register.gif""  alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle""></a>&nbsp;&nbsp;<a href=""login_user.asp"" target=""_self""><img src=""" & strImagePath & "login.gif""  alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle""></a><br /><br /><br /><br /><br /><br />")

'If the user has logged in then read in the members from the database and dispaly them
Else

	'If this is to show a group the query the database for the members of the group
	If intGetGroupID <> 0 Then
		'Initalise the strSQL variable with an SQL statement to query the database
		strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username, " & strDbTable & "Author.Group_ID, " & strDbTable & "Author.Homepage, " & strDbTable & "Author.No_of_posts, " & strDbTable & "Author.Join_date, " & strDbTable & "Author.Active, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
		strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Group "
		strSQL = strSQL & "WHERE " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Author.Group_ID=" & intGetGroupID & " "
		strSQL = strSQL & "ORDER BY " & strSortBy & ";"

	'Else get all the members from the database
	Else
		'Initalise the strSQL variable with an SQL statement to query the database
		strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username, " & strDbTable & "Author.Group_ID, " & strDbTable & "Author.Homepage, " & strDbTable & "Author.No_of_posts, " & strDbTable & "Author.Join_date, " & strDbTable & "Author.Active, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
		strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Group "
		strSQL = strSQL & "WHERE " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Author.Username Like '" & strSearchCriteria & "%' "
		strSQL = strSQL & "ORDER BY " & strSortBy & ";"
	End If

	'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
	rsCommon.CursorType = 1

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Set the number of records to display on each page
	rsCommon.PageSize = 25
	
	


	'If there are no memebers to display then show an error message
	If rsCommon.EOF Then
		Response.Write "<span class=""bold"">" & strTxtSorryYourSearchFoundNoMembers & "</span><br /><br /><br />"

	'If there is a recordset returned by the query then read in the details
	Else
		'Set the page number to display records for
		rsCommon.AbsolutePage = intRecordPositionPageNum


		'Count the number of members there are in the database by returning the number of records in the recordset
		intTotalNumMembers = rsCommon.RecordCount

		'Count the number of pages there are in the database calculated by the PageSize attribute set above
		intTotalNumMembersPages = rsCommon.PageCount


		'Display the HTML for the total number of pages and total number of records in the database for the users
		Response.Write vbCrLf & "	<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">"
		Response.Write vbCrLf & " 	  <tr>"
		Response.Write vbCrLf & " 	    <td align=""center"" class=""bold"">"

		'If we are showing all the forum memebers then display how many members there are
		If Request.QueryString("SF") = "" Then
			Response.Write vbCrLf & "	      " & strTxtThereAre & " " & intTotalNumMembers & " " & strTxtForumMembersOn & " " & intTotalNumMembersPages & " " & strTxtPageYouAerOnPage & " " & intRecordPositionPageNum
		'Else display how many results were fround from the search
		Else
			Response.Write vbCrLf & "	      " & strTxtYourSearchMembersFound & " " & intTotalNumMembers & " " & strTxtMatches
		End If

		Response.Write vbCrLf & "	    </td>"
		Response.Write vbCrLf & "	  </tr>"
		Response.Write vbCrLf & "	</table>"
		Response.Write vbCrLf & "	<br />"

%>  <form>
     <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center" height="32">
      <tr>
       <td align="right" height="28" valign="top"><span class="text"><% = strTxtSortResultsBy %></span>
        <select name="SelectSort" onChange="MembersSort(this)">
         <option value="UN" <% If intSortSelectField = 0 Then Response.Write "selected" %>><% = strTxtUsernameAlphabetically %></option>
         <option value="PT" <% If intSortSelectField = 1 Then Response.Write "selected" %>><% = strTxtPosts %></option>
         <option value="LU" <% If intSortSelectField = 2 Then Response.Write "selected" %>><% = strTxtNewForumMembersFirst %></option>
         <option value="OU" <% If intSortSelectField = 3 Then Response.Write "selected" %>><% = strTxtOldForumMembersFirst %></option>
         <option value="GP" <% If intSortSelectField = 4 Then Response.Write "selected" %>><% = strTxtType %></option>
         <option value="SR" <% If intSortSelectField = 5 Then Response.Write "selected" %>><% = strTxtNoOfStars %></option>
        </select>
       </td>
      </tr>
     </table>
     <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
    <tr>
     <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
       <tr>
        <td bgcolor="<% = strTableBgColour %>">
         <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
         <tr>
          <td bgcolor="<% = strTableTitleColour %>" height="2" width="116" class="tHeading" background="<% = strTableTitleBgImage %>"><a href="http://www.webwizguide.info"></a><% = strTxtUsername %></td>
          <td bgcolor="<% = strTableTitleColour %>" width="113" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtType %></td>
          <td bgcolor="<% = strTableTitleColour %>" width="116" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtRegistered %></td>
          <td bgcolor="<% = strTableTitleColour %>" width="41" height="2" class="tHeading" background="<% = strTableTitleBgImage %>" align="center"><% = strTxtPosts %></td>
          <td bgcolor="<% = strTableTitleColour %>" width="59" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtHomepage %></td>
          <% If blnPrivateMessages = True Then %>
          <td bgcolor="<% = strTableTitleColour %>" width="64" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtAddBuddy %></td>
          <% End If %>
          <td bgcolor="<% = strTableTitleColour %>" width="57" height="2" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSearch %></td>
         </tr><%

		'For....Next Loop to loop through the recorset to display the forum members
		For intRecordLoopCounter = 1 to 25

			'If there are no member's records left to display then exit loop
			If rsCommon.EOF Then Exit For

			'Initialise varibles
			dtmLastPostDate = ""

			'Read in the profile from the recordset
			lngUserID = CLng(rsCommon("Author_ID"))
			strUsername = rsCommon("Username")
			'strEmail = rsCommon("Author_email")
			'blnShowEmail = CBool(rsCommon("Show_email"))
			strHomepage = rsCommon("Homepage")
			lngNumOfPosts = CLng(rsCommon("No_of_posts"))
			dtmRegisteredDate = CDate(rsCommon("Join_date"))
			intMemberGroupID = CInt(rsCommon("Group_ID"))
			strMemberGroupName = rsCommon("Name")
			intRankStars = CInt(rsCommon("Stars"))
			strRankCustomStars = rsCommon("Custom_stars")
			

			'If the users account is not active make there account level guest
			If CBool(rsCommon("Active")) = False Then intMemberGroupID = 0

			'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
			%>
         <tr>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="116" height="24"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngUserID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"><% = strUsername %></a></td>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="113" height="24" class="smText"><% = strMemberGroupName %><br /><img src="<% If strRankCustomStars <> "" Then Response.Write(strRankCustomStars) Else Response.Write(strImagePath & intRankStars & "_star_rating.gif") %>" alt="<% = strMemberGroupName %>"></td>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="116" height="24" class="smText"><% = DateFormat(dtmRegisteredDate, saryDateTimeData) %></td>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="41" align="center" height="24" class="text"><% = lngNumOfPosts %></td>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="59" align="center" height="24" class="text"><% If NOT strHomepage = "" Then Response.Write("<a href=""" & strHomepage & """ target=""_blank""><img src=""" & strImagePath & "home_icon.gif"" border=""0"" alt=""" & strTxtVisit & " " & strUsername & "'s " & strTxtHomepage & """></a>") %>&nbsp;</td>
          <% If blnPrivateMessages = True Then %>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="64" align="center" height="24" class="text"><a href="pm_buddy_list.asp?name=<% = Server.URLEncode(strUsername) %>" target="_self"><img src="<% = strImagePath %>add_buddy_sm.gif" align="absmiddle" border="0" alt="<% = strTxtAddToBuddyList %>"></a></td>
          <% End If %>
          <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="57" align="center" height="24"><a href="search_form.asp?KW=<% = Server.URLEncode(strUsername) %>&SI=AR"><img src="<% = strImagePath %>search_sm.gif" border="0" alt="<% = strTxtSearchForPosts %>&nbsp;<% = strUsername %>" align="absmiddle"></a></td>
         </tr><%

			'Move to the next record in the database
	   		rsCommon.MoveNext

		'Loop back round
		Next
	End If


%>
        </table>
      </tr>
     </table>
     </td>
    </tr>
    </table>
     <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center" height="32">
      <tr><%

   'If there is more than 1 page of members then dispaly drop down list to the other members
	If intTotalNumMembersPages > 1 Then

		'Display an drop down list to the other members in list
		Response.Write (vbCrLf & "		<td align=""right"" class=""text"">")
		
		'Display a prev link if previous pages are available
		If intRecordPositionPageNum > 1 Then Response.Write("<a href=""members.asp?SF=" & Server.URLEncode(Request.QueryString("SF")) & "&GID=" & intGetGroupID & "&SO=" & Trim(Mid(Request.QueryString("SO"),1,2)) & "&MemPN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")
		
		Response.Write (strTxtPage & " " & _
		vbCrLf & "		 <select onChange=""MembersPage(this)"" name=""SelectPage"">")

		Dim intTopicPageLoopCounter

		'Loop round to display links to all the other pages
		For intTopicPageLoopCounter = 1 to intTotalNumMembersPages

			'Display a link in the link list to the another members page
			Response.Write (vbCrLf & "		  <option value=""" & intTopicPageLoopCounter & """")

			'If this page number to display is the same as the page being displayed then make sure it's selected
			If intTopicPageLoopCounter = intRecordPositionPageNum Then
				Response.Write (" selected")
			End If

			'Display the link page number
			Response.Write (">" & intTopicPageLoopCounter & "</option>")

		Next

		'End the drop down list
		Response.Write (vbCrLf & "		</select> " & strTxtOf & " " & intTotalNumMembersPages)
		
		'Display a next link if needed
		If intRecordPositionPageNum <> intTotalNumMembersPages Then Response.Write("&nbsp;<a href=""members.asp?SF=" & Server.URLEncode(Request.QueryString("SF")) & "&GID=" & intGetGroupID & "&SO=" & Trim(Mid(Request.QueryString("SO"),1,2)) & "&MemPN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")
		
		Response.Write("</td>")
	End If
%>	</tr>
     </table>
     <%
	'Reset Server Variables
	rsCommon.Close
End If

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


     
Response.Write(vbCrLf & "    </form>" & _
vbCrLf & "    <div align=""center"">")
 
 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
    </div>
   </div>
   <!-- #include file="includes/footer.asp" -->