<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Declare variables
Dim intRowColourNumber	'Holds the number to calculate the table row colour	
Dim blnIsUserOnline	'Set to true if the user is online
Dim intForumID

'Initialise variable
intRowColourNumber = 0


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If



'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If


'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If


%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Private Messenger: Buddy List</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {
	
	var errorMsg = "";
	
	//Check for a buddy
	if (document.frmBuddy.username.value==""){
		errorMsg += "\n\t<% = strTxtNoBuddyErrorMsg %>";
	}
	
	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
		
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
	
	return true;
}
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtPrivateMessenger %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtPrivateMessenger %><br /></td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr> 
  <td width="60%"><span class="lgText"><img src="<% = strImagePath %>subject_folder.gif" width="26" height="26" alt="<% = strTxtSubjectFolder %>" align="absmiddle"> <% = strTxtPrivateMessenger & ": " & strTxtBuddyList %></span></td>
  <td align="right" width="40%" nowrap="nowrap"><a href="pm_inbox.asp" target="_self"><img src="<% = strImagePath %>inbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtInbox %>" border="0"></a><a href="pm_outbox.asp" target="_self"><img src="<% = strImagePath %>outbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtOutbox %>" border="0"></a><a href="pm_buddy_list.asp" target="_self"><img src="<% = strImagePath %>buddy_list.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtBuddyList %>" border="0"></a><a href="pm_new_message_form.asp" target="_self"><img src="<% = strImagePath %>new_private_message.gif" alt="<% = strTxtNewPrivateMessage %>" border="0"></a></td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" width="19%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtBuddy %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="40%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtDescription %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="35%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtContactStatus %></td><%
If blnActiveUsers Then %>
     <td bgcolor="<% = strTableTitleColour %>" width="6%" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtOnLine2 %></td><%
End If %>
     <td bgcolor="<% = strTableTitleColour %>" width="6%" align="center" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtDelete %></td>
    </tr><%
    
'Get the users buddy detals from the db
	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "BuddyList.*, " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID "
strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "BuddyList "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "BuddyList.Buddy_ID AND " & strDbTable & "BuddyList.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "BuddyList.Buddy_ID <> 2 "
strSQL = strSQL & "ORDER BY " & strDbTable & "BuddyList.Block ASC, " & strDbTable & "Author.Username ASC;" 
	

'Query the database
rsCommon.Open strSQL, adoCon
    
'Check there are PM messages to display
If rsCommon.EOF Then

	'If there are no pm messages to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtNoBuddysInList & "</td>"

'Else there the are topic's so write the HTML to display the topic names and a discription
Else 	
	
	'Loop round to read in all the Topics in the database
	Do while NOT rsCommon.EOF 
		
		'Get the row number
		intRowColourNumber = intRowColourNumber + 1
	%>
    <tr> 
     <td bgcolor="<% If (intRowColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="19%" class="text"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = rsCommon("Buddy_ID") %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"><% = rsCommon("Username") %></a></td>
     <td bgcolor="<% If (intRowColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="40%" class="text"><% = rsCommon("Description") %>&nbsp;</td>
     <td bgcolor="<% If (intRowColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="35%" class="text"><%
     		'Get the contact status
     		If rsCommon("Block") = True Then
     			Response.Write(strTxtThisPersonCanNotMessageYou)
     		Else
     			Response.Write(strTxtThisPersonCanMessageYou)
     		End If
     %></td><%
		'If active users is enabled see if any buddies are online
		If blnActiveUsers Then 
			
			'Initilase variable
			blnIsUserOnline = False
			
			'Get the users online status
			For intArrayPass = 1 To UBound(saryActiveUsers, 2)
				If saryActiveUsers(1, intArrayPass) = CLng(rsCommon("Buddy_ID")) Then blnIsUserOnline = True
			Next
			
			%>
     <td bgcolor="<% If (intRowColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="6%" class="text" align="center"><% If blnIsUserOnline Then Response.Write("<img src=""" & strImagePath & "yes.gif"">") Else Response.Write("<img src=""" & strImagePath & "no.gif"">") %></td><%
		 	
		End If 
		
		%>
     <td bgcolor="<% If (intRowColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="6%" class="text" align="center"><a href="pm_delete_buddy.asp?pm_id=<% = rsCommon("Address_ID") %>" OnClick="return confirm('<% = strTxtDeleteBuddyAlert %>')"><img src="<% = strImagePath %>delete_icon.gif" width="15" height="16" alt="<% = strTxtDelete %>" border="0"></a></td>
    </tr><%

		
		'Move to the next recordset
		rsCommon.MoveNext
	Loop
End If

'clear up
rsCommon.Close
%>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr><form method="post" name="frmBuddy" action="pm_add_buddy.asp" onSubmit="return CheckForm();" onReset="return ResetForm();">
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtAddNewBuddyToList %></td>
    </tr>
    <tr> 
     <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="23%" class="text"><% = strTxtMemberName %></td>
        <td width="32%" class="text"><% = strTxtDescription %></td>
        <td width="24%" class="text"><% = strTxtAllowThisMemberTo %></td>
        <td width="21%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="23%"> 
         <input type="text" name="username" size="15" maxlength="15" value="<% If CInt(Request.QueryString("code")) <> 2 Then Response.Write(Server.HTMLEncode(Request.QueryString("name"))) %>">
          <a href="JavaScript:openWin('pop_up_member_search.asp?RP=BUD','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=255')"><img src="<% = strImagePath %>search.gif" alt="<% = strTxtMemberSearch %>" border="0" align="absmiddle"></a> 
         </td>
        <td width="32%"> 
         <input type="text" name="description" size="25" maxlength="30" value="<% If CInt(Request.QueryString("code")) <> 2 Then Response.Write(Server.HTMLEncode(Request.QueryString("desc"))) %>">
        </td>
        <td width="24%"> 
         <select name="blocked">
          <option value="False" selected><% = strTxtMessageMe %></option>
          <option value="True"><% = strTxtNotMessageMe %></option>
         </select>
        </td>
        <td width="21%" align="right"><input type="submit" name="Submit" value="<% = strTxtAddToBuddy %>"></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</td>
</form></tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
  <tr><form>
   <td><!-- #include file="includes/forum_jump_inc.asp" --></td>
   </form>
  </tr>
 </table>
<div align="center"><br />
 <%
'Clear server objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"

%>
</div>
<%
'Display a msg letting the user know any add or delete details to the buddy list
Select Case Request.QueryString("ER")
	Case "1"
		Response.Write("<script  language=""JavaScript"">")
		Response.Write("alert('" & Replace(Server.HTMLEncode(Request.QueryString("name")), "'", "\'", 1, -1, 1) & " " & strTxtIsAlreadyInYourBuddyList & ".');")
		Response.Write("</script>")
	Case "2"
		Response.Write("<script  language=""JavaScript"">")
		Response.Write("alert('" & Replace(Server.HTMLEncode(Request.QueryString("name")), "'", "\'", 1, -1, 1) & ", " & strTxtUserCanNotBeFoundInDatabase & ".');")
		Response.Write("</script>")
End Select
%>
<!-- #include file="includes/footer.asp" -->