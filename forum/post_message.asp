<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<!--#include file="functions/functions_format_post.asp"	-->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill	All Rights Reserved.
'**
'**  This program is free software; you	can modify (at your own	risk) any part of it
'**  under the terms of	the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must	remain in tacked in the	scripts	and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole	or any part of this program even
'**  if	it is modified or reverse engineered in	whole or in part without express
'**  permission	from the author.
'**
'**  You may not pass the whole	or any part of this application	off as your own	work.
'**
'**  All links to Web Wiz Guide	and powered by logo's must remain unchanged and	in place
'**  and must remain visible when the pages are	viewed unless permission is first granted
'**  by	the copyright holder.
'**
'**  This program is distributed in the	hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the	implied	warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES	WHETHER	EXPRESSED OR IMPLIED.
'**
'**  You should	have received a	copy of	the License along with this program;
'**  if	not, write to:-	Web Wiz	Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No	official support is available for this program but you may post	support	questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered	by e-mail ever!
'**
'**  For correspondence	or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or	at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer	= True


'Dimension variables
Dim lngNumOfPosts		'Holds the number of posts a user has made
Dim blnEmailNotify		'Set to	true if	the users want to be notified by e-mail	of a post
Dim blnEmailSent		'Set to	true if	the e-mail is sent
Dim strEmailSubject		'Holds the subject of the e-mail
Dim strMessage			'Holds the Users Message
Dim lngMessageID		'Holds the message ID number
Dim strMode			'Holds the mode	of the page so we know whether we are editing, updating, or new	topic
Dim intForumID			'Holds the forum ID number
Dim lngTopicID			'Holds the topic ID number
Dim strSubject			'Holds the subject
Dim strPostDateTime		'Holds the current date	and time for the post
Dim strUserName			'Holds the username of the person we are going to email
Dim lngEmailUserID		'Holds the users ID of the person we are going to email
Dim strUserEmail		'Holds the users e-mail	address
Dim strEmailMessage		'Holds the body	of the e-mail
Dim blnSignature		'Holds wether a	signature is to	be shown or not
Dim intPriority			'Holds the priority of tipics
Dim strPostMessage		'Holds the post	to send	as mail	notify
Dim intReturnPageNum		'Holds the page	number to return to
Dim strForumName		'Holds the name	of the forum the message is being posted in
Dim intNumOfPostsInFiveMin	'Holds the number of posts the user has	made in	the last 5 minutes
Dim strReturnCode		'Holds the code	if the post is not valid and we	need to	return to forum	without	posting
Dim strPollQuestion		'Holds the poll	question
Dim blnMultipleVotes		'Set to	true if	multiple votes are allowed
Dim blnPollReply		'Set to	true if	users can't reply to a poll
Dim saryPollChoice()		'Array to hold the poll	choices
Dim intPollChoice		'Holds the poll	choices	loop counter
Dim strBadWord			'Holds the bad words
Dim strBadWordReplace		'Holds the rplacment word for the bad word
Dim lngPollID			'Holds the poll	ID number
Dim blnForumLocked		'Set to true if the forum is locked
Dim blnTopicLocked		'Set to true if the topic is locked
Dim intNewGroupID		'Holds the new group ID for the poster
Dim strGuestName		'Holds the name of the guest if it is a guest posting



'Initalise variables
strPostDateTime	= Now()
intNumOfPostsInFiveMin = 0
lngPollID = 0
blnForumLocked = False
blnTopicLocked = False



'If the	user has not logged in then redirect them to the main forum page
If lngLoggedInUserID = 0 OR blnActiveMember = False Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect "default.asp"
End If



'******************************************
'***	      Check IP address		***
'******************************************

'If the	user is	user is	using a	banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If





'******************************************
'***	  Read in form details		***
'******************************************

'Read in user deatils from the post message form
strMode	= Trim(Mid(Request.Form("mode"), 1, 10))
intForumID = CInt(Request.Form("FID"))
lngTopicID = CLng(Request.Form("TID"))
strSubject = Trim(Mid(Request.Form("subject"), 1, 41))
strMessage = Request.Form("Message")
lngMessageID = CLng(Request.Form("PID"))
blnEmailNotify = CBool(Request.Form("email"))
blnSignature = CBool(Request.Form("signature"))
intPriority = CInt(Request.Form("priority"))
'If the user is in a guest then get there name
If lngLoggedInUserID = 2 Then strGuestName = Trim(Mid(Request.Form("Gname"), 1, 20))




'******************************************
'***	     Get permissions	      *****
'******************************************

'Read in the forum name	and forum permssions from the database
'Initalise the strSQL variable with an SQL statement to	query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE	Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returned by the recordset	then get the forum permssions
If NOT rsCommon.EOF Then

	'See if the forum is locked if this is not the admin
	If blnAdmin = False Then blnForumLocked = CBool(rsCommon("Locked"))

	'Check the user	is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsCommon("Read")), CInt(rsCommon("Post")), CInt(rsCommon("Reply_posts")), CInt(rsCommon("Edit_posts")), 0, CInt(rsCommon("Priority_posts")), CInt(rsCommon("Poll_create")), 0,	0, 0)
End If

'Clean up
rsCommon.Close





'******************************************
'***    See if the topic is closed    *****
'******************************************

'If this is not a new topic see if the topic is closed
If strMode <> "new" AND blnForumLocked = False AND blnAdmin = False AND blnModerator = False Then

	'Initliase the SQL query to get the topic details from the database
	strSQL = "SELECT " & strDbTable & "Topic.Locked FROM " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there is a record returened see if the topic is closed
	If NOT rsCommon.EOF Then
		blnTopicLocked = CBool(rsCommon("Locked"))
	End If

	'Clean up
	rsCommon.Close
End If





'*****************************************************
'***   Redirect if the forum or topic is locked   ****
'*****************************************************

'If the forum or topic is locked then don't let the user post a message
If blnForumLocked OR blnTopicLocked Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect to error page
	If blnForumLocked Then
		Response.Redirect("not_posted.asp?mode=FLocked")
	Else
		Response.Redirect("not_posted.asp?mode=TClosed")
	End If
End If







'******************************************
'***	 Get return page details      *****
'******************************************

'If there is no	number must be a new post
If Request.Form("TPN") = "" Then
	intReturnPageNum = 1
Else
	intReturnPageNum = CInt(Request.Form("TPN"))
End If

'calcultae which page the tread	is posted on
If NOT Request.Form("ThreadPos") = "" Then

	'If the	position in the	topic is on next page add 1 to the return page number
	If CInt(Request.Form("ThreadPos")) > (intThreadsPerPage	* intReturnPageNum) Then
		intReturnPageNum = intReturnPageNum + 1
	End If
End If



'********************************************
'***  Clean up and check in form details  ***
'********************************************

'If there is no	subject	or message then	don't post the message as won't	be able	to link	to it
If strSubject =	"" AND (strMode	= "new"	OR strMode = "editTopic" OR strMode = "poll") Then strReturnCode = "noSubject"
If Trim(strMessage) = "" OR Trim(strMessage) = "<P>&nbsp;</P>" OR Trim(strMessage) = "<br>" Then strReturnCode = "noSubject"



'Place format posts posted with	the WYSIWYG Editor (RTE)
If Request.Form("browser") = "RTE" Then

	'Call the function to format WYSIWYG posts
	strMessage = WYSIWYGFormatPost(strMessage)

'Else standrd editor is	used so	convert	forum codes
Else
	'Call the function to format posts
	strMessage = FormatPost(strMessage)
End If


'If the user wants forum codes enabled then format the post using them
If Request.Form("forumCodes") Then strMessage = FormatForumCodes(strMessage)


'Check the message for malicious HTML code
strMessage = checkHTML(strMessage)

'Strip long text strings from message
strMessage = removeLongText(strMessage)

'Get rid of scripting tags in the subject
strSubject = removeAllTags(strSubject)
strSubject = formatInput(strSubject)

'If the user is in a guest then clean up their username to remove malicious code
If lngLoggedInUserID = 2 Then
	strGuestName = formatSQLInput(strGuestName)
	strGuestName = formatInput(strGuestName)
End If





'********************************************
'***	Read in	poll details (if Poll)	  ***
'********************************************

'If this is a poll then	clean read in the poll details
If strMode = "poll" AND	blnPollCreate Then

	'Read in poll question and multiple votes
	strPollQuestion	= Trim(Mid(Request.Form("pollQuestion"), 1, 70))
	blnMultipleVotes = CBool(Request.Form("multiVote"))
	blnPollReply = CBool(Request.Form("pollReply"))

	'If there is no	poll question then there initilise the error variable
	If strPollQuestion = ""	Then strReturnCode = "noPoll"

	'Clean up poll question
	strPollQuestion	= removeAllTags(strPollQuestion)
	strPollQuestion	= formatInput(strPollQuestion)


	'Loop through and read in the poll question
	For intPollChoice = 1 To intMaxPollChoices

		'ReDimension the array for the correct number of choices
		'ReDimensioning	arrays is bad for performance but usful	in this	for what I need	it for
		ReDim Preserve saryPollChoice(intPollChoice)

		'Read in the poll choice
		saryPollChoice(intPollChoice) =	Trim(Mid(Request.Form("choice" & intPollChoice), 1, 60))

		'If there is nothing in	position 1 and 2 throw up an error
		If intPollChoice < 2 AND saryPollChoice(intPollChoice) = "" Then strReturnCode = "noPoll"

		'If there is nothing in	the poll selection then	jump out the loop
		If saryPollChoice(intPollChoice) = "" Then

			'ReDimension the array for the correct number of choices
			ReDim Preserve saryPollChoice(intPollChoice - 1)

			'Exit loop
			Exit For
		End If

		'Clean up input
		saryPollChoice(intPollChoice) =	removeAllTags(saryPollChoice(intPollChoice))
		saryPollChoice(intPollChoice) =	formatInput(saryPollChoice(intPollChoice))
	Next
End If





'******************************************
'***	     Filter Bad	Words	      *****
'******************************************

'Initalise the SQL string with a query to read in all the words	from the smut table
strSQL = "SELECT " & strDbTable & "Smut.* FROM	" & strDbTable & "Smut"

'Open the recordset
rsCommon.Open strSQL, adoCon

'Loop through all the words to check for
Do While NOT rsCommon.EOF

	'Put the bad word into a string	for imporoved perfoamnce
	strBadWord = rsCommon("Smut")
	strBadWordReplace = rsCommon("Word_replace")

	'Replace the swear words with the words	in the database	the swear words
	strSubject = Replace(strSubject, strBadWord, strBadWordReplace,	1, -1, 1)
	strMessage = Replace(strMessage, strBadWord, strBadWordReplace,	1, -1, 1)

	'If this is a poll run the poll	choices	through	the bad	word filter as well
	If strMode = "poll" Then

		'Clean up the poll question
		strPollQuestion	= Replace(strPollQuestion, strBadWord, strBadWordReplace, 1, -1, 1)

		'Loop though and check all the strings in the Poll array
		For intPollChoice = 1 To UBound(saryPollChoice)
			saryPollChoice(intPollChoice) =	Replace(saryPollChoice(intPollChoice), strBadWord, strBadWordReplace, 1, -1, 1)
		Next
	End If

	'Move to the next word in the recordset
	rsCommon.MoveNext
Loop

'Reset server varaible
rsCommon.Close





'******************************************
'***	      Anti-spam	Check		***
'******************************************

'Check the user	is not pressing	refresh	and submitting the same	post more than once
If strMode <> "edit" AND strMode <> "editTopic" Then 


	'Initalise the SQL string with a query to read in the last post	from the database
	strSQL = "SELECT TOP 15	" & strDbTable & "Thread.Message, " & strDbTable & "Thread.Author_ID,	" & strDbTable & "Thread.Message_date FROM " & strDbTable & "Thread ORDER BY " & strDbTable & "Thread.Message_date DESC;"
	
	'Open the recordset
	rsCommon.Open strSQL, adoCon
	
	'If there is a post returned by	the recorset then check	it's not already posted	and for	spammers
	If NOT rsCommon.EOF Then
		'Check the last	message	posted is not the same as the new one
		If (rsCommon("Message")	= strMessage) AND NOT (strMode = "edit"	OR strMode = "editTopic") Then
	
			'Set the return	code
			strReturnCode =	"posted"
		End If
	
		'Check the user	hasn't posted in the last limit	set for	secounds and not more than 5 times in the last spam time limit set for minutes
		Do While NOT rsCommon.EOF AND blnAdmin = False AND lngLoggedInUserID <> 2
	
			'Check the user	hasn't posted in the last spam time limit set for seconds
			If rsCommon("Author_ID") = lngLoggedInUserID AND DateDiff("s", rsCommon("Message_date"), now())	< intSpamTimeLimitSeconds AND intSpamTimeLimitSeconds <> 0 Then
	
				'Set the return	code
				strReturnCode =	"maxS"
			End If
	
			'Check that the	user hasn't posted 5 posts in the spam time limit set for minutes
			If rsCommon("Author_ID") = lngLoggedInUserID AND DateDiff("n", rsCommon("Message_date"), now())	< intSpamTimeLimitMinutes AND intSpamTimeLimitMinutes <> 0 Then
	
				'Add 1 to the number of	posts in the last 5 minutes
				intNumOfPostsInFiveMin = intNumOfPostsInFiveMin	+ 1
	
				'If the	number of posts	is more	than 3 then set	the return code
				If intNumOfPostsInFiveMin = 5 Then
	
					'Set the return	code
					strReturnCode =	"maxM"
				End If
			End If
	
			'Move to the next post
			rsCommon.MoveNext
		Loop
	End If
	
	'Clean up
	rsCommon.Close
End If




'**********************************************
'***  If input problems	send to	error page  ***
'**********************************************

'If there is a return code then	this post is not valid so redirect to error page
If strReturnCode <> "" Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect to error page
	Response.Redirect "not_posted.asp?mode=" & strReturnCode
End If




'********************************************
'***		  Save new Poll		  ***
'********************************************

'If this is a poll then	clean read in the poll details
If strMode = "poll" AND	blnPollCreate Then

	'********************************************
	'***	     Save poll question		  ***
	'********************************************

	'Initalise the SQL string with a query to get the poll details
	strSQL = "SELECT TOP 1 " & strDbTable & "Poll.* FROM " & strDbTable & "Poll ORDER BY " & strDbTable & "Poll.Poll_ID DESC;"

	With rsCommon
		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Open the author table
		.Open strSQL, adoCon

		'Insert	the new	poll question in the recordset
		.AddNew

		'Update	recordset
		.Fields("Poll_question") = strPollQuestion
		.Fields("Multiple_votes") = blnMultipleVotes
		.Fields("Reply") = blnPollReply

		'Update	the database with the new poll question
		.Update

		'Re-run	the Query once the database has	been updated
		.Requery

		'Read in the new topic's ID number
		lngPollID = CLng(rsCommon("Poll_ID"))

		'Clean up
		.Close
	End With


	'********************************************
	'***	      Save poll	choices		  ***
	'********************************************

	'Add the new poll choices
	For intPollChoice = 1 To UBound(saryPollChoice)

		'Initalise the SQL string with a query to get the choice
		strSQL = "SELECT TOP 1 " & strDbTable & "PollChoice.* FROM " & strDbTable & "PollChoice;"

		With rsCommon
			'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
			.CursorType = 2

			'Set the Lock Type for the records so that the record set is only locked when it is updated
			.LockType = 3

			'Open the author table
			.Open strSQL, adoCon

			'Insert	the new	poll choices in	the recordset
			.AddNew

			'Update	recordset
			.Fields("Poll_ID") = lngPollID
			.Fields("Choice") = saryPollChoice(intPollChoice)

			'Update	the database with the new poll choices
			.Update

			'Re-run	the Query once the database has	been updated
			.Requery

			'Clean up
			.Close
		End With
	Next

	'Change	the mode to new	to save	the new	polls post message
	strMode = "new"
End If





'******************************************
'***	 Save new topic	subject		***
'******************************************

'If this is a new topic	then save the new subject heading and read back	the new	topic ID number
If strMode = "new" AND (blnPost OR blnPollCreate OR (blnAdmin OR blnModerator)) Then

	'Initalise the SQL string with a query to get the Topic	details
	strSQL = "SELECT TOP 1 " & strDbTable & "Topic.* FROM " & strDbTable & "Topic	"
	strSQL = strSQL	& "WHERE Forum_ID =" & intForumID & " "
	strSQL = strSQL	& "ORDER By " & strDbTable & "Topic.Start_date	DESC;"

	With rsCommon
		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Open the author table
		.Open strSQL, adoCon

		'Insert	the new	topic details in the recordset
		.AddNew

		'Update	recordset
		.Fields("Forum_ID") = intForumID
		.Fields("Poll_ID") = lngPollID
		.Fields("Subject") = strSubject
		.Fields("Priority") = intPriority
		.Fields("Start_date") =	strPostDateTime

		'Update	the database with the new topic	details
		.Update

		'Re-run	the Query once the database has	been updated
		.Requery

		'Read in the new topic's ID number
		lngTopicID = CLng(rsCommon("Topic_ID"))

		'Set the rerun page properties
		intReturnPageNum = 1

		'Clean up
		.Close
	End With
End If




'******************************************
'***	     Edit Topic	Update		***
'******************************************

'If the	post is	the first in the thread	then update the	topic details
If strMode = "editTopic" AND (blnEdit =	True OR	(blnAdmin OR blnModerator)) Then

	'Initalise the SQL string with a query to get the Topic	details
	strSQL = "SELECT " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Priority FROM " & strDbTable & "Topic "
	strSQL = strSQL	& "WHERE Topic_ID =" & lngTopicID & ";"

	With rsCommon
		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Open the author table
		.Open strSQL, adoCon

		'Update	the recorset
		.Fields("Subject") = strSubject
		.Fields("Priority") = intPriority

		'Update	the database with the new topic	details
		.Update

		'Change	the mode to edit
		strMode	= "edit"

		'Clean up
		.Close
	End With
End If




'******************************************
'***	    Edit Post Update		***
'******************************************

'If the	post is	a previous post	that has been edited then update the post
If strMode = "edit" AND	(blnEdit = True	OR (blnAdmin = True OR blnModerator = True)) Then

	'If we are to show who edit the post and time then contantinet it to the end of the message
	If blnShowEditUser Then
		strMessage = strMessage & "<edited><editID>" & strLoggedInUsername & "</editID><editDate>" & CDbl(now()) &  "</editDate></edited>"
	End If


	'Initalise the strSQL variable with an SQL statement to	query the database get the message details
	strSQL = "SELECT " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Message, " & strDbTable & "Thread.Show_signature, " & strDbTable & "Thread.IP_addr "
	strSQL = strSQL	& "FROM	" & strDbTable & "Thread "
	strSQL = strSQL	& "WHERE " & strDbTable & "Thread.Thread_ID=" & lngMessageID &	";"

	With rsCommon
		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Open the author table
		.Open strSQL, adoCon

		'Enter the updated post	into the recordset
		.Fields("Message") = strMessage
		.Fields("Show_signature") = CBool(blnSignature)
		.Fields("IP_addr") = getIP()
		


		'Update	the database
		.Update

		'Close rs
		.Close
	End With





'******************************************
'***	  Else Process New Post		***
'******************************************

'Else this is a	new post so save the new post to the database
ElseIf ((strMode = "new" AND (blnPost OR blnPollCreate)) OR (blnReply)) OR (blnAdmin OR blnModerator) Then


	'******************************************
	'***	       Save New	Post		***
	'******************************************

	'Initalise the strSQL variable with an SQL statement to	query the database get the message details
	strSQL = "SELECT TOP 1 " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Topic_ID, " & strDbTable & "Thread.Author_ID,	" & strDbTable & "Thread.Message, " & strDbTable & "Thread.Message_date, " & strDbTable & "Thread.Show_signature, " & strDbTable & "Thread.IP_addr "
	strSQL = strSQL	& "FROM	" & strDbTable & "Thread "
	strSQL = strSQL	& "ORDER BY " & strDbTable & "Thread.Thread_ID DESC;"

	With rsCommon
		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Open the threads table
		.Open strSQL, adoCon

		'Insert	the new	Thread details in the recordset
		.AddNew

		.Fields("Topic_ID") = lngTopicID
		.Fields("Author_ID") = lngLoggedInUserID
		.Fields("Message") = strMessage
		.Fields("Message_date")	= strPostDateTime
		.Fields("Show_signature") = blnSignature
		.Fields("IP_addr") = getIP()

		'Update	the database with the new Thread
		.Update

		'Requery cuase Access is so slow (needed to get	accurate post count)
		.Requery

		'Read in the thread ID for the guest posting
		lngMessageID = CLng(rsCommon("Thread_ID"))

		'Clean up
		.Close
	End With




	'******************************************
	'***	 Update	Topic Last Post	Date	***
	'******************************************

	'Initalise the SQL string with an SQL update command to	update the date	of the last post in the	Topic table
	strSQL = "UPDATE " & strDbTable & "Topic SET "
	strSQL = strSQL	& strDbTable & "Topic.Last_entry_date = "	& strDatabaseDateFunction & " "
	'strSQL = strSQL & ", " & strDbTable & "Topic.Moved_ID = 0 "
	strSQL = strSQL	& "WHERE (((" & strDbTable & "Topic.Topic_ID)=	" & lngTopicID & "));"

	'Write the updated date	of last	post to	the database
	adoCon.Execute(strSQL)




	'******************************************
	'***	 Save the guest username	***
	'******************************************

	'If this is a guest that is posting then save there name to the db
	If lngLoggedInUserID = 2 AND strGuestName <> "" Then
		'Initalise the SQL string with an SQL update command to	update the date	of the last post in the	Topic table
		strSQL = "INSERT INTO " & strDbTable & "GuestName ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Thread_ID] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('" & strGuestName & "', "
		strSQL = strSQL & "'" & lngMessageID & "' "
		strSQL = strSQL & ")"

		'Write the updated date	of last	post to	the database
		adoCon.Execute(strSQL)
	End If




	'******************************************
	'***	Update Author Number of	Posts	***
	'******************************************

	'Initalise the strSQL variable with an SQL statement to	query the database to get the number of	posts the user has made
	strSQL = "SELECT " & strDbTable & "Author.No_of_posts, " & strDbTable & "Group.Special_rank "
	strSQL = strSQL	& "FROM	" & strDbTable & "Author, " & strDbTable & "Group "
	strSQL = strSQL	& "WHERE " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Author.Author_ID= " & lngLoggedInUserID & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there is a record returned by the database then read in the	no of posts and	increment it by	1
	If NOT rsCommon.EOF Then

		'Read in the no	of posts the user has made and username
		lngNumOfPosts =	CLng(rsCommon("No_of_posts"))

		'Inrement the number of	posts by 1
		lngNumOfPosts =	lngNumOfPosts +	1

		'Initalise the SQL string with an SQL update command to	update the number of posts the user has	made
		strSQL = "UPDATE " & strDbTable & "Author SET "
		strSQL = strSQL	& "" & strDbTable & "Author.No_of_posts = " & lngNumOfPosts
		strSQL = strSQL	& " WHERE " & strDbTable & "Author.Author_ID= " & lngLoggedInUserID & ";"

		'Write the updated number of posts to the database
		adoCon.Execute(strSQL)
	End If




	'******************************************
	'***	    Update Rank Group	        ***
	'******************************************

	'See if the user is a member of a rank group and if so update their group if they have enough posts

	'If there is a record returned by the database then see if it is a group that needs updating
	If NOT rsCommon.EOF Then

		'If not a non rank group then see if the group needs updating
		If CBool(rsCommon("Special_rank")) = False Then

			'Clean up
			rsCommon.Close

			'Initlise variables
			intNewGroupID = intGroupID

			'Get the rank group the member shoukd be part of
			'Initalise the strSQL variable with an SQL statement to	query the database to get the number of	posts the user has made
			strSQL = "SELECT TOP 1 " & strDbTable & "Group.Group_ID "
			strSQL = strSQL	& "FROM " & strDbTable & "Group "
			strSQL = strSQL	& "WHERE (" & strDbTable & "Group.Minimum_posts <= " & lngNumOfPosts & ") And (" & strDbTable & "Group.Minimum_posts >= 0) "
			strSQL = strSQL	& "ORDER BY " & strDbTable & "Group.Minimum_posts DESC;"

			'Query the database
			rsCommon.Open strSQL, adoCon


			'Get the new Group ID
			If NOT rsCommon.EOF Then intNewGroupID = CInt(rsCommon("Group_ID"))


			'If the group ID is different to the present group one then update it
			If intGroupID <> intNewGroupID Then

				'Initalise the SQL string with an SQL update command to	update group ID of the author
				strSQL = "UPDATE " & strDbTable & "Author SET "
				strSQL = strSQL	& "" & strDbTable & "Author.Group_ID = " & intNewGroupID
				strSQL = strSQL	& " WHERE " & strDbTable & "Author.Author_ID= " & lngLoggedInUserID & ";"

				'Write the updated number of posts to the database
				adoCon.Execute(strSQL)
			End If
		End If
	End If

	'Close the recordset
	rsCommon.Close





	'******************************************
	'***	   Send	Email Notification	 **
	'******************************************

	If blnEmail = True Then


		'**********************************************************
		'*** Format the	post if	it is to be sent with the email	 **
		'**********************************************************

		'Get the subject of the	topic the thread is posted in

		'Initalise the SQL string with a query to get the Topic	details
		strSQL = "SELECT " & strDbTable & "Forum.Forum_name, " & strDbTable & "Topic.Subject "
		strSQL = strSQL	& "FROM	" & strDbTable & "Forum INNER JOIN " & strDbTable & "Topic ON	" & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID "
		strSQL = strSQL	& "WHERE Topic_ID =" & lngTopicID & ";"

		'Open the author table
		rsCommon.Open strSQL, adoCon

		'If there is records returned then read	in the details
		If NOT rsCommon.EOF Then

			'Read in the forum name	and subject
			strSubject = rsCommon("Subject")
			strForumName = rsCommon("Forum_name")
		End If

		'Close the recordset
		rsCommon.Close



		'Set the e-mail	subject
		strEmailSubject	= strMainForumName & " : " & decodeString(strSubject)

		'If we are to send an e-mail notification and send the post with the e-mail then format	the post for the e-mail
		If blnSendPost = True Then

			'Format	the post to be sent with the e-mail
			strPostMessage = "<br /><b>" & strTxtForum & ":</b> " &	strForumName
			strPostMessage = strPostMessage	& "<br /><b>" &	strTxtTopic & ":</b> " & strSubject
			strPostMessage = strPostMessage	& "<br /><b>" &	strTxtPostedBy & ":</b>	" & strLoggedInUsername & "<br /><br />"
			strPostMessage = strPostMessage	& strMessage

			'Change	the path to the	emotion	symbols	to include the path to the images
			strPostMessage = Replace(strPostMessage, "src=""smileys/", "src=""" & strForumPath & "/smileys/", 1, -1, 1)
		End If



		'*******************************************
		'***	   Send	Email Notification	 ***
		'*******************************************

		'Initalise the strSQL variable with an SQL statement to	query the database get the details for the email
		strSQL = "SELECT DISTINCT " & strDbTable & "EmailNotify.Author_ID, " & strDbTable & "Author.Username,	" & strDbTable & "Author.Author_email "
		strSQL = strSQL	& "FROM	" & strDbTable & "Author INNER	JOIN " & strDbTable & "EmailNotify ON " & strDbTable & "Author.Author_ID = " & strDbTable & "EmailNotify.Author_ID "
		If strDatabaseType = "SQLServer" Then
			strSQL = strSQL	& "WHERE (" & strDbTable & "EmailNotify.Forum_ID=" & intForumID & " OR	" & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ") AND	" & strDbTable & "Author.Author_email Is Not Null AND " & strDbTable & "Author.Active=1;"
		Else
			strSQL = strSQL	& "WHERE (" & strDbTable & "EmailNotify.Forum_ID=" & intForumID & " OR	" & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ") AND	" & strDbTable & "Author.Author_email Is Not Null AND " & strDbTable & "Author.Active=True;"
		End If

		'Query the database
		rsCommon.Open strSQL, adoCon

		'If a record is	returned by the	recordset then read in the details and send the	e-mail
		Do While NOT rsCommon.EOF

			'Read in the details from the recordset	for the	e-mail
			strUserName = rsCommon("Username")
			lngEmailUserID = CLng(rsCommon("Author_ID"))
			strUserEmail = rsCommon("Author_email")

			'If the	user wants to be e-mailed and the user has enetered their e-mail and they are not the original topic writter then send an e-mail
			If lngEmailUserID <> lngLoggedInUserID AND strUserEmail <> "" Then

				'Initailise the	e-mail body variable with the body of the e-mail
				strEmailMessage	= strTxtHi & " " & decodeString(strUserName) & ","
				strEmailMessage	= strEmailMessage & "<br /><br />" & strTxtEmailAMeesageHasBeenPosted &	" " & strMainForumName & " " & strTxtThatYouAskedKeepAnEyeOn
				strEmailMessage	= strEmailMessage & "<br /><br />" & strTxtEmailClickOnLinkBelowToView & " : -"
				strEmailMessage	= strEmailMessage & "<br /><a href=""" & strForumPath &	"/forum_posts.asp?TID="	& lngTopicID & "&TPN=" & intReturnPageNum & """>" & strForumPath & "/forum_posts.asp?TID=" & lngTopicID	& "&TPN=" & intReturnPageNum & "</a>"
				strEmailMessage	= strEmailMessage & "<br /><br />" & strTxtClickTheLinkBelowToUnsubscribe & " :	-"
				strEmailMessage	= strEmailMessage & "<br /><a href=""" & strForumPath &	"/email_notify.asp?TID=" & lngTopicID &	"&FID="	& intForumID & "&M=Unsubscribe"">" & strForumPath & "/email_notify.asp?TID=" & lngTopicID & "&FID=" & intForumID & "&M=Unsubscribe</a>"

				'If we are to send the post then attach	it as well
				If blnSendPost = True Then
					strEmailMessage	= strEmailMessage & "<br /><br /><hr />" & strPostMessage
				End If

				'Call the function to send the e-mail
				blnEmailSent = SendMail(strEmailMessage, decodeString(strUserName), decodeString(strUserEmail),	strMainForumName, decodeString(strForumEmailAddress), strEmailSubject, strMailComponent, true)
			End If

			'Move to the next record in the	recordset
			rsCommon.MoveNext
		Loop

		'Close the recordset
		rsCommon.Close
	End If



	'******************************************
	'***  Update The Number	of Forum Posts	***
	'******************************************

	'Update	the number of topics and posts in the database
	Call updateTopicPostCount(intForumID)
End If





'******************************************
'***	     Update Email Notify	***
'******************************************

'Delete	or Save	email notification for the user, if email notify is enabled

If (strMode = "new" OR strMode = "edit"	OR strMode="reply") AND	blnEmail = True	Then

	'Initalise the SQL string with a query to get the email	notify details
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "TopicEmailNotify	@lngAuthorID = " & lngLoggedInUserID & ", @lngTopicID= " & lngTopicID
	Else
		strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
		strSQL = strSQL	& "FROM	" & strDbTable & "EmailNotify "
		strSQL = strSQL	& "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID	& " AND	" & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ";"
	End If

	With rsCommon

		'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
		.CursorType = 2

		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3

		'Query the database
		.Open strSQL, adoCon


		'If the	user no-longer wants email notification	for this topic then remove the entry form the db
		If blnEmailNotify = False AND NOT .EOF Then

			'Delete	the db entry
			.Delete

		'Else if this is a new post and	the user wants to be notified add the new entry	to the database
		ElseIf (((strMode = "new" OR strMode = "reply") OR (strMode = "edit" AND .EOF)) AND blnEmailNotify = True) AND .EOF Then

			'Add new rs
			.AddNew

			'Create	new entry
			.Fields("Author_ID") = lngLoggedInUserID
			.Fields("Topic_ID") = lngTopicID

			'Upade db with new rs
			.Update
		End If

		'Clean up
		.Close

	End With
End If




'******************************************
'***	 Clean up and leave town	***
'******************************************

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'Return	to the page showing the	threads
Response.Redirect "forum_posts.asp?TID=" & lngTopicID &	"&PN=1&TPN=" & intReturnPageNum
%>