/*
 overlibmws_scroll.js plug-in module - Copyright Foteos Macrides 2002-2004.
  For support of the SCROLL feature.
  Initial: October 20, 2002 - Last Revised: February 24, 2004
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=0;
OLscrollRefresh=100;
registerCommands('scroll');

/////////
// DEFAULT CONFIGURATION
if (typeof ol_scroll=='undefined') var ol_scroll=0;
// END CONFIGURATION
////////

// INIT
var o3_scroll=0;

// For setting runtime variables to default values.
function setScrollVariables(){
o3_scroll=ol_scroll;
}

// For commandline parser.
function parseScrollExtras(pf,i,ar){
var k=i;
if(k<ar.length){
if(ar[k]==SCROLL){eval(pf+'scroll=('+pf+'scroll==0)?1:0');return k;}
if(ar[k]==-SCROLL){eval(pf+'scroll=0');return k;}}
return -1;
}

////////
// SCROLL SUPPORT FUNCTIONS
////////
// Sanity check, Init scroll timer
function OLchkScroll(){
if(o3_scroll&&(!o3_sticky||(OLdraggablePI&&o3_draggable)||
(o3_relx==null&&o3_midx==null)||(o3_rely==null&&o3_midy==null)))o3_scroll=0;
}
function OLinitScroll(X,Y){
if(o3_scroll){if(typeof over.scroll=='undefined'||over.scroll.canScroll)
over.scroll=new OLsetScroll(X,Y,OLscrollRefresh);}
}

// Set, Clear scroll timer
function OLsetScroll(X,Y,refresh){
this.canScroll=0;
this.refresh=refresh;
this.x=X;this.y=Y;
this.timer=setTimeout("OLscrollReposition()",this.refresh);
}
function OLclearScroll(){
if(!o3_scroll||typeof over.scroll=='undefined')return;
over.scroll.canScroll=1;
if(over.scroll.timer){clearTimeout(over.scroll.timer);over.scroll.timer=null;}
}

// Get window x,y
function getPageScrollX(){
return (o3_frame.pageXOffset)?o3_frame.pageXOffset:
(eval(docRoot))?eval('o3_frame.'+docRoot+'.scrollLeft'):-1;
}
function getPageScrollY(){
return (o3_frame.pageYOffset)?o3_frame.pageYOffset:
(eval(docRoot))?eval('o3_frame.'+docRoot+'.scrollTop'):-1;
}

// Get layer x,y
function getLayerLeft(layer){
return (layer.pageX)?layer.pageX:(layer.style.left)?parseInt(layer.style.left):-1;
}
function getLayerTop(layer){
return (layer.pageY)?layer.pageY:(layer.style.top)?parseInt(layer.style.top):-1;
}

// Repositions the layer if needed
function OLscrollReposition() {
var X,Y,pgLeft,pgTop;
pgTop=getPageScrollY();
pgLeft=getPageScrollX();
X=getLayerLeft(over)-pgLeft;
Y=getLayerTop(over)-pgTop;
if(X!=over.scroll.x||Y!=over.scroll.y){
repositionTo(over,pgLeft+over.scroll.x,pgTop+over.scroll.y);
if(OLshadowPI)repositionShadow(pgLeft+over.scroll.x,pgTop+over.scroll.y);
if(OLiframePI)repositionIfShim(pgLeft+over.scroll.x,pgTop+over.scroll.y);
if(OLhidePI)OLhideUtil(0,1,1,0,0,0);}
over.scroll.timer=setTimeout("OLscrollReposition()",over.scroll.refresh);
}

////////
// PLUGIN REGISTRATIONS
////////
registerRunTimeFunction(setScrollVariables);
registerCmdLineFunction(parseScrollExtras);

OLscrollPI=1;
OLloaded=1;
