<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>

<!-- 
	XSLT Stylesheet to transform to plaintext with fonttags which is used to convert to gif-format by Jon Henning Bergane
	Syntax of fonttags is: @<font-name>;<font-style>;<font-size>:text goes here.....
-->

<xsl:template match="/nitf">
@arial;regular;16:<xsl:value-of select="body/body.head/hedline/hl1"/>
<xsl:apply-templates select="body/body.head/byline"/>
<xsl:apply-templates select="body/body.content"/>

<!--
<xsl:if test="body/body.content/media[@media-type='image' and not(@class)]">
<xsl:apply-templates select="body/body.content/media[@media-type='image' and not(@class)]"/>
</xsl:if>
-->

@arial;regular;9:<xsl:value-of select="body/body.end/tagline"/>
</xsl:template>


<!-- Templates -->
<xsl:template match="body/body.head/byline">
@arial;bold;12:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="body/body.content">
<xsl:apply-templates select="p | hl2 | table | br"/>

<!--
<xsl:choose>
<xsl:when test="p">
</xsl:when>	
<xsl:otherwise>
<xsl:value-of select="."/>
</xsl:otherwise>
</xsl:choose>
-->

</xsl:template>

<!--
<xsl:template match="body/body.head/byline/*">
<xsl:value-of select="."/>
</xsl:template>
-->

<xsl:template match="p[.!='']">
<!-- Normal paragraphs -->
@arial;regular;9:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[.='']">
<!-- Empty paragraphs -->
	
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
<!-- Paragraph of "ingress" -->
@arial;bold;9:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[@innrykk='true']">
<!-- Paragraph of "Br�dtekst innrykk" -->
@arial;regular;9:   <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[@style='tabellkode']">
<!-- Paragraph of "tabellkode" -->
@arial;regular;9:<xsl:value-of select="."/>
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
<!-- Normal paragraphs -->
@arial;regular;9:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="hl2">
<!-- Mellomtittel -->
@arial;bold-italic;9:<xsl:value-of select="."/>
</xsl:template>

<!-- Template for Scanpix media	-->
<!--
<xsl:template match="body/body.content/media[@media-type='image' and not(@class)]">
@arial;bold;9:<xsl:value-of select="media-reference/@source"/>
@arial;italic;9:<xsl:value-of select="media-caption"/>
</xsl:template>
-->

</xsl:stylesheet>