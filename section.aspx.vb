Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports intranett
'Imports System.Security.Principal
'Imports System.DirectoryServices


Partial Class Section

    Inherits Page
    Public MenuId As Integer = 1
    Protected DataReader As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents LeftMenuNo As Literal
    Protected WithEvents PanelMenu As Panel
    Protected WithEvents PanelTicker As Panel
    Protected WithEvents PanelBodyFront As Panel

    'Protected WithEvents lblPictureHTML As System.Web.UI.HtmlControls.HtmlGenericControl




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub DoIplogin()
        Dim iploggedinuser As String
        iploggedinuser = CheckIP(Request.UserHostAddress, CType(Session("CurrentArticleMainGroup"), String))
        If iploggedinuser <> "" Then
            Session("UserId") = iploggedinuser

            AddUserHit(CType(Session("UserID"), String), True, False, CType(Session("SiteCode"), String))

            Dim arr As Array = Split(GetUserData(CType(Session("UserId"), String), "MainGroups"), ",")
            Dim i As Integer
            For i = 0 To arr.Length - 1
                If arr(i) <> "" Then Session("MainGroup" & CStr(arr(i))) = "1"
            Next
        End If
    End Sub

    Private Sub ShowSection()

        ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
        Dim dbNtb As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand
        Dim forcelogin As Boolean = Request.QueryString("ForceLogin") = "1"
        Dim section As String
        section = Request.QueryString("section")
        If section = "" Then section = "FRONTPAGE"
        If section = "FRONTPAGE" Then CheckUpdateDailyPicture(MapPath("dbilde\"))

        If Request.QueryString("sort") <> "" Then
            phonebookSort = Request.QueryString("sort")
        End If

        ' liten hack for � logge bruker inn p� nyhetstjenesten f�rst for � vise korrekte stoffgrupper og kategorier...
        If forcelogin And Session("UserId") = "" Then
            Session("CurrentArticleMainGroup") = "-1"
            Session("RefreshSearch") = section

            DoIplogin()

            If Session("UserID") = "" Then
                Session("TargetURL") = Request.RawUrl
                Server.Transfer("login.aspx?Section=" & section)
            End If
        ElseIf Session("UserId") <> "" Then
            AddUserHit(CType(Session("UserID"), String), False, False, CType(Session("SiteCode"), String))
        End If

        If Session("FirstLogin") = "" Then
            ' Finn "hjemmesiden" for denne brukeren

            Dim currentUser As System.Security.Principal.IPrincipal = HttpContext.Current.User

            If currentUser.IsInRole("Avd_NTB_Sport") Then
                lblTestingUser.Text = "Sport"
                section = "SPORT"
                Session("FirstLogin") = "first"
            End If




        End If
        cd.Connection = dbNtb
        cd.CommandType = CommandType.StoredProcedure
        cd.CommandText = "_GetSection"
        cd.Parameters.Add(New SqlParameter("@section", SqlDbType.VarChar, 100))
        cd.Parameters.Item("@section").Value = section
        cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        cd.Parameters.Item("@sitecode").Value = Session("SiteCode")

        dbNtb.Open()
        DataReader = cd.ExecuteReader
        pPicture.Visible = False
        pPictureHTML.Visible = False
        imgTitleGif.Visible = False
        If DataReader.Read() Then
            If (DataReader("HTMLContent") = 1) Then
                lblContent.Text = ReplaceTags(DataReader("Content").ToString(), section)

                If Session("RefreshSearch") = section Then
                    lblContent.Text &= vbCrLf & "<script>parent.search.document.location.reload();</script>"
                    Session("RefreshSearch") = ""
                End If

                pHTMLContent.Visible = True
                pTextContent.Visible = False
                pPicture.Visible = False
            Else
                pHTMLContent.Visible = False
                If Not (DataReader("Title") Is DBNull.Value) Then lblTitle.Text = ReplaceTags(ToHTML(DataReader("Title").ToString()))
                If Not (DataReader("Ingress") Is DBNull.Value) Then lblIngress.Text = ReplaceTags(ToHTML(DataReader("Ingress").ToString()))
                If Not (DataReader("Text") Is DBNull.Value) Then lblText.Text = ReplaceTags(ToHTML(DataReader("Text").ToString()))
                If Not (DataReader("Footer") Is DBNull.Value) Then lblFooter.Text = ReplaceTags(ToHTML(DataReader("Footer").ToString()))
                If (DataReader("Footer").ToString = "") Then pFooter.Visible = False Else pFooter.Visible = True

                If DataReader("HasPicture") = 1 And DataReader("PictureHTML").ToString = "" Then
                    If Not (DataReader("PictureHeading") Is DBNull.Value) Then lblPictureHeading.Text = ReplaceTags(ToHTML(DataReader("PictureHeading").ToString()))
                    If Not (DataReader("PictureText") Is DBNull.Value) Then lblPictureText.Text = ReplaceTags(ToHTML(DataReader("Picturetext").ToString()))
                    pPicture.Visible = True
                    imgSection.ImageUrl = "picture.aspx?Section=" & section

                    'If DataReader("PictureWidth") > DataReader("PictureHeight") Then
                    ' imgSection.Width = System.Web.UI.WebControls.Unit.Pixel(ConfigurationSettings.AppSettings("SectionPictureMaxWidth"))
                    imgSection.Width = Unit.Pixel(CType(WebConfigurationManager.AppSettings("SectionPictureMaxWidth").ToString(), Integer))
                    'Else
                    'imgSection.Height = System.Web.UI.WebControls.Unit.Pixel(ConfigurationSettings.AppSettings("SectionPictureMaxHeight"))
                    'End If
                Else
                    pPicture.Visible = False
                    If DataReader("PictureHTML").ToString <> "" Then
                        mainTable.Width = CType(300, String)
                        pPictureHTML.Visible = True
                        lblPictureHTML.Text = DataReader("PictureHTML").ToString()
                    End If
                End If
            End If
            If DataReader("TitleGif").ToString <> "" Then
                topTbl.BackImageUrl = "images/headere/vert-bar.gif"
                imgTitleGif.Visible = True
                imgTitleGif.ImageUrl = DataReader("TitleGif").ToString()
            End If
            ' pLastModified.Visible = (ConfigurationSettings.AppSettings("ShowLastmodified") = "1")
            pLastModified.Visible = (WebConfigurationManager.AppSettings("ShowLastmodified") = "1")
            lblLastModified.Text = CType(DataReader("LastModified"), String)
            MenuId = CType(DataReader("MenuNo"), Integer)
        Else
            pHTMLContent.Visible = True
            lblContent.Text = "<b>Seksjon ikke funnet!</b>"
        End If
        DataReader.Close()
        dbNtb.Close()

        'If Not ShowMenu Then
        'PanelMenu.Visible = False
        'body.Attributes.Add("background", "")
        'End If

        If Request.QueryString("MenuId") <> "" Then MenuId = CInt(Request.QueryString("MenuId"))
        If MenuId = 0 Then MenuId = 1
        'Vis kun ticker p� www.ntb.no forsiden
        If section = "FRONTPAGE" And Session("SiteCode") = "WWW" Then
            'PanelTicker.Visible = True
        Else
            'PanelTicker.Visible = False
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        ShowSection()
    End Sub
End Class